<?php include_once('header.php') ?>
<?php include_once('class_przelewy24.php') ?>
<div class="block-header">
    <div class="container">
        <div class="block-title">Szczegóły płatności</div>
    </div>
</div>

<div class="block-order">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 vol-lg-12">
                <div class="block-info-for-passengers block-registration">
                    <div class="block-title">Dane do sprzedaży</div>
                    <div class="block-form">
                        <hr class="line-form">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <input id="name" type="text" class="form-control"
                                               placeholder="Imię"
                                               value="<?= $resulted['name'] ?>"
                                               style="margin-bottom: 20px;">

                                        <input id="surname" type="text" class="form-control"
                                               placeholder="Nazwisko"
                                               value="<?= $resulted['surname'] ?>">
                                    </div>
                                    <div class="col-lg-6">
                                        <input id="passeg-email" type="email" class="form-control" placeholder="E-mail"
                                               value="<?= $resulted['email'] ?>"
                                               style="margin-bottom: 20px;">

                                        <input id="phone" type="tel" class="form-control" placeholder="Tel"
                                               value="<?= $resulted['phone'] ?>">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="checkbox-buy">
                            <input type="checkbox" class="checkbox_faktury"
                                   id="bilet-zbiorczy" <?= $resulted['checkFacture'] === 'true' ? 'checked' : '' ?>
                                   autocomplete="false">
                            <label for="bilet-zbiorczy">
                                <span></span>
                            </label>
                            <strong>Faktura VAT</strong>
                        </div>
                        <div id="group_registration"
                             class="block-registration" <?= $resulted['checkFacture'] === 'true' ? '' : 'style="display: none"' ?> >
                            <div class="part left_part">
                                <div class="wrap-block">
                                    <div class="caption">
                                        <h3 class="faktura_title">Dane do faktury</h3>
                                    </div>

                                </div>
                            </div>
                            <?php
                                include_once ('helpers/invoice/InvoiceHelper.php');
                                $invoice = new InvoiceHelper();
                                $invoice->getHtml();
                            ?>
<!--                            <div class="part right_part">-->
<!--                                <div class="wrap-block">-->
<!--                                    <div class="form-group find">-->
<!--                                        <input id="name-company" type="text"-->
<!--                                               placeholder="Nazwa firmy lub os. fiz"-->
<!--                                               data="name"-->
<!--                                               value="--><?//= $resulted['checkFacture'] == 'true' ? $resulted['name_company'] : '' ?><!--"-->
<!--                                               class="field-name-company form-control">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="wrap-block">-->
<!--                                    <div class="form-group find">-->
<!--                                        <input id="NIP" data="nip"-->
<!--                                               value="--><?//= $resulted['checkFacture'] == 'true' ? $resulted['nip'] : '' ?><!--"-->
<!--                                               type="text"-->
<!--                                               placeholder="NIP" class="field-NIP form-control">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="wrap-block">-->
<!--                                    <div class="form-group find">-->
<!--                                        <input id="postal-code" data="code" type="text" placeholder="Kod pocztowy"-->
<!--                                               value="--><?//= $resulted['checkFacture'] == 'true' ? $resulted['postal_code'] : '' ?><!--"-->
<!--                                               class="field-postal-code form-control">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="wrap-block">-->
<!--                                    <div class="form-group find">-->
<!--                                        <input id="region" type="text" data="city"-->
<!--                                               value="--><?//= $resulted['checkFacture'] == 'true' ? $resulted['region'] : '' ?><!--"-->
<!--                                               placeholder="Kraj"-->
<!--                                               class="field-city form-control">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="wrap-block">-->
<!--                                    <div class="form-group find">-->
<!--                                        <input id="city" type="text" data="city"-->
<!--                                               value="--><?//= $resulted['checkFacture'] == 'true' ? $resulted['company_city'] : '' ?><!--"-->
<!--                                               placeholder="Miasto"-->
<!--                                               class="field-city form-control">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="wrap-block">-->
<!--                                    <div class="form-group find">-->
<!--                                        <input id="street" type="text" data="street"-->
<!--                                               value="--><?//= $resulted['checkFacture'] == 'true' ? $resulted['company_street'] : '' ?><!--"-->
<!--                                               placeholder="Ulica"-->
<!--                                               class="field-street form-control">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                        <hr class="line-form">

                        <div class="block-order-tickets">
                            <div class="row-goods header-row-goods">
                                <div class="row-g">

                                    <div class="title-distance">
                                        <div class="col-1">
                                            <div class="col-title">Z</div>
                                        </div>
                                        <div class="col-2">
                                            <div class="col-title">Do</div>
                                        </div>
                                    </div>
                                    <div class="wrap-field">
                                        <div class="count-tickets">
                                            <div class="col-title">Ilość</div>
                                        </div>
                                        <div class="price">
                                            <div class="col-title">Cena</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="details"></div>
                        </div>
                        <div id="renderTickets">
                            <div class="container" style="text-align: center">
                                <div id="loader" class="single4"></div>
                            </div>
                        </div>
                        <div class="block-all-sum">
                            <div class="title">Razem</div>
                            <div class="all-sum"></div>
                        </div>
                        <div class="wrap-btn">
                            <button type="submit" value="Wykonaj przelew" class="btn btn-go-payment">
                                Przejdź do płatności
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap-middle">
    <!--  <div class="slider-middle"><img src="../img/slide-1.jpg" alt=""><img src="../img/slide-1.jpg" alt=""><img src="../img/slide-1.jpg" alt=""></div>-->
    <!--  <div class="bg-bottom"></div>-->
    <?php include_once('footer.php') ?>
</div>
<div id="footer">
    <div class="bg-contact"></div>
    <div class="footer">
        <div class="container">
            <div class="row-f">
                <div>
                    <div class="footer-carousel">
                        <div class="carousel-company">
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-1.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-2.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-3.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-4.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-5.png" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-6.jpg" alt=""></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-text">
                <div>Nasz serwis wykorzystuje pliki cookies. Korzystanie z witryny oznacza zgodę na ich zapis lub odczyt zgodnie z ustawieniami przeglądarki.
                </div>
                <div class="copy">Copyright © MDA S.A Kraków</div>
            </div>
        </div>
    </div>
</div>
<script src="libs/jquery/jquery-1.9.1.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/jquery-mask/jquery.mask.js"></script>
<script>
    $('[data="code"]').mask('00-000', {clearIfNotMatch: true});
    // $('[data="nip"]').mask('000000000', {clearIfNotMatch: true});
</script>
<script src="libs/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
<script src="libs/waypoints/waypoints.min.js"></script>
<script src="libs/animate/animate-css.js"></script>
<script src="libs/plugins-scroll/plugins-scroll.js"></script>
<script src="libs/WOW/wow.js"></script>
<script src="libs/slick/slick.min.js"></script>
<script src="libs/slicknav/jquery.slicknav.min.js"></script>
<script src="libs/equalheight/jquery.equalheight.js"></script>
<script src="libs/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.js"></script>
<script src="libs/ui/jquery.ui.timepicker-pl.js"></script>
<script src="libs/ui/datepicker-pl.js"></script>
<script src="libs/ui/datepicker-de.js"></script>
<script src="libs/ui/jquery.ui.timepicker.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/purify.min.js"></script>
<script src="libs/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="libs/bootstrap-fileinput/themes/fa/theme.js"></script>
<script src="libs/bootstrap-fileinput/js/locales/pl.js"></script>
<script src="libs/sweetalert/sweetalert.min.js"></script>
<script src="libs/jquery-cookie/cookie.js"></script>
<script src="libs/jq-validation/jquery.validate.min.js"></script>
<script src="libs/jq-validation/additional-methods.min.js"></script>
<script src="js/common.js"></script>
<script src="js/account.js"></script>
<script src="js/registration.js"></script>
<script src="js/main.js"></script>
<script src="js/order.js?v3"></script>
<script src="node_modules/js-md5/src/md5.js"></script>

</body>
</html>