<?php
$db = Model::Instance();
$customer = $db->Array_where('subsections', "WHERE id_sect='3'");
$company = $db->Array_where('subsections', "WHERE id_sect='4'");
$customer1 = array();
$customer2 = array();
$i = 0;
foreach ($customer as $item) {
    if ($i % 2 == 0)
        {array_push($customer1, $item);}
    else
        {array_push($customer2, $item);}
    $i++;
}
?>
<div class="footer-contacts">
    <div class="container">
        <div class="row menu_row">
            <div class="col-xs-7 col-sm-9 col-md-8 col-lg-9">
                <div class="block-title">Dla pasażera</div>
                <div class="row">
<!--                    <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-6">-->
<!--                        <ul class="block-menu">-->
<!--                            <li><a href="#">Ważne wskażowki | jzk kupować</a></li>-->
<!--                            <li><a href="#">Regulamin wyszukiwarki połączeń</a></li>-->
<!--                            <li><a href="#">Sprzedaży biletów online</a></li>-->
<!--                            <li><a href="#">Prawa i obowiązki Pasażera</a></li>-->
<!--                            <li><a href="#">Przewoźnicy | firmy przewozowe</a></li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                    <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-6">-->
<!--                        <ul class="block-menu">-->
<!--                            <li><a href="#">Uwagi | zwroty | reklamacje</a></li>-->
<!--                            <li><a href="#">Aplikacja mobilna</a></li>-->
<!--                            <li><a href="#">Polityka bezpieczeństwa | prywatności</a></li>-->
<!--                            <li><a href="#">Zarejestruj się</a></li>-->
<!--                            <li><a href="#">Pomoc</a></li>-->
<!--                        </ul>-->
<!--                    </div>-->
                    <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <ul class="block-menu">
                            <li><a target="_blank" href="/regulamin_sprzedaży_internetowej.html">Regulamin sprzedaży internetowej</a></li>
                            <?php
                                foreach ($customer1 as $item) {?>
                                    <li><a href="/customer/<?= $item["url"] ?>"><?= $item["name"] ?></a></li>
                                <?}
                            ?>
                        </ul>
                    </div>
                    <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <ul class="block-menu">
                            <?php
                                foreach ($customer2 as $item) {?>
                                    <li><a href="/partners/<?= $item["url"] ?>"><?= $item["name"] ?></a></li>
                                <?}
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-5 col-sm-3 col-md-4 col-lg-3">
                <div class="block-title">
                    Dla biznesu / Dla partnerów
                </div>
                <ul class="block-menu">
                    <?php
                        foreach ($company as $item) {?>
                            <li><a href="/partners/<?= $item["url"] ?>"><?= $item["name"] ?></a></li>
                        <?}
                    ?>
                </ul>
            </div>
        </div>
        <div class="row contact clearfix">
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="row">
                        <div class="block-title">
                            INFORMACJA TELEFONICZNA O ROZKŁADACH
                        </div>
                        <div class="schedule">24 h/czynne całą dobę</div>
                        <div class="phone"><a href="tel:48518919255">+48 703 40 33 40</a></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 mobile_version">
                <div class="block-social-share">
                    <div>Lubię to!</div>
                    <div class="social-counter"></div>
                </div>
            </div>
        </div>
        <div class="row bottom_row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="notation">Koszt 2,10 netto za każdą rozpoczęta minutę połączenia. Infolinia
                            udziela informacji o krajowych połączeniach autobusowych - brak możliwości zakupu biletu
                            oraz dokonywania rezerwacji
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="notation">Pamiętaj! przerwa technologiczna w systemie w czasie 23:30 - 1:00 Zakup
                            biletów w tym czasie jest niemożliwy
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 desktop_version">
                <div class="block-social-share">
                    <div class="social-counter"></div>
                </div>
            </div>
        </div>
    </div>
</div>