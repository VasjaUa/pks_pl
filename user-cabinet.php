<?php include_once('header.php');
$per_page = 8; //6
$page = (isset($_GET['page'])) ? $_GET['page'] - 1 : 0;

$pagesSold = (isset($_GET['sold']) ? $page : 0);
$pagesCanceled = (isset($_GET['canceled']) ? $page : 0);
$pagesInvoices = (isset($_GET['invoices']) ? $page : 0);

$startSold = abs($pagesSold * $per_page);
$startCanceled = abs($pagesCanceled * $per_page);
$startInvoices = abs($pagesInvoices * $per_page);


$sqlSoldTickets = "SELECT * FROM `tickets` WHERE `id` = '$id' ORDER BY `Number` DESC LIMIT $startSold,$per_page";
$soldTickets = mysql_query($sqlSoldTickets) or die(mysql_error());


$sqlCountSoldTickets = "SELECT COUNT(*)  FROM `tickets` WHERE `id` = '$id'";
$countSoldTickets = mysql_fetch_row(mysql_query($sqlCountSoldTickets))[0];
$numPagesSoldTickets = ceil($countSoldTickets / $per_page);


$sqlCanceledTickets = "SELECT * FROM `cancelledTickets` WHERE `id_user` = '$id' ORDER BY `Number` DESC LIMIT $startCanceled,$per_page";
$canceledTickets = mysql_query($sqlCanceledTickets) or die(mysql_error());


$sqlCountCanceledTickets = "SELECT COUNT(*)  FROM `cancelledTickets` WHERE `id_user` = '$id'";
$countCanceledTickets = mysql_fetch_row(mysql_query($sqlCountCanceledTickets))[0];
$numPagesCanceledTickets = ceil($countCanceledTickets / $per_page);


$sqlInvoices = "SELECT i.*, tr.invoice_number as inv_num, tr.firm as inv_firm, tr.uuid as inv_uuid, tr.SalesDate as inv_SalesDate, tr.Exhibitor as inv_Exhibitor, tr.DocumentTotal as inv_DocumentTotal FROM `invoices` AS i JOIN transactions AS tr ON (tr.id = i.transaction_id) WHERE  tr.user_id = '$id' ORDER BY i.id DESC LIMIT $startInvoices,$per_page";
$Invoices = mysql_query($sqlInvoices) or die(mysql_error());

$sqlCountInvoices = "SELECT COUNT(*)  FROM `invoices` AS i JOIN transactions AS tr ON (tr.id = i.transaction_id) WHERE  tr.user_id = '$id'";
$countInvoices = mysql_fetch_row(mysql_query($sqlCountInvoices))[0];
$numPagesInvoices = ceil($countInvoices / $per_page);

?>
<div class="block-header">
    <div class="container">
        <div class="block-title">Moje konto</div>
    </div>
</div>
<div class="block-user-tabs">
    <div class="container">
        <div class="block-tabs">
            <ul menuCabinet class="nav nav-tabs">
                <li page=""
                    class="<?= (isset($_GET['sold']) || isset($_GET['canceled']) || isset($_GET['invoices'])) ? '' : 'active' ?>">
                    <a href="#tab-5" data-toggle="tab">Moje konto</a>
                </li>

                <li page="?sold=true" class="<?= isset($_GET['sold']) ? 'active' : '' ?>">
                    <a href="#tab-2" data-toggle="tab">Opłacone</a>
                </li>

                <li page="?canceled=true" class="<?= isset($_GET['canceled']) ? 'active' : '' ?>">
                    <a href="#tab-3" data-toggle="tab">Anulowane</a>
                </li>

                <li page="?invoices=true" class="<?= isset($_GET['invoices']) ? 'active' : '' ?>">
                    <a href="#tab-4" data-toggle="tab">Faktury VAT</a>
                </li>
            </ul>
            <script>
                $('[menuCabinet] a').click(function () {
                    var $href = $(this).parents('li').attr('page');
                    window.history.replaceState(null, null, location.pathname + $href);
                    location.reload();
                })
            </script>
            <!--            <div class="tab-content">-->


            <div id="tab-5"
                 class="tab-pane fade<?= (isset($_GET['sold']) || isset($_GET['canceled']) || isset($_GET['invoices'])) ? '' : ' in active' ?>">
                <div class="block-user-cabinet">
                    <div class="row">
                        <div class="col col-xs-4 col-sm-4 col-md-4 col-lg-4 block-0">
                            <div class="block-personal">
                                <div class="box-img">
                                    <img style="height: 150px; width: 150px; border-radius: 50%"
                                         src="<?= (!$resulted['url']) ? "/img/userImg.png" : $resulted['url'] ?>"
                                         alt="">
                                </div>
                                <div class="wrap-btn">
                                    <form enctype="multipart/form-data" action="" method="post">
                                        <input name="file" id="file-3" type="file">
                                    </form>
                                    <a href="#" style="display: none" onclick="return false"
                                       class="btn-photo">Akceptuję</a>
                                </div>
                                <div class="bl-info">
                                    <div class="row-user">
                                        <span class="title">Login:</span>
                                        <span data="login" class="login"><?= $resulted['login'] ?></span>
                                    </div>
                                    <div class="row-user">
                                        <span class="title">e-mail:</span>
                                        <span data="email" class="e-mail edit"><?= $resulted['email'] ?></span>
                                    </div>
                                    <div class="row-user">
                                        <span class="title">Nr telefonu:</span>
                                        <span data="phone" class="phone edit"><?= $resulted['phone'] ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-4 col-sm-4 col-md-4 col-lg-4 block-1">
                            <div class="block-user-info">
                                <div class="row-info">
                                    <div class="title">Imię:</div>
                                    <div data="name" class="caption edit"><?= $resulted['name'] ?></div>
                                </div>
                                <div class="row-info">
                                    <div class="title">Nazwisko:</div>
                                    <div data="surname" class="caption edit"><?= $resulted['surname'] ?></div>
                                </div>
                                <div class="row-info">
                                    <div class="title">Data urodzenia:</div>
                                    <div data="birthday" class="caption edit"><?= $resulted['birthday'] ?></div>
                                </div>
                                <div class="row-info">
                                    <div class="title">Płeć:</div>
                                    <div data="sex" class="caption"><?= $resulted['gender'] ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-4 col-sm-4 col-md-4 col-lg-4 block-2">
                            <div class="block-user-info">
                                <div class="row-info">
                                    <div class="title">Kraj:</div>
                                    <div class="caption">
                                        <span class="edit" data="country"><?= $resulted['country'] ?></span></div>
                                </div>
                                <div class="row-info">
                                    <div class="title">Miasto:</div>
                                    <div class="caption">
                                        <span data="city" class="edit"><?= $resulted['city'] ?></span></div>
                                </div>
                                <div class="row-info">
                                    <div class="title">Zawód:</div>
                                    <div data="professions"
                                         class="caption edit"><?= $resulted['profession'] ?></div>
                                </div>
                                <div class="wrap-btn wrap-btn-1">
                                    <a onclick="return false" style="display: block" id="edit_user_cabinet" href="#"
                                       class="btn-edit">Edytuj</a>
                                </div>
                                <div class="wrap-btn wrap-btn-1">
                                    <a onclick="return false" style="display: none" id="save_user_cabinet" href="#"
                                       class="btn-edit btn-save">Zapisz</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-password-change">
                    <div class="container">
                        <div class="form-change-password">
                            <div class="col">
                                <div class="title">Zmiana hasła:</div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <input id="old-password" type="password" placeholder="Stare hasło"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <input id="new-password" type="password" placeholder="Nowe hasło"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="wrap-btn">
                                    <button id="change_password" type="submit" class="btn btn-change-password">
                                        Zapisz
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="group_registration" class="block-registration">
                    <div class="part left_part">
                        <div class="wrap-block">
                            <div class="caption">
                                <h3 class="faktura_title">Dane do faktury</h3>
                                <div>(wypełnij, jeśli nie chcesz uzupełniać danych dla każdej faktury)</div>
                            </div>
                        </div>
                    </div>

                    <?php
                    include_once('helpers/invoice/InvoiceHelper.php');
                    $invoice = new InvoiceHelper();
                    $invoice->getHtml();
                    ?>

                    <div class="wrap_btn_save">
                        <!--                                <a onclick="return false" style="display: inline-block" id="edit_user_faktura" href="#"-->
                        <!--                                   class="btn-edit">Edytuj</a>-->
                        <button type="submit" id="save_user_faktury" class="btn">Zapisz dane do faktury</button>
                        <div class="checkbox-buy">
                            <input type="checkbox" <?= $resulted['checkFacture'] == 'true' ? 'checked' : '' ?>
                                   class="checkbox_faktury" id="bilet-zbiorczy">
                            <label for="bilet-zbiorczy"
                                   class="<?= $resulted['checkFacture'] === 'true' ? 'checked' : '' ?>">
                                <span checkFacture></span><strong>Uzupełniaj dane dla każdej faktury</strong>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!--                </div>-->

            <div id="tab-2" class="tab-pane oplacheno_tab fade<?= (isset($_GET['sold'])) ? ' in active' : '' ?>">
                <div class="table_mob">
                    <div class="table_scroll">
                        <div class="block-filters"
                             style="display: <?= ($soldTickets) ? "block" : "none" ?>">
                            <div class="row-f">
                                <div class="col col-1">
                                    <div data-sort="order:asc" class="title sort-btn">Bilet</div>
                                </div>
                                <div class="col col-2">
                                    <div class="title">Relacja</div>
                                </div>
                                <div class="col col-3">
                                    <div id="sortByDate" class="title">Data</div>
                                </div>
                                <div class="col col-4">
                                    <div id="sortByPrice" class="title">Cena</div>
                                </div>
                                <div class="col col-5"></div>
                            </div>
                        </div>

                        <div id="sold">
                            <?php while ($row = mysql_fetch_assoc($soldTickets)) { ?>
                                <!--  One ticket-->
                                <div class="block-tickets fadeInUp mix-target undergraduate"
                                     data-order="<?= $row['Number']; ?>" data-year="2">
                                    <div class="row-ticket">
                                        <div class="row-t">
                                            <div class="col col-1">
                                                <div class="mob_title">Bilet</div>
                                                <div class="mob_col">
                                                    <div class="numb-ticket"><?= $row['Number']; ?></div>
                                                    <div class="city-ticket"><?= $row['ticket_type']; ?></div>
                                                </div>
                                            </div>
                                            <div class="col col-2">
                                                <div class="mob_title">Relacja</div>
                                                <div class="ticket-distance mob_col"><?= $row['station_from']; ?>
                                                    / <?= $row['station_to']; ?></div>
                                            </div>
                                            <div class="col col-3">
                                                <div class="mob_title">Data</div>
                                                <div class="ticket-date mob_col">
                                                    <div class="ticket-date-row">
                                                        <span class="title">Wyjazd: </span>
                                                        <span class="caption"><?php $data = str_split($row['departure_time'], 16);
                                                            echo $data[0] ?></span>
                                                    </div>
                                                    <div class="ticket-date-row">
                                                        <span class="title">Przyjazd: </span>
                                                        <span class="caption"><?php $data = str_split($row['arrival_time'], 16);
                                                            echo $data[0] ?></span>
                                                    </div>
                                                    <div class="ticket-date-row">
                                                        <span class="title">Czas podróży: </span>
                                                        <span class="caption time"><?= $row['total_time_road']; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-4">
                                                <div class="mob_title">Cena</div>
                                                <div class="ticket-price mob_col">
                                                    <span class="price"><?= number_format($row['price'], 2); ?>
                                                        zł</span>
                                                    <span class="valute"></span>
                                                </div>
                                            </div>
                                            <div class="col col-5">
                                                <div class="mob_col">
                                                    <div class="wrap-btn">
                                                        <a href="<?= SERVER_URL ?>/download.php?transaction=<?= $row['transaction_id'] ?>&number=<?= $row['number_ticket'] ?>"
                                                           download="bilet.pdf" class="btn-pay">Pobierz</a>
                                                    </div>
                                                    <div class="wrap-btn">
                                                        <a href="#" class="btn-cancellation">Anuluj</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <?php
                if ($numPagesSoldTickets > 1) { ?>
                    <div class='pagination'>
                        <?php if ($_GET['page'] > 1) { ?>
                            <a href='<?= SERVER_URL ?>/user-cabinet?sold=true&page=<?= ($_GET['page'] > 0) ? $_GET['page'] - 1 : 1 ?>'>≪</a>
                        <?php } ?>
                        <?php for ($i = 1; $i <= $numPagesSoldTickets; $i++) { ?>
                            <?php if ($i == 1 && ($_GET['page'] - 3) > 1) { ?>
                                <a href='<?= SERVER_URL ?>/user-cabinet?sold=true&page=1'>1</a>

                                <?php $i = $_GET['page'] - 3; ?>
                                <a href='<?= SERVER_URL ?>/user-cabinet?sold=true&page=<?= $i ?>'>...</a>

                            <?php } elseif ($i == ($_GET['page'] + 3) && $i != $numPagesSoldTickets) { ?>
                                <a href='<?= SERVER_URL ?>/user-cabinet?sold=true&page=<?= $i ?>'>...</a>
                                <a href='<?= SERVER_URL ?>/user-cabinet?sold=true&page=<?= $numPagesSoldTickets ?>'><?= $numPagesSoldTickets ?></a>
                                <?php $i = $numPagesSoldTickets; ?>
                            <?php } else { ?>
                                <a class='<?= ($_GET['page'] == $i || !isset($_GET['page']) && $i == 1) ? "active" : "" ?>'
                                   href='<?= SERVER_URL ?>/user-cabinet?sold=true&page=<?= $i ?>'><?= $i ?></a>
                            <?php } ?>

                        <?php } ?>

                        <?php if (!($_GET['page'] >= $numPagesSoldTickets)) { ?>
                            <a href='<?= SERVER_URL ?>/user-cabinet?sold=true&page=<?= ($_GET['page'] > 0) ? $_GET['page'] + 1 : 2 ?>'>≫</a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>


            <div id="tab-3" class="tab-pane anulowane_tab fade<?= (isset($_GET['canceled'])) ? ' in active' : '' ?>">
                <div class="table_mob">
                    <div class="table_scroll">
                        <div class="block-filters"
                             style="display: <?= ($canceledTickets) ? "block" : "none" ?>">
                            <div class="row-f">
                                <div class="col col-1">
                                    <div class="title">Bilet</div>
                                </div>
                                <div class="col col-2">
                                    <div class="title">Relacja</div>
                                </div>
                                <div class="col col-3">
                                    <div class="title">Data</div>
                                </div>
                                <div class="col col-4">
                                    <div class="title">Cena</div>
                                </div>
                            </div>
                        </div>
                        <div id="canceled">
                            <?php while ($row = mysql_fetch_assoc($canceledTickets)) { ?>
                                <!--  One ticket-->
                                <div class="block-tickets fadeInUp" data-order="<?= $row['Number']; ?>">
                                    <div class="row-ticket">
                                        <div class="row-t">
                                            <div class="col col-1">
                                                <div class="mob_title">Bilet</div>
                                                <div class="mob_col">
                                                    <div class="numb-ticket"><?= $row['Number']; ?></div>
                                                    <div class="city-ticket"><?= $row['ticket_type']; ?></div>
                                                </div>
                                            </div>
                                            <div class="col col-2">
                                                <div class="mob_title">Relacja</div>
                                                <div class="ticket-distance mob_col"><?= $row['station_from']; ?>
                                                    / <?= $row['station_to']; ?></div>
                                            </div>
                                            <div class="col col-3">
                                                <div class="mob_title">Data</div>
                                                <div class="ticket-date mob_col">
                                                    <div class="ticket-date-row">
                                                        <span class="title">Wyjazd: </span>
                                                        <span class="caption"><?php $data = str_split($row['departure_time'], 16);
                                                            echo $data[0] ?></span>
                                                    </div>
                                                    <div class="ticket-date-row">
                                                        <span class="title">Przyjazd: </span>
                                                        <span class="caption"><?php $data = str_split($row['arrival_time'], 16);
                                                            echo $data[0] ?></span>
                                                    </div>
                                                    <div class="ticket-date-row">
                                                        <span class="title">Czas podróży: </span>
                                                        <span class="caption time"><?= $row['total_time_road']; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-4">
                                                <div class="mob_title">Cena</div>
                                                <div class="ticket-price mob_col">
                                                    <span class="price"><?= number_format($row['price'], 2); ?>
                                                        zł</span>
                                                    <span class="valute"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <?php
                if ($numPagesCanceledTickets > 1) { ?>
                    <div class='pagination'>
                        <?php if ($_GET['page'] > 1) { ?>
                            <a href='<?= SERVER_URL ?>/user-cabinet?canceled=true&page=<?= ($_GET['page'] > 0) ? $_GET['page'] - 1 : 1 ?>'>≪</a>
                        <?php } ?>
                        <?php for ($i = 1; $i <= $numPagesCanceledTickets; $i++) { ?>
                            <?php if ($i == 1 && ($_GET['page'] - 3) > 1) { ?>
                                <a href='<?= SERVER_URL ?>/user-cabinet?canceled=true&page=1'>1</a>

                                <?php $i = $_GET['page'] - 3; ?>
                                <a href='<?= SERVER_URL ?>/user-cabinet?canceled=true&page=<?= $i ?>'>...</a>

                            <?php } elseif ($i == ($_GET['page'] + 3) && $i != $numPagesCanceledTickets) { ?>
                                <a href='<?= SERVER_URL ?>/user-cabinet?canceled=true&page=<?= $i ?>'>...</a>
                                <a href='<?= SERVER_URL ?>/user-cabinet?canceled=true&page=<?= $numPagesCanceledTickets ?>'>
                                    <?= $numPagesCanceledTickets ?>
                                </a>
                                <?php $i = $numPagesCanceledTickets; ?>
                            <?php } else { ?>
                                <a class='<?= ($_GET['page'] == $i || !isset($_GET['page']) && $i == 1) ? "active" : "" ?>'
                                   href='<?= SERVER_URL ?>/user-cabinet?canceled=true&page=<?= $i ?>'><?= $i ?></a>
                            <?php } ?>

                        <?php } ?>

                        <?php if (!($_GET['canceled'] >= $numPagesCanceledTickets)) { ?>
                            <a href='<?= SERVER_URL ?>/user-cabinet?canceled=true&page=<?= ($_GET['page'] > 0) ? $_GET['page'] + 1 : 2 ?>'>≫</a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>


            <div id="tab-4" class="tab-pane invoices_tab fade<?= (isset($_GET['invoices'])) ? ' in active' : '' ?>">


                <div class="table_mob">
                    <div class="table_scroll">
                        <div class="block-filters"
                             style="display: <?= ($Invoices) ? "block" : "none" ?>">
                            <div class="row-f">
                                <div class="col col-1">
                                    <div class="title">Numer</div>
                                </div>
                                <div class="col col-2">
                                    <div class="title">Data</div>
                                </div>
                                <div class="col col-3">
                                    <div class="title">Sprzedawca</div>
                                </div>
                                <div class="col col-4">
                                    <div class="title">Wystawca</div>
                                </div>
                                <div class="col col-5">
                                    <div class="title">Wartość brutto</div>
                                </div>
                            </div>
                        </div>

                        <div id="faktury">
                            <?php while ($row = mysql_fetch_assoc($Invoices)) {
                                $amount = mysql_fetch_assoc(mysql_query("SELECT `amount` FROM `transactions` WHERE id=$row[transaction_id]"));
                                $price_end = substr($amount["amount"], -2);
                                ?>
                                <!--  One ticket-->
                                <div class="block-tickets fadeInUp" data-order="<?= $row['id']; ?>">
                                    <div class="row-ticket">
                                        <div class="row-t">
                                            <div class="col col-1">
                                                <div class="mob_title">Numer</div>
                                                <div class="mob_col">
                                                    <div class="numb-ticket"><?= $row['inv_num']; ?></div>
                                                    <div class="city-ticket"><?= $row['ticket_type']; ?></div>
                                                </div>
                                            </div>
                                            <div class="col col-2">
                                                <div class="mob_title">Data</div>
                                                <div class="ticket-distance mob_col">
                                                    <?php $data = str_split($row['inv_SalesDate'], 16);
                                                    echo $data[0]
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col col-3">
                                                <div class="mob_title">Sprzedawca</div>
                                                <div class="ticket-distance mob_col">
                                                    <?= $row['inv_firm'] ?>
                                                </div>
                                            </div>
                                            <div class="col col-4">
                                                <div class="mob_title">Wystawca</div>
                                                <div class="ticket-distance mob_col">
                                                    <?= $row['inv_Exhibitor'] ?>
                                                </div>
                                            </div>
                                            <div class="col col-5">
                                                <div class="mob_title">Wartość brutto</div>
                                                <div class="ticket-price mob_col">
                                                        <span class="price">
                                                            <?= $row['inv_DocumentTotal'] ?>
                                                            zł</span>
                                                    <span class="valute"></span>
                                                </div>
                                            </div>
                                            <div class="col col-6">
                                                <div class="mob_col">
                                                    <div class="wrap-btn">
                                                    <?php 
                                                    $responce = mysql_fetch_assoc(mysql_query("SELECT `response` FROM `transactions` WHERE id=$row[transaction_id]"));
                                                    	if(!empty($responce['response'])){ ?>
															<a href="<?= SERVER_URL ?>/generate-save-pdf.php?type=i&file=faktura_<?= $row['transaction_id'] ?>"
                                                           download="faktura.pdf" class="btn-pay">Pobierz</a>
														<?php }else{ ?>
															<span class="btn-pay not-gen">Nie utworzony</span>
														<?php }
                                                    ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <?php
                if ($numPagesInvoices > 1) { ?>
                    <div class='pagination'>
                        <?php if ($_GET['page'] > 1) { ?>
                            <a href='<?= SERVER_URL ?>/user-cabinet?invoices=true&page=<?= $_GET['page'] - 1 ?>'>≪</a>
                        <?php } ?>
                        <?php for ($i = 1; $i <= $numPagesInvoices; $i++) { ?>
                            <?php if ($i == 1 && ($_GET['page'] - 3) > 1) { ?>
                                <a href='<?= SERVER_URL ?>/user-cabinet?invoices=true&page=<?= 1 ?>'>1</a>

                                <?php $i = $_GET['page'] - 3; ?>
                                <a href='<?= SERVER_URL ?>/user-cabinet?invoices=true&page=<?= $i ?>'>...</a>

                            <?php } elseif ($i == ($_GET['page'] + 3) && $i != $numPagesInvoices) { ?>
                                <a href='<?= SERVER_URL ?>/user-cabinet?invoices=true&page=<?= $i ?>'>...</a>
                                <a href='<?= SERVER_URL ?>/user-cabinet?invoices=true&page=<?= $numPagesInvoices ?>'>
                                    <?= $numPagesInvoices ?>
                                </a>
                                <?php $i = $numPagesInvoices; ?>
                            <?php } else { ?>
                                <a class='<?= ($_GET['page'] == $i || !isset($_GET['page']) && $i == 1) ? "active" : "" ?>'
                                   href='<?= SERVER_URL ?>/user-cabinet?invoices=true&page=<?= $i ?>'><?= $i ?></a>
                            <?php } ?>

                        <?php } ?>

                        <?php if (!($_GET['page'] >= $numPagesInvoices)) { ?>
                            <a href='<?= SERVER_URL ?>/user-cabinet?invoices=<?= $_GET['page'] + 1 ?>'>≫</a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>


        </div>
    </div>
</div>
</div>
<?php include_once('footer.php') ?>
<div id="footer">
    <div class="bg-contact"></div>
    <div class="footer">
        <div class="container">
            <div class="row-f">
                <div>
                    <div class="footer-carousel">
                        <div class="carousel-company">
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-1.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-2.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-3.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-4.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-5.png" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-6.jpg" alt=""></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-text">
                <div>Nasz serwis wykorzystuje pliki cookies. Korzystanie z witryny oznacza zgodę na ich zapis lub odczyt
                    zgodnie z ustawieniami przeglądarki.
                </div>
                <div class="copy">Copyright © MDA S.A Kraków</div>
            </div>
        </div>
    </div>
</div>


<script src="libs/jquery/jquery-1.9.1.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/jquery-mask/jquery.mask.js"></script>
<script src="libs/bootstrap/js/bootstrap.min.js"></script>
<script src="libs/waypoints/waypoints.min.js"></script>
<script src="libs/animate/animate-css.js"></script>
<script src="libs/plugins-scroll/plugins-scroll.js"></script>
<script src="libs/WOW/wow.js"></script>
<script src="libs/slick/slick.min.js"></script>
<script src="libs/slicknav/jquery.slicknav.min.js"></script>
<!--<script src="libs/equalheight/jquery.equalheight.js"></script>-->
<script src="libs/jquery.matchHeight-min.js"></script>
<script src="libs/ui/jquery-ui.js"></script>
<script src="libs/ui/jquery.ui.timepicker-pl.js"></script>
<script src="libs/ui/datepicker-pl.js"></script>
<script src="libs/ui/datepicker-de.js"></script>
<script src="libs/ui/jquery.ui.timepicker.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/purify.min.js"></script>
<script src="libs/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="libs/bootstrap-fileinput/themes/fa/theme.js"></script>
<script src="libs/bootstrap-fileinput/js/locales/pl.js"></script>
<script src="libs/jquery-cookie/cookie.js"></script>
<script src="libs/sweetalert/sweetalert.min.js"></script>
<script src="libs/jq-validation/jquery.validate.min.js"></script>
<script src="libs/jq-validation/additional-methods.min.js"></script>

<script src="js/common.js"></script>
<script src="js/account.js?v=2.2"></script>
<script src="js/registration.js"></script>
<script src="js/main.js"></script>



</body>
</html>