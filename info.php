<?php
include_once('header.php');
$db = Model::Instance();
$id = $_GET["q"];
$page = $db->Array_where('subsections', "WHERE url='$id'")[0];
?>
<div class="block-header">
    <div class="container">
        <div class="block-title">Wyszukaj połączenie</div>
    </div>
</div>

<div class="breadcrumbs">
    <ul class="container">
        <li>
            <a href="<?= SERVER_URL ?>/">Strona główna</a>
        </li>
        <li>
            <span> <?= $page["name"] ?></span>
        </li>
    </ul>
</div>

<div class="content info_content">           
    <section class="container">
        <h1 class="block_title"><?= $page["name"] ?></h1>
        <div class="entry_content">
            <?= $page["value"] ?>
        </div>
    </section>
</div>

<div class="wrap-middle">
    <?php include_once('footer.php') ?>
</div>

<div id="footer">
    <div class="bg-contact"></div>
    <div class="footer">
        <div class="container">
            <div class="row-f">
                <div>
                    <div class="footer-carousel">
                        <div class="carousel-company">
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="<?= SERVER_URL ?>/img/company-1.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="<?= SERVER_URL ?>/img/company-2.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="<?= SERVER_URL ?>/img/company-3.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="<?= SERVER_URL ?>/img/company-4.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="<?= SERVER_URL ?>/img/company-5.png" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="<?= SERVER_URL ?>/img/company-6.jpg" alt=""></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-text">
                <div>Nasz serwis wykorzystuje pliki cookies. Korzystanie z witryny oznacza zgodę na ich zapis lub odczyt zgodnie z ustawieniami przeglądarki.
                </div>
                <div class="copy">Copyright © MDA S.A Kraków</div>
            </div>
        </div>
    </div>
</div>
<script src="<?= SERVER_URL ?>/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/waypoints/waypoints.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/animate/animate-css.js"></script>
<script src="<?= SERVER_URL ?>/libs/plugins-scroll/plugins-scroll.js"></script>
<script src="<?= SERVER_URL ?>/libs/WOW/wow.js"></script>
<script src="<?= SERVER_URL ?>/libs/slick/slick.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/slicknav/jquery.slicknav.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/equalheight/jquery.equalheight.js"></script>
<script src="<?= SERVER_URL ?>/libs/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.js"></script>
<script src="<?= SERVER_URL ?>/libs/ui/jquery.ui.timepicker-pl.js"></script>
<script src="<?= SERVER_URL ?>/libs/ui/datepicker-pl.js"></script>
<script src="<?= SERVER_URL ?>/libs/ui/datepicker-de.js"></script>
<script src="<?= SERVER_URL ?>/libs/ui/jquery.ui.timepicker.js"></script>
<script src="<?= SERVER_URL ?>/libs/jQueryFormStyler/jquery.formstyler.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/bootstrap-fileinput/js/plugins/purify.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/bootstrap-fileinput/themes/fa/theme.js"></script>
<script src="<?= SERVER_URL ?>/libs/bootstrap-fileinput/js/locales/pl.js"></script>

<script src="<?= SERVER_URL ?>/libs/jquery-cookie/cookie.js"></script>
<script src="<?= SERVER_URL ?>/libs/jq-validation/jquery.validate.min.js"></script>
<script src="<?= SERVER_URL ?>/libs/jq-validation/additional-methods.min.js"></script>

<script src="<?= SERVER_URL ?>/js/registration.js"></script>
<script src="<?= SERVER_URL ?>/js/account.js"></script>
<script src="<?= SERVER_URL ?>/js/main.js"></script>
<script src="<?= SERVER_URL ?>/js/common.js"></script>
</body>
</html>