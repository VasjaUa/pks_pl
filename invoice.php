<?php

function generatePDF($transaction, $isInvoice, $ticketNumber)
{
    include_once('libs/mpdf50/mpdf.php');

    session_start();
    $mpdf = new mPDF('utf-8', '', 0, 'ebrima', 10, 10, 7, 7, 10, 10, "P");
    $mpdf->charset_in = 'utf-8';

    if (!$isInvoice) {
        if ($ticketNumber)
            $path = 'tickets/bilet_' . $transaction['id'] . '_' . $ticketNumber;
        else
            $path = 'tickets/bilet_' . $transaction['id'];


        $mpdf->WriteHTML(file_get_contents('./css/ticket.css'), 1);
        $mpdf->WriteHTML(renderTicketPdf($transaction, $ticketNumber), 2);
        $mpdf->Output($path, 'F');

        return realpath($path);
    } else {
        $path = 'invoices/faktura_' . $transaction['id'];

        $mpdf->WriteHTML(file_get_contents('css/style_faktura.css'), 1);
        $mpdf->WriteHTML(generateInvoice($transaction), 2);
        $mpdf->Output($path, 'F');

        return realpath($path);
    }
}


function generateInvoice($transaction, $invoice_id = null)
{
    include_once('admin/config.php');
    include_once('admin/startup.php');
    include_once('admin/Model/Model.php');
    startup();
    $db = Model::Instance();
    $page = $db->Array_where('sections', "WHERE id='1'")[0];

    $faktura = json_decode($transaction['invoices'], true);
    $tickets = json_decode($transaction['tickets'], true);
    $numberTicket = '';

    foreach ($tickets as $t) {
        $selectTicket = "SELECT * FROM tickets WHERE number_ticket ='" . $t[number_ticket] . "'";
        $query = mysql_query($selectTicket);

        $numberTicket .= ($key != 0) ? ", " : "";
        $numberTicket .=  mysql_fetch_assoc($query)["Number"];
    }

    if (!$invoice_id) {
        $now = date('Y-m-d H:i:s');
        $sql = "insert into invoices (transaction_id, firm, post_code, city, street, nip, created_at, updated_at) values 
                ('" . $transaction['id'] . "','" . $faktura['name_company'] . "','" . $faktura['postal_code'] . "','" . $faktura['city'] . "','" . $faktura['street'] . "','" . $faktura['nip'] . "','" . $now . "','" . $now . "')";

        if (mysql_query($sql)) {
            $invoice_id = mysql_insert_id();
        }
    }


    $invoice = "<div id='faktura' class='faktura'>
            <div class='container pdf' style='width: 1180px;'>
                <div class='row'>
                    <h2>Faktura nr I - " . $invoice_id . "</h2>
                </div>
                <table class='row contacts_wrrap'>
                    <tr>
                        <td class='col'>
                            <div>Miejsce, data wystawienia: Kraków, " . date_create_from_format('Y-m-d H:i:s', $transaction['created_at'])->format('Y-m-d H:i') . "</div>                            
                        </td>
                        <td class='col'>
                            <div>Data dostawy/wykonania usługi: " . date_create_from_format('Y-m-d H:i:s', $transaction['created_at'])->format('Y-m-d H:i') . "</div>                            
                        </td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:5px'></td>
                        <td class='col' style='height:5px'></td>
                    </tr>
                    <tr>
                        <td class='col'>
                            <div class='contacts_block'>
                                <h4>Sprzedawca</h4>
                                <p>MAŁOPOLSKIE DWORCE AUTOBUSOWE</p>
                                <p>SPÓŁKA AKCYJNA</p>
                                <p>al. płk. Władysława Beliny-Prażmowskiego 6A lok.6</p>
                                <p>31-514 Kraków </p>
                            </div>
                        </td>
                        <td class='col'>
                            <div class='contacts_block'>
                                <h4>Nabywca</h4>
                                <p>Nazwa firmy lub os. fiz: " . $faktura['name_company'] . "</p>
                                <p>Kod pocztowy: " . $faktura['postal_code'] . "</p>
                                <p>Miasto: " . $faktura['city'] . "</p>
                                <p>Ulica: " . $faktura['street'] . "</p>
                                <p>NIP: " . $faktura['nip'] . "</p>
                            </div>
                        </td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:5px'></td>
                        <td class='col' style='height:5px'></td>
                    </tr>
                    <tr>
                        <td class='col'><div>Forma zapłaty : Przelew</div></td>
                        <td class='col'></td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:5px'></td>
                        <td class='col' style='height:5px'></td>
                    </tr>
                    <tr>
                        <td class='col'><div>Numer konta: DB PBC S.A. - 07 1910 1048 4800 1407 1121 0001</div></td>
                        <td class='col'></td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:5px'></td>
                        <td class='col' style='height:5px'></td>
                    </tr>
                    <tr>
                        <td class='col'><div>Uwagi: Dotyczy biletu nr " . $numberTicket . "</div></td>
                        <td class='col'></td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:10px'></td>
                        <td class='col' style='height:10px'></td>
                    </tr>
                </table>
                <table class='faktura table'>
                    <tr>
                        <th rowspan='2'>Lp</th>
                        <th rowspan='2'>Nazwa towaru / usługi</th>
                        <th rowspan='2'>szt.</th>
                        <th rowspan='2'>Ilość</th>
                        <th rowspan='2'>Cena<br>netto</th>
                        <th rowspan='2'>Wartość<br>netto</th>
                        <th colspan='2'>Podatek</th>
                        <th rowspan='2'>Wartość<br>brutto</th>
                    </tr>
                    <tr>
                        <th>%</th>
                        <th>kwota</th>
                    </tr>";

    $index = 1;
//    $table = (isset($tableData)) ? json_decode(json_encode($tableData), true) : $_POST['table'];
    $total_netto = 0;
    $total_kwota = 0;
    $total_brutto = 0;

    foreach ($tickets as $item) {
        $netto = $item['fullPrice'] - $item['price_vat'];

        $total_netto += floatval($netto * $item['count']['count']);
        $total_kwota += floatval($item['price_vat'] * $item['count']['count']);
        $total_brutto += floatval($item['fullPrice'] * $item['count']['count']);

        $invoice .= "
                    <tr>
                        <td>" . $index . "</td>
                        <td class='description'>
                            Sprzedaż biletów jednorazowych;<br>
                            Relacja: " . $item['station_from'] . " - " . $item['station_to'] . "<br>
                            Data kursu:" . explode(' ', $item['departure'])[0] . "; Godzina : " . explode(' ', $item['departure'])[1] . "; Odległość: " . $item['distance'] . "
                        </td>
                        <td></td>
                        <td>" . $item['count']['count'] . "</td>
                        <td>" . number_format($netto, 2) . "</td>
                        <td>" . number_format($netto * $item['count']['count'], 2) . "</td>
                        <td>8.00 </td>
                        <td>" . number_format($item['price_vat'] * $item['count']['count'], 2) . "</td>
                        <td>" . number_format(floatval($item['current_price'] * $item['count']['count']), 2) . "</td>
                    </tr>
                   ";
        $index++;
    }

    $invoice .= "<tr class='bold'>
                        <td colspan='5'>Razem</td>
                        <td>" . number_format($total_netto, 2) . "</td>
                        <td>X</td>
                        <td>" . number_format($total_kwota, 2) . "</td>
                        <td>" . number_format($total_brutto, 2) . "</td>
                    </tr>
                    <tr class='bold'>
                        <td colspan='5' class='without_border'></td>
                        <td>" . number_format($total_netto, 2) . "</td>
                        <td>8.00</td>
                        <td>" . number_format($total_kwota, 2) . "</td>
                        <td>" . number_format($total_brutto, 2) . "</td>
                    </tr>
                </table>
                <table class='final_row row'>
                    <tr>
                        <td class='col'>
                            <h4>Do zapłaty : " . number_format($total_brutto, 2) . " zł</h4>
                            <div>
                                <p>Słownie: " . numberToText($total_brutto) . "</p>
                            </div>
                        </td>
                        <td class='col'>
                            <div>Wydruk z dnia: " . date_create_from_format('Y-m-d H:i:s', $transaction['created_at'])->format('Y-m-d H:i') . "</div>
                        </td>
                        <td class='col'></td>
                </table>
            </div>
        </div>";

    return $invoice;
}

function numberToText($liczba)
{

    $separator = ' ';
    $jednosci = array('', ' jeden', ' dwa', ' trzy', ' cztery', ' pięć', ' sześć', ' siedem', ' osiem', ' dziewięć');
    $nascie = array('', ' jedenaście', ' dwanaście', ' trzynaście', ' czternaście', ' piętnaście', ' szesnaście', ' siedemnaście', ' osiemnaście', ' dziewietnaście');
    $dziesiatki = array('', ' dziesieć', ' dwadzieścia', ' trzydzieści', ' czterdzieści', ' pięćdziesiąt', ' sześćdziesiąt', ' siedemdziesiąt', ' osiemdziesiąt', ' dziewięćdziesiąt');
    $setki = array('', ' sto', ' dwieście', ' trzysta', ' czterysta', ' pięćset', ' sześćset', ' siedemset', ' osiemset', ' dziewięćset');
    $grupy = array(
        array('', '', ''),
        array(' tysiąc', ' tysiące', ' tysięcy'),
        array(' milion', ' miliony', ' milionów'),
        array(' miliard', ' miliardy', ' miliardów'),
        array(' bilion', ' biliony', ' bilionów'),
        array(' biliard', ' biliardy', ' biliardów'),
        array(' trylion', ' tryliony', ' trylionów')
    );

    $wynik = '';
    $znak = '';
    if ($liczba == 0)
        return 'zero';
    if ($liczba < 0) {
        $znak = 'minus';
        $liczba = -$liczba;
    }
    $g = 0;
    while ($liczba > 0) {


        $s = floor(($liczba % 1000) / 100);
        $n = 0;
        $d = floor(($liczba % 100) / 10);
        $j = floor($liczba % 10);


        if ($d == 1 && $j > 0) {
            $n = $j;
            $d = $j = 0;
        }

        $k = 2;
        if ($j == 1 && $s + $d + $n == 0)
            $k = 0;
        if ($j == 2 || $j == 3 || $j == 4)
            $k = 1;

        if ($s + $d + $n + $j > 0)
            $wynik = $setki[$s] . $dziesiatki[$d] . $nascie[$n] . $jednosci[$j] . $grupy[$g][$k] . $wynik;

        $g++;
        $liczba = floor($liczba / 1000);
    }
    return trim($znak . $wynik);

}

function renderTicket($transaction)
{
    $tickets = json_decode($transaction['tickets'], true);
    $html = '<div class="ticket-new"><div class="container"><div id="renderTickets">';

    foreach ($tickets as $ticket) {
        $total_price = number_format($ticket['current_price'] * $ticket['count']['count'], 2);
        $net_price = number_format($ticket['fullPrice'] - $ticket['price_vat'], 2);
        $discount = number_format($ticket['fullPrice'] - $ticket['current_price'], 2);

        $html .= '
        <div class="ticket">
            <div class="header">
                <img src="img/bg-header-ticket.png"> 
                <div class="span_wrapp"> 
                    <span>Nr biletu<br>Ticket no</span> 
                </div>
                <span class="ticket_number">EL ' . $ticket['course_id'] . '</span> 
            </div>
            <div class="content">
                <div class="col first_col">
                    <div class="ticket_row">
                        <div class="left_part">
                            <img src="img/rotate_block_1.png"> 
                        </div>
                        <div class="right_part">
                            <div class="title">' . $ticket['carrier']['description'] . '</div>
                            <div class="adress">
                                ' . $ticket['carrier']['address'] . '<br>
                                Tel. ' . $ticket['carrier']['phone'] . '<br> 
                                NIP: ' . $ticket['carrier']['vat_number'] . '
                            </div>
                            <img src="img/bg-bus-ticket.png"> 
                        </div>
                    </div>
                    <div class="ticket_row route_row">
                        <div class="left_part">
                            <img src="img/rotate_block_2.jpg">
                        </div>
                        <div class="right_part">
                            <div class="col date_col">
                                <div class="head"><strong>Data</strong> | Date</div>
                                <div class="mark_text">' . explode(' ', $ticket['departure'])[0] . '</div>
                                <br>
                                <div class="mark_text">' . explode(' ', $ticket['arrival'])[0] . '</div>
                                <br> 
                            </div>
                            <div class="col hour_col">
                                <div class="head"><strong>Godzina</strong> | Hour</div>
                                <div class="mark_text">' . date_create_from_format('Y.m.d H:i:s', $ticket['departure'])->format('H:i') . '</div>
                                <br>
                                <div class="mark_text">' . date_create_from_format('Y.m.d H:i:s', $ticket['arrival'])->format('H:i') . '</div>
                                <br>
                            </div>
                            <div class="col city_col">
                                <div class="head"><strong>Miasto</strong> | City</div>
                                <div class="mark_text">' . $ticket['locality_from'] . '</div>
                                <div >' . $ticket['station_from'] . '</div>
                                <div class="mark_text">' . $ticket['locality_to'] . '</div>
                                <div ">' . $ticket['station_to'] . '</div>
                            </div>
                        </div>
                    </div>
                    <div class="ticket_row">
                        <img src="img/logo-mda-ticket.png"> 
                        <div class="contact_adress"> Małopolskie Dworce Autobusowe S.A.<br> ul. Bosacka 18, 31-505 Kraków, tel. 12 351 82 08 </div>
                    </div>
                </div>
                <div class="col second_col">
                    <div class="ticket_row">
                        <div class="head"><strong>Nabywca</strong> | Buyer</div>
                        <div class="name">' . $ticket['name'] . '</div>
                        <div class="tel">e-mail: ' . $ticket['email'] . '</div>
                    </div>
                    <div class="ticket_row">
                        <div class="bold">' . getTicketTypePl($ticket) . '</div>
                        <div>' . getTicketTypeEn($ticket) . '</div>
                    </div>
                    <div class="ticket_row">
                        <div class="stroke">
                            <div class="left">
                                <span class="bold">Ulga</span>
                                <span> | Discount</span> 
                            </div>
                            <div class="right">' . $ticket['current_ulga'] . '</div>
                        </div>
                        <div class="stroke">
                            <div class="left"> <span class="bold">Ilość</span>
                                <span> | Count</span> 
                            </div>
                            <div class="right">' . $ticket['count']['count'] . '</div>
                        </div>
                        <div class="stroke">
                            <div class="left"> <span class="bold">Netto PL</span>
                                <span> | Net price:</span> 
                            </div>
                            <div class="right">' . $net_price . ' zł</div>
                        </div>
                        <div class="stroke">
                            <div class="left">
                                <span class="bold">VAT PL:</span> 
                            </div>
                            <div class="right">' . number_format($ticket['price_vat'], 2) . ' zł</div>
                        </div>
                        <div class="stroke">
                            <div class="left"> <span class="bold">Brutto</span>
                                <span> | Gross price</span> 
                            </div>
                            <div class="right">' . number_format(['fullPrice'], 2) . ' zł</div>
                        </div>
                        <div class="stroke">
                            <div class="left"> <span class="bold">Rabat</span>
                                <span> | Discount </span> 
                            </div>
                            <div class="right">' . $discount . ' zł</div>
                        </div>
                        <div class="stroke">
                            <div class="left"> <span class="bold">RAZEM</span>
                                <span> | Total</span> 
                            </div>
                            <div class="right mark_text">' . $total_price . ' zł</div>
                        </div>
                    </div>
                </div>
                <div class="col third_col">
                    <div class="ticket_row">
                        <div class="head">
                         <strong>Data sprzedaży</strong><br><span>' . date('Y-m-d') . '</span>
                        </div>
                        <div class="date bold"> </div>
                    </div>
                    <div class="ticket_row">
                        <div class="head"><strong>Odległość</strong> | Distance</div>
                        <div class="distance">' . $ticket['distance'] . ' km</div>
                    </div>
                    <div id="qrcode-' . $ticket['course_id'] . '" class="ticket_row"><img src="' . getQrCode($ticket) . '"></div>
                </div>
            </div>
        </div>
        ';
    }

    $html .= '</div></div></div>';
    return $html;
}

function getTicketTypePl($ticket)
{
    if ($ticket['current_ulga'] == "Bez ulgi") return 'Normalny';
    else return $ticket['current_ulga'];
}

function getTicketTypeEn($ticket)
{
    if ($ticket['fullPrice'] == $ticket['current_price']) return 'Normal ticket';
    else return 'Reduced-fare ticket';
}

function getQrCode($ticket)
{
    include_once('libs/phpqrcode/qrlib.php');

    // how to save PNG codes to server

    $tempDir = 'qrcodes/';

    $codeContents =
        'fullPrice - ' . $ticket['fullPrice'] .
        'currentPrice= ' . $ticket['current_price'] .
        'course_id=' . $ticket['course_id'] .
        'station_from=' . $ticket['station_from'] .
        'station_to=' . $ticket['station_to'];

    // we need to generate filename somehow,
    // with md5 or with database ID used to obtains $codeContents...
    $fileName = $ticket['course_id'] . '.png';

    $pngAbsoluteFilePath = $tempDir . $fileName;

    QRcode::png($codeContents, $pngAbsoluteFilePath);

    return $pngAbsoluteFilePath;
//    $urlRelativeFilePath = EXAMPLE_TMP_URLRELPATH.$fileName;
//
//    // generating
//    if (!file_exists($pngAbsoluteFilePath)) {
//        QRcode::png($codeContents, $pngAbsoluteFilePath);
//        echo 'File generated!';
//        echo '<hr />';
//    } else {
//        echo 'File already generated! We can use this cached file to speed up site on common codes!';
//        echo '<hr />';
//    }
//
//    echo 'Server PNG File: '.$pngAbsoluteFilePath;
//    echo '<hr />';
//
//    // displaying
//    echo '<img src="'.$urlRelativeFilePath.'" />';
}

function renderTicketPdf($transaction, $ticketNumber)
{
    $connection = mysql_connect("localhost", "pkskrakow_4", "Dond33st4S!");
    $db = mysql_select_db("pkskrakow_4");
    mysql_query("SET NAMES 'utf8'");

    $tickets = json_decode($transaction['tickets'], true);
    $html = '';

    foreach ($tickets as $ticket) {
        if ($ticketNumber !== null) {
            $selectTicket = "SELECT * FROM tickets WHERE number_ticket ='" . $ticketNumber . "'";
            $query = mysql_query($selectTicket);
            $additionalData = mysql_fetch_assoc($query);
            $ticket["FiscalNumber"] = $additionalData["FiscalNumber"];
            $ticket["Number"] = $additionalData["Number"];
        }

        for ($i = 0; $i < 3; $i++) {
            $full_price = number_format($ticket['current_price'] * $ticket['count']['count'], 2);
            $vat_price = number_format($full_price * 8 / 108, 2);
            $net_price = number_format($full_price - $vat_price, 2);

            $brutto = array(
                'text' => '',
                'price' => ''
            );

            if (($ticket['foreign_price'] != 0)) {
                $brutto['text'] = 'Brutto.zagr';
                $brutto['price'] = number_format($ticket['foreign_price'], 2) . ' zł';
            } else {
                $i = 3;
            }

            $total_price = number_format($vat_price + $net_price + $ticket['foreign_price'], 2);

            $html .= '
<table border="1" style="width: 100%;">
    <tr style="display: none;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="1" style="text-align: center; border: 1px solid black;height: 40px;"><img src="./img/logo-pks-pl.png" /></td>
        <td colspan="5" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Nr biletu</small><p><b class="sm-f">' . $ticket['Number'] . '</b></p></span></td>
        <td colspan="4" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Sposób sprzedaży</small><p><b class="sm-f">Internet</b></p></span></td>
        <td colspan="4" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Data sprzedaży</small><p><b class="sm-f">' . date('Y-m-d') . '</b></p></span></td>
        <td colspan="4" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Nr paragonu fiskalnego</small><p><b class="sm-f">' . $ticket["FiscalNumber"] . '</b></p></span></td>
    </tr>

    <tr>
        <td colspan="3" style="border-left:1px solid black;padding-left:3px">
            <small>Sprzedawca (Przewoźnik)</small>
        </td>
        <td colspan="3"></td>
        <td colspan="4"><small class="vsm-f">NIP: ' . $ticket['carrier']['vat_number'] . '</small></td>
        <td style="border-left:1px solid black;padding-left: 3px;border-right: 1px solid black" colspan="8">
            <small class="vsm-f">Nabywca (Pasażer)</small>
        </td>
    </tr>

    <tr>
        <td colspan="8" style="border-left:1px solid black;padding-left:3px"><b>' . $ticket['carrier']['description'] . '</b></td>
        <td colspan="2"></td>
        <td  style="border-left:1px solid black;padding-left:3px" colspan="6"><b style="font-size:14px">' . $ticket['name'] . '  ' . $ticket['surname'] . '</b></td>
        <td style="border-right: 1px solid black" colspan="2"></td>
    </tr>

    <tr>
        <td colspan="8" style="border-left:1px solid black;padding-left:3px"><small class="vsm-f">' . $ticket['carrier']['address'] . '</small></td>
        <td colspan="2"></td>
        <td style="border-left:1px solid black;padding-left:3px" colspan="6"><small>email : ' . $ticket['email'] . '</small></td>
        <td style="border-right: 1px solid black" colspan="2"></td>
    </tr>

    <tr>
        <td colspan="3" style="border-left:1px solid black;padding-left:3px"><small class="vsm-f">Tel: +48 146 371 777</small></td>
        <td colspan="3"></td>
        <td colspan="4"><b class="vsm-f">' . $ticket["MarketDescription"] . '</b></td>
        <td style="border-right:1px solid black;border-left:1px solid black;padding-left:3px" colspan="8"><small>Tel: ' . $ticket['phone'] . '</small></td>
    </tr>

    <tr>
        <td colspan="2" style="border-left:1px solid black; border-bottom:1px solid black; border-top:1px solid black;padding-left:3px">
            <small>Oferent:</small>
        </td>
        <td colspan="8" style="border-bottom:1px solid black; border-top:1px solid black;"><small style="font-size: 10px;"> ' . $ticket["Bidder"] . '</small></td>
        <td style="border:1px solid black;border-right: 0px;padding-left:3px" colspan="5">
            <b style="font-size: 11px;">Telefon do kierowcy:</b>
        </td>
        <td style="border-top:1px solid black;border-right: 1px solid black" align="right" colspan="3"><!--<small>+48146371777,+48146371777</small>--></td>
    </tr>

    <tr>
    <tr>
        <td class="gray no-borders-gray" style="border-top:1px solid black;border-right: 1px solid black;border-left:1px solid black" colspan="15" align="center">
            <span style="font-size:12px">Nazwa linii(kursu): </span>
            <b>' . $ticket["begin_station_locality"] . ' - ' . $ticket["end_station_locality"] . '</b>
        </td>
        <td style="border-right: 1px solid black" align="right" colspan="3" rowspan="1"><!-- <small>+48146371777,+48146371777</small>--></td>
    </tr>

    <tr style="padding:0px">
        <td class="gray no-borders-gray" style="border-left:1px solid black;padding-left:3px" colspan="2"><small class="vsm-f">Data</small></td>
        <td class="gray no-borders-gray" colspan="2" align="right" style="padding-right: 8px;"><small class="vsm-f">Godzina</small></td>
        <td class="gray no-borders-gray" colspan="4" style="padding:0"><small class="vsm-f">Miejscowość</small></td>
        <td class="gray no-borders-gray" colspan="2"></td>
        <td class="gray no-borders-gray" colspan="2" align="right"><small class="vsm-f">Długość trasy :</small></td>
        <td class="gray no-borders-gray" colspan="1" align="right"></td>
        <td style="border-right: 1px solid black" class="gray no-borders-gray" colspan="2"><b>' . $ticket['distance'] . ' km</b></td>
        <td colspan="3" style="border-right: 1px solid black"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px" class="gray no-borders-gray" colspan="2"><b>' . explode(' ', $ticket['departure'])[0] . '</b></td>
        <td class="gray no-borders-gray" colspan="2" align="right" style="padding-right: 8px"><b>' . date_create_from_format('Y.m.d H:i:s', $ticket['departure'])->format('H:i') . '</b></td>
        <td class="gray no-borders-gray" colspan="4"><b>' . $ticket['locality_from'] . '</b></td>
        <td class="gray no-borders-gray" colspan="2"></td>
        <td style="border-right: 1px solid black" class="gray no-borders-gray" colspan="5"></td>
        <td style="border:1px solid black;" colspan="3" rowspan="13"><img src="' . getQrCode($ticket) . '"></td>

    </tr>

    <tr>
        <td style="border-left:1px solid black;" class="gray no-borders-gray" colspan="2"></td>
        <td colspan="2" class="gray no-borders-gray"></td>
        <td class="gray no-borders-gray" colspan="6"><small>' . $ticket['station_from'] . '</small></td>
        <td class="gray no-borders-gray" colspan="5"></td>
        <td style="border-right: 1px solid black" colspan="1"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px" class="gray no-borders-gray" colspan="2"><b>' . explode(' ', $ticket['arrival'])[0] . '</b></td>
        <td class="gray no-borders-gray" colspan="2" align="right" style="padding-right: 8px"><b>' . date_create_from_format('Y.m.d H:i:s', $ticket['arrival'])->format('H:i') . '</b></td>
        <td class="gray no-borders-gray" colspan="7"><b>' . $ticket['locality_to'] . '	</b></td>
        <td class="gray no-borders-gray" colspan="2"></td>
        <td class="gray no-borders-gray" colspan="2"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black; border-bottom: 1px solid black" class="gray no-borders-gray" colspan="2"></td>
        <td style="border-bottom:1px solid black;" colspan="2" class="gray no-borders-gray"></td>
        <td style="border-bottom:1px solid bottom;" class="gray no-borders-gray" colspan="7"><small>' . $ticket['station_to'] . '</small></td>
        <td class="gray no-borders-gray" colspan="2"></td>
        <td style="border-bottom:1px solid bottom;" class="gray no-borders-gray" colspan="2"></td>
    </tr>


    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px; vertical-align: top;" colspan="1"><small class="vsm-f">Rodzaj biletu</small></td>
        <td colspan="6" style="vertical-align: top;"><b class="vsm-f">' . getTicketTypePl($ticket) . '</b></td>
        <td colspan="2" style="vertical-align: top;"><small class="vsm-f">Ilość pasażerów</small></td>
        <td style="border:1px solid black" rowspan="2" width="5px" align="center"><b>' . $ticket['count']['count'] . '</b></td>
        <td style="border-left:1px solid black;padding-left:3px" colspan="5"><small class="vsm-f">Cena:</small></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;border-bottom: 1px solid black;padding-left:3px" ><b class="vsm-f"></b></td>
        <td style="border-bottom:1px solid black" colspan="5"></td>
        <td style="border-bottom:1px solid black" colspan="4"></td>
        <td style="border-left:1px solid black" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px" colspan="4"><small class="vsm-f">Uwagi</small></td>
        <td colspan="6"></td>
        <td style="border-left:1px solid black;padding-left:3px" colspan="1"><small class="vsm-f">Netto PL</small></td>
        <td colspan="3"></td>
        <td colspan="1" align="right" style="padding-right: 3px"><b class="vsm-f">' . $net_price . ' zł</b></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;border-bottom:1px solid black" colspan="10"></td>
        <td style="border-left:1px solid black;padding-left:3px" colspan="1"><small class="vsm-f">VAT 8% PL</small></td>
        <td colspan="3"></td>
        <td colspan="1" align="right" style="padding-right: 3px"><b class="vsm-f" style="font-weight: bold">' . $vat_price . ' zł</b></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px" colspan="10"><small class="vsm-f">Administrator Platformy Komunikacji Samochodowej PKS.pl</small></td>
        <td colspan="3" style="border-left:1px solid black;padding-left: 3px"><small class="vsm-f">' . $brutto["text"] . '</small></td>
        <td colspan="2" align="right" style="padding-right: 3px;"><b class="vsm-f">' . $brutto["price"] . '</b></td>
    </tr>

    <tr>
        <td colspan="3" style="border-left:1px solid black;padding-left:3px"><small class="vsm-f">NIP: 6750001219</small></td>
        <td style="padding: 5px" colspan="7"></td>
        <td style="border-left:1px solid black;" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black"></td>
        <td colspan="9"></td>
        <td style="border-left:1px solid black" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black"></td>
        <td style="border-left:1px solid black;" colspan="9"></td>
        <td style="border-left:1px solid black" colspan="1"></td>
        <td colspan="3"></td>
        <td colspan="1"></td>
    </tr>

    <tr >
        <td style="border:1px solid black" rowspan="1" colspan="1" width="90px" align="center"><img src="./img/mda_logo.png" height="30px"></td>
        <td style="border-left:1px solid black; border-bottom: 1px solid black;padding-left:3px" colspan="9"><b class="vsm-f">Małopolskie Dworce Autobusowe S.A.</b><br>
            <small class="vsm-f">31-514 Kraków, al. Beliny-Prażmowskiego 6A/6</small>
        </td>
        <td style="border-left:1px solid black;border-bottom: 1px solid black; vertical-align: bottom;padding-left: 3px" colspan="2"><b style="font-size: 14px">Razem</b></td>
        <td style="border-bottom: 1px solid black" colspan="2"></td>
        <td style="border-bottom: 1px solid black; vertical-align: bottom; padding-right: 3px" colspan="1" align="right"><b style="font-size:14px">' . $total_price . ' zł</b></td>
    </tr>
</table>
<div class="indent"></div>
        ';

        }
    }

    return $html;
}
