<?php
$type = $_GET['type'];
$filename = basename($_GET['file']);
$ticketNumber = $_GET['number'];
$t_id = $_GET['transaction'];

if ($ticketNumber) {
    include_once('invoice.php');
    $connection = mysql_connect("localhost", "pkskrakow_4", "Dond33st4S!");
    $db = mysql_select_db("pkskrakow_4");
    mysql_query("SET NAMES 'utf8'");

    if (!$connection || !$db) {exit(mysql_error());}
    $select = "SELECT t.* FROM transactions t left JOIN userdata u on u.idUser = t.user_id WHERE t.id = " . $t_id;
    $query = mysql_query($select);
    $transaction = mysql_fetch_assoc($query);

    if (!$transaction) {
        //error
        exit('transaction id not found');
    }

    generatePDF($transaction, false, $ticketNumber);

    $filedir = 'tickets/bilet_'  . $transaction['id'] . '_' . $ticketNumber;
    
} else if ($type == 't') {
    $filedir = 'tickets/' . $filename;
}

if(!$filedir){ // file does not exist
    die('file not found');
} else {
    if ($ticketNumber) $filename = "bilet";
    
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$filename.pdf");
    header("Content-Type: application/pdf");
    header("Content-Transfer-Encoding: binary");

    // read the file from disk
    readfile($filedir);
}