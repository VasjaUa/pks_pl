<?php
include_once ("header.php");
include_once ('invoice.php');

$connection = mysql_connect("localhost", "pkskrakow_4", "Dond33st4S!");
$db = mysql_select_db("pkskrakow_4");
mysql_query("SET NAMES 'utf8'");

if (!$connection || !$db) {
    exit(mysql_error());
}

if (isset($_COOKIE['logged'])) {

    $id = $_COOKIE['logged'];
    $query = mysql_query("SELECT * FROM `userdata` WHERE `idUser` LIKE '$id'");
    $resulted = mysql_fetch_assoc($query);
}

// for transaction complete
$select = "SELECT t.*, i.id as invoice_id FROM transactions t left JOIN invoices i on i.transaction_id = t.id WHERE t.id = " . $_GET['my_session_id'];
$query = mysql_query($select);
$transaction = mysql_fetch_assoc($query);

if ($transaction && $transaction['status'] == 1) {
    $sql = "update transactions set status = 2 where id = " . $transaction['id'];
    $query = mysql_query($sql);
} else if (!$transaction['p24_order_id']){
    header("Location:/");
}

?>
<link rel="stylesheet" href="<?= SERVER_URL ?>/css/style_faktura.css">
<div class="block-header">
    <div class="container">
        <div class="block-title">Bilet</div>
    </div>
</div>

<?php if($transaction && $transaction['status'] !== 1): ?>
    <div class="tab_block_ticket">
        <div class="tab_header_panel">
            <div class="container">
                <ul class="tickets_page_nav nav nav-tabs" style="display: none;">
                    <li class="active" data-tab="tab_content_1" data-toggle="tab" aria-expanded="true">Bilet</li>
                    <?php if($transaction['invoices'] != 'false'): ?>
                        <li data-tab="tab_content_2" data-toggle="tab" aria-expanded="false">Faktura</li>
                    <?php endif ?>
                </ul>
            </div>
        </div>

        <div class="tab_content active" id="tab_content_1">
            <div id="buttonWrapper" style="overflow: hidden;  text-align: center" class="animate fadeInLeft"><!--display:none;-->
            <span id="pdfBiletText" class="pdfTitle">
                Teraz możesz pobrać bilet
            </span>
                <div class="img_block">Bilet zostanie pobrany w formacie *.pdf</div>

                <a onclick="return false" href="download.php?type=t&file=bilet_<?= $transaction['id'] ?>" download="bilet">
                    <button id="downloadBilet" class="m-progress btn_download_pdf btn btn-lg btn-primary"
                            onclick="">Pobierz bilet
                    </button>
                </a>
            </div>
        </div>

        <?php if($transaction['invoices'] != 'false'): ?>
            <div class="tab_content" id="tab_content_2">
                <div id="buttonWrapper" style="overflow: hidden; text-align: center" class="animate fadeInLeft">
                <span id="pdfFakturaText" class="pdfTitle">
                    Teraz możesz pobrać fakturę
                </span>
                    <div class="img_block">faktura zostanie pobrany na komputer w formacie *.pdf</div>
                    <a onclick="return false" href="/generate-save-pdf.php?type=i&file=faktura_<?= $transaction['id'] ?>" data-session_id="<?= $transaction['id'] ?>" >
                        <button id="downloadFaktura" class="m-progress btn_download_pdf btn btn-lg btn-primary"
                                onclick="">Pobierz fakturę
                        </button>
                    </a>
                </div>
            </div>
        <?php endif ?>

    </div>
<?php else : ?>
<div style="    text-align: center;
    position: relative;
    padding-top: 10%;
    padding-bottom: 10%;">
    <h4>Wystąpił błąd. Powtarzaj to ponownie.</h4>
</div>
<?php endif ?>
<div class="wrap-middle ticket-footer">
    <?php include_once('footer.php') ?>
</div>
<div id="footer">
    <div class="bg-contact"></div>
    <div class="footer">
        <div class="container">
            <div class="row-f">
                <div>
                    <div class="footer-carousel">
                        <div class="carousel-company">
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-1.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-2.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-3.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-4.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-5.png" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-6.jpg" alt=""></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-text">
                <div>Nasz serwis wykorzystuje pliki cookies. Korzystanie z witryny oznacza zgodę na ich zapis lub odczyt zgodnie z ustawieniami przeglądarki.
                </div>
                <div class="copy">Copyright © MDA S.A Kraków</div>
            </div>
        </div>
    </div>
</div>
<script src="libs/jquery/jquery-1.9.1.min.js"></script>
<script src="libs/bootstrap/js/bootstrap.min.js"></script>
<script src="libs/waypoints/waypoints.min.js"></script>
<script src="libs/animate/animate-css.js"></script>
<script src="libs/plugins-scroll/plugins-scroll.js"></script>
<script src="libs/WOW/wow.js"></script>
<script src="libs/slick/slick.min.js"></script>
<script src="libs/slicknav/jquery.slicknav.min.js"></script>
<script src="libs/equalheight/jquery.equalheight.js"></script>
<script src="libs/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.js"></script>
<script src="libs/ui/jquery.ui.timepicker-pl.js"></script>
<script src="libs/ui/datepicker-pl.js"></script>
<script src="libs/ui/datepicker-de.js"></script>
<script src="libs/ui/jquery.ui.timepicker.js"></script>
<script src="libs/html2canvas/html2canvas.min.js"></script>
<script src="libs/jsPDF/jspdf.min.js"></script>
<script src="libs/jsPDF/from_html.js"></script>
<script src="libs/jsPDF/standard_fonts_metrics.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/purify.min.js"></script>
<script src="libs/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="libs/bootstrap-fileinput/themes/fa/theme.js"></script>
<script src="libs/bootstrap-fileinput/js/locales/pl.js"></script>
<script src="libs/jq-validation/jquery.validate.min.js"></script>
<script src="libs/jq-validation/additional-methods.min.js"></script>
<script src="libs/sweetalert/sweetalert.min.js"></script>
<script src="libs/makePDF/pdfmake.min.js"></script>
<script src="libs/makePDF/vfs_fonts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
<script src="libs/jquery-cookie/cookie.js"></script>
<script src="js/common.js"></script>
<script src="js/account.js"></script>
<script src="js/registration.js"></script>
<!--<script src="js/ticket.js"></script>-->
<?php
include_once ('./helpers/ticket/InvoiceHelper.php');
$invoices = new InvoiceHelper();
$invoices->getHtml();
 ?>

<script>
    var invoices = JSON.parse(localStorage.getItem('invoices_c'));

    if (invoices) { // скритие кнопки
        $('.tickets_page_nav').attr('style', 'display:table')
    }

    $('.btn-bascket-user').css('display', 'none');

    localStorage.setItem('check', 'test');
    localStorage.setItem("courses", null);
    localStorage.setItem("features", null);

    $('.tickets_page_nav li').click(function () {
        var tabLi = $(this).data('tab');
        $('.tab_block_ticket .tab_content').removeClass('active');
        $('.tab_block_ticket .tab_content#' + tabLi).addClass('active');
        $('.tickets_page_nav li').removeClass('active');
        $(this).addClass('active');
    });

    var download = $('#downloadBilet');
    var download2 = $('#downloadFaktura');

    setTimeout(function () {
        download.removeClass("m-progress");
        download2.removeClass("m-progress");
        download.parent().removeAttr("onclick");
        download2.parent().removeAttr("onclick");
    }, 2000)

    /**
     * Preventing this page to make "back" in browser
     **/
    (function (global) {

        if (typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }

        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };

        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };

        global.onload = function () {
            noBackPlease();
        };

        noBackPlease();

    })(window);

</script>
</body>
</html>