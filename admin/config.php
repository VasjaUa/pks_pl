<?php
function sanitize_output($buffer)
{
    $search = array(
        '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
        '/[^\S ]+\</s',  // strip whitespaces before tags, except space
        '/(\s)+/s'       // shorten multiple whitespace sequences
        );
    $replace = array(
        '>',
        '<',
        '\\1'
        );
    $buffer = preg_replace($search, $replace, $buffer);
    return $buffer;
}
//ob_start("sanitize_output");
define('JB_VERSION', '2.5.2.1');
//--MySQL SETTING--//////////////
define('DB_HOST_SITE','localhost'); // хост
define('DB_USER_SITE','pkskrakow_4'); // имя пользователя
define('DB_PASSWORD_SITE','Dond33st4S!'); // пароль
define('DB_NAME_SITE','pkskrakow_4'); // имя базы данных
define('DB_ENCODE_SITE','utf8');

//admin email

	define('ADMIN_EMAIL', 'eloner3@gmail.com');


//site_name

	define('SITE_NAME', 'MDA');


//--SECTIONS--//

	define('SECTIONS', 1);

	define('SET_URL_TEMPLATE_SECTIONS', '{1}');

//--SUBSECTIONS--//

	define('SUBSECTIONS', 1);

	define('SET_URL_TEMPLATE_SUBSECTIONS', '{1}/{2}');


//--Products--//

	define('PRODUCTS', 1);


	define('SET_URL_TEMPLATE_PRODUCTS', '{1}/product-{2}');


//--Categories--//

	define('PRODUCT_CATS', 0);


	define('SET_URL_TEMPLATE_PRODUCT_CATS', 'category-{1}');


//--ORDERS--//

	define('ORDERS', 1);


//--Customers--//

	define('CUSTOMERS', 0);


//--Gallery--//

	define('GALLERIES', 0);


	define('SET_URL_TEMPLATE_GALLERIES', '{1}/gallery-{2}');


//--Gallery Category--//

	define('GALLERY_CATS', 0);


	define('SET_URL_TEMPLATE_GALLERY_CATS', 'galleries-{1}');


//--Blog--//

	define('ARTICLES', 0);


	define('SET_URL_TEMPLATE_ARTICLES', '{1}/article-{2}');


//--Blog Category--//

	define('ARTICLE_CATS', 0);


	define('SET_URL_TEMPLATE_ARTICLE_CATS', 'articles-{1}');


//--Blog Comments--//

	define('ARTICLE_COMMENTS', 0);


//--Blog Tags//

	define('ARTICLE_TAGS', 0);


//--Testimonials--//

	define('TESTIMONIALS', 0);


	define('SET_URL_TEMPLATE_TESTIMONIALS', 'testimonials-{1}');


//--News--//

	define('NEWS', 0);


	define('SET_URL_TEMPLATE_NEWS', 'news-{1}');


//--Media--//

	define('MEDIA', 0);


	define('SET_URL_TEMPLATE_MEDIA', 'media-{1}');


//--Slider_1--//

	define('SLIDER_1', 'Homepage Slider');

//--Slider_2--//

	define('SLIDER_2', 0);

//--Slider_3--//

	define('SLIDER_3', 0);


//--Banner_1--//

	define('BANNER_1', 0);

//--Banner_2--//

	define('BANNER_2', 0);

//--Banner_3--//

	define('BANNER_3', 0);


//--Subscribers--//

	define('SUBSCRIBERS', 0);


//--FAQ--//

	define('FAQ', 0);
?>