<?php

Include_once ('MSQL.php');

Class Model
{
	// ссылка на экземпляр класса
	private static $instance; 
	
	// драйвер БД
	private $msql;


	// получение экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			{
				self::$instance = new Model();
			}
		
		return self::$instance;
	}
	
	// конструктор
	public function __construct()
	{
		$this->msql = MSQL::Instance();
	}
	
//--ALL MODULES--///////////////////////////////////////////////////////////////

	// обрезка текста
	public function ShortText($text, $col) {
		$newText = mb_substr($text, 0, $col, 'utf-8');
		if (strlen($text) > $col) {
			$newText.= ' ...';
		}
		return $newText;
	}
	
	// генирация случайной строки
	private function GenerateStr($length = 10) 
	{
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789';
		$code = '';
		$clen = strlen($chars) - 1;  

		while (strlen($code) < $length) 
			$code .= $chars[mt_rand(0, $clen)];  

		return $code;
	}
	
	// загругка картинки
	public function Upload_image($prefix, $file)
	{
		$path_info = pathinfo($file['name']);
		$mime = mb_strtolower($path_info['extension'], 'utf-8');
		if ($mime == 'jpg' || $mime == 'jpeg' || $mime == 'gif' || $mime == 'png' || $mime == 'pdf' || $mime == 'mp3')
		{
			$rand = $this->GenerateStr(10);
			$filename = $prefix.'_'.$rand.'.'.$mime;
			if(copy($file['tmp_name'], '../pictures/'.$filename))
			{
				$image_name = $filename;
			}
			else
			{
				$image_name = '';
			}
		}
		
		return $image_name;
	}
	
	// загругка AUDIO
	public function Upload_audio($prefix, $file)
	{
		$path_info = pathinfo($file['name']);
		$mime = mb_strtolower($path_info['extension'], 'utf-8');
		if ($mime == 'mp3')
		{
			$rand = $this->GenerateStr(10);
			$filename = $prefix.'_'.$rand.'.'.$mime;
			if(copy($file['tmp_name'], '../music/'.$filename))
			{
				$audio_name = $filename;
			}
			else
			{
				$audio_name = '';
			}
		}
		
		return $audio_name;
	}
	
	// обновление AUDIO
	public function Update_audio($prefix, $file, $oldfile)
	{
		$path_info = pathinfo($file['name']);
		$mime = mb_strtolower($path_info['extension'], 'utf-8');
		if ($mime == 'mp3')
		{
			$rand = $this->GenerateStr(10);
			$filename = $prefix.'_'.$rand.'.'.$mime;
			if(copy($file['tmp_name'], '../music/'.$filename))
			{
				$audio_name = $filename;
				if ($oldfile != '' && file_exists('../music/'.$oldfile))
				{
					unlink('../music/'.$oldfile);
				}					
			}
			else
			{
				$audio_name = $oldfile;
			}
		}
		
		return $audio_name;
	}
	

	
	// запрос строки из базы данных
	public function Row_by_id($table, $id)
	{
		// запрос к базе
		$t = 'SELECT * FROM %s WHERE id = %d';
		$query = sprintf ($t, $table, $id);
		
		return $this->msql->Select_string($query);		
	}
	
	// запрос строки из базы данных
	public function Row_where($table, $where)
	{
		// запрос к базе
		$t = 'SELECT * FROM %s %s';
		$query = sprintf ($t, $table, $where);
		
		return $this->msql->Select_string($query);		
	}
	
	// запрос строк из базы данных
	public function All_rows($table)
	{
		// запрос к базе
		$t = 'SELECT * FROM %s';
		$query = sprintf ($t, $table);
		
		return $this->msql->Select($query);		
	}
	
	// запрос массива данных из базы
	public function Array_where($table, $where)
	{
		// запрос к базе
		$t = 'SELECT * FROM %s %s';
		$query = sprintf ($t, $table, $where);

		return $this->msql->Select($query);		
	}
	public function Array_clean($where)
	{
		return $this->msql->Select($where);		
	}

	// запрос количества строк
	public function Num_where($table, $where)
	{
		// запрос к базе
		$t = 'SELECT * FROM %s %s';
		$query = sprintf ($t, $table, $where);
		
		return $this->msql->Select_num($query);		
	}	
	
	// добавление
	public function Add($table, $object)
	{	
		return $this->msql->Insert($table, $object);
	}
	
	// обновление
	public function Edit_by_id($table, $id, $object)
	{
		$t = 'id = "%d"';		
		$where = sprintf($t, $id);
		
		$this->msql->Update($table, $object, $where);
		
		return true;
	}
	
	// обновление
	public function Edit_where($table, $object, $where)
	{	
		$where = sprintf($where);
	
		$this->msql->Update($table, $object, $where);
		
		return true;
	}

	// удаление
	public function Delete_by_id($table, $id)
	{	
		$t = 'id = "%d"';		
		$where = sprintf($t, $id);
		
		return $this->msql->Delete($table, $where);
	}
	
	// удаление
	public function Delete_where($table, $where)
	{					
		return $this->msql->Delete($table, $where);
	}

	public function IndexBy($array,$index){
		$new_arr = array();
		foreach($array as $item){
			$new_arr[$item[$index]] = $item;
		}
	return $new_arr;
	}


	// обновление позиций
	public function Edit_positions ($table, $sortdata)
	{	
		$data = explode(',',$sortdata);
		
		$num = 0;
		$position = 1;
		foreach ($data as $k => $v)
		{
			$str[] = $v;
			
			// отправка запроса
			$object = array();
			$object['position'] = $position;		
			$t = "id = '%d'";		
			$where = sprintf($t, $str[$num]);		
			$this->msql->Update($table, $object, $where);
			
			$num = $num + 1;
			$position = $position + 1;
		}
		
		return true;
	}
}