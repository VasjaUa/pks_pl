<?php
/**
 * @var $table string
 * @var $columns array
 * @var $images array
 * @var $categories array
 * @var $product_in_cats array
 */
?>
<div class="wrapper">
    <div class="wrapper" data-table-name="<?=$table?>" >
    <form data-edit-create="<?=$table?>" method='POST' class='clearfix' enctype="multipart/form-data">
        <!-- якщо це редагування то виводиться id -->
        <input type="hidden" value="<?=$columns['id']?>" name="<?=$table?>[id]">

        <section>

            <!-- виводиться назва таблиці, та дія (створення, редагування) -->
            <span class="h1"><?= constant(strtoupper($table))==1 ? strtoupper($table) : constant(strtoupper($table));?> <?=$_GET['id'] ? 'edit' : 'add';?>

            </span>


            <div class="row">
                <?php if($settings['name']['on_off']):?>
    <!-- name -->
                    <div class="col_6">
                        <div class="pre_input"><?=$settings['name']['field_title'] ? $settings['name']['field_title'] : "name"?></div>
                        <input type="text" name="<?=$table?>[name]" class="input <?=$settings['name']['field_style'] ? $settings['name']['field_style'] : ""?>" id="name" value="<?=$columns['name']?>">
                    </div>
                <?php endif;?>
                <?php if($settings['categories']['on_off']):?>
    <!-- categories -->
                    <div class="col_6">
                        <div class='pre_input'><?=$settings['categories']['field_title'] ? $settings['categories']['field_title'] : "Categories";?></div>
                        <div class="add_tags || clearfix">
                            <select name='cats_in' class="input <?=$settings['categories']['field_style'];?>">
                                <option value="0" style="display: none;" selected>Select</option>
                                <?php foreach($categories as $cat): ?>
                                    <option value="<?=$cat['id']?>"><?=$cat['name']?></option>
                                <?php endforeach; ?>
                            </select>

                            <button data-add-cat class="button">add</button>
                        </div>

                        <div data-cat-in class='tag_spans'>
                            <?php foreach ($product_in_cats as $cat): ?>
                                <input type="hidden" name="a_cat[]" value="<?= $cat['id_category'] ?>" data-val="<?= $cat['id_category'] ?>">
                                <span data-val="<?= $cat['id_category'] ?>"><?= $categories[$cat['id_category']]['name'] ?> <a data-delete-cat href="#"><svg class='icon'><use xlink:href='/admin/View/img/svgdefs.svg#icon_close'></use></svg></a></span>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif;?>
            <div class="row">
                <?php if($settings['price']['on_off']):?>
    <!-- price -->
                    <div class="col_6 ">
                        <div class="pre_input"><?=$settings['price']['field_title'] ? $settings['price']['field_title'] : "Price"?></div>
                        <input type="text" name="<?=$table?>[price]" class="input <?=$settings['price']['field_style']?>" id="price" value="<?=$columns['price']?>">
                    </div>
                <?php endif;?>
                <?php if($settings['checkbox']['on_off']):?>
                    <div class="col_6" style="padding-top: 3.5rem;">
    <!-- checkbox -->
                        <input name='<?=$table ?>[checkbox]' type='checkbox' class="checkbox <?=$settings['checkbox']['field_style'];?>" value='<?=$columns['checkbox']?>' id='<?=$table ?>[checkbox]' <?=$columns['checkbox']?'checked':''?>>
                        <label for='<?=$table ?>[checkbox]'><?=$settings['checkbox']['field_title'] ? $settings['checkbox']['field_title'] : "checkbox";?></label>
                    </div>
                <?php endif;?>
            </div>
            <div class="row">
                <?php if($settings['thumbnail']['on_off']):?>
    <!-- thumbnail -->
                    <div class="col_6 upload_file">
                        <!--file-->
                        <div data-table="<?=$table?>" data-field="thumbnail" data-<?=$table . '_thumbnail'?> class='pre_input' data-image-url="../pictures/<?=$table?>/<?=$columns['thumbnail']?>"><?=$settings['thumbnail']['field_title'] ? $settings['thumbnail']['field_title'] : "thumbnail"?>
                            <span title='Delete image' class='delete_thumbnail'><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_trash'></use></svg></span>
                            <span title='Show current image' class='watch_thumbnail'><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_eye'></use></svg></span>
                        </div>
                        <input data-table="<?=$table?>" data-field="thumbnail" type="file" name='thumbnail' id='<?=$table . '_thumbnail'?>' class="upload <?=$settings['thumbnail']['field_style'] ? $settings['thumbnail']['field_style'] : ""?>" data-multiple-caption="{count} files selected">
                        <label class='last_item' for="<?=$table . '_thumbnail'?>"><span class='file_name'><?=$columns['thumbnail']?></span><span class='file_deleted'>Deleted</span><strong><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_upload'></use></svg></svg>Choose a file</strong></label>
                        <?php if ($columns['thumbnail']): ?>
                            <script>$('[data-<?=$table . '_thumbnail'?>] .delete_thumbnail, [data-<?=$table . '_thumbnail'?>] .watch_thumbnail').addClass('active')</script>
                        <?php endif; ?>

                    </div>
                <?php endif;?>
                <?php if($settings['background']['on_off']):?>
                    <div class="col_6 upload_file">
    <!-- background -->
                        <!--file-->
                        <div data-table="<?=$table?>" data-field="background" data-<?=$table . '_background'?> class='pre_input' data-image-url="/pictures/<?=$table?>/<?=$columns['background']?>"><?=$settings['background']['field_title'] ? $settings['background']['field_title'] : "background"?>
                            <span title='Delete image' class='delete_thumbnail'><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_trash'></use></svg></span>
                            <span title='Show current image' class='watch_thumbnail'><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_eye'></use></svg></span>
                        </div>
                        <input data-table="<?=$table?>" data-field="background" type="file" name='background' id='<?=$table . '_background'?>' class="upload <?=$settings['background']['field_style'] ? $settings['background']['field_style'] : ""?>" data-multiple-caption="{count} files selected">
                        <label class='last_item' for="<?=$table . '_background'?>"><span class='file_name'><?=$columns['background']?></span><span class='file_deleted'>Deleted</span><strong><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_upload'></use></svg></svg>Choose a file&hellip;</strong></label>
                        <?php if ($columns['background']): ?>
                            <script>$('[data-<?=$table . '_background'?>] .delete_thumbnail, [data-<?=$table . '_background'?>] .watch_thumbnail').addClass('active')</script>
                        <?php endif; ?>
                    </div>
                <?php endif;?>
            </div>
            <?php if($settings['value']['on_off']):?>
    <!-- value -->
                <div class="row">
                    <!--ckeditor-->
                    <div class='clearfix'></div>
                    <div class='pre_input'><?=$settings['value']['field_title'] ? $settings['value']['field_title'] : "value"?></div>
					<textarea <?=($settings['value']['field_style'] != 'ckeditor' ? "class='".$settings['value']['field_style']."'" : "data-ckeditor id='".$table."_value'")?> name="<?=$table?>[value]"><?=$columns['value']?></textarea>
                </div>
            <?php endif;?>
            <?php if($settings['value1']['on_off']):?>
    <!-- value1 -->
                <div class="row <?=$settings['value1']['on_off']?"":"none"?>">
                    <!--ckeditor-->
                    <div class='clearfix'></div>
                    <div class='pre_input'><?=$settings['value1']['field_title'] ? $settings['value1']['field_title'] : "value1"?></div>
                    <textarea <?=($settings['value1']['field_style'] != 'ckeditor' ? "class='".$settings['value1']['field_style']."'" : "data-ckeditor id='".$table."_value1'")?> name="<?=$table?>[value1]"><?=$columns['value1']?></textarea>
                </div>
            <?php endif;?>
            <?php if($settings['value2']['on_off']):?>
    <!-- value2 -->
                <div class="row">
                    <!--ckeditor-->
                    <div class='clearfix'></div>
                    <div class='pre_input'><?=$settings['value2']['field_title'] ? $settings['value2']['field_title'] : "value2"?></div>
                    <textarea <?=($settings['value2']['field_style'] != 'ckeditor' ? "class='".$settings['value2']['field_style']."'" : "data-ckeditor id='".$table."_value2'")?> name="<?=$table?>[value2]"><?=$columns['value2']?></textarea>
                </div>
            <?php endif;?>
            <?php if($settings['value3']['on_off']):?>
    <!-- value3 -->
                <div class="row <?=$settings['value3']['on_off']?"":"none"?>">
                    <!--ckeditor-->
                    <div class='clearfix'></div>
                    <div class='pre_input'><?=$settings['value3']['field_title'] ? $settings['value3']['field_title'] : "value3"?></div>
                    <textarea <?=($settings['value3']['field_style'] != 'ckeditor' ? "class='".$settings['value3']['field_style']."'" : "data-ckeditor id='".$table."_value3'")?> name="<?=$table?>[value3]"><?=$columns['value3']?></textarea>
                </div>
            <?php endif;?>

            <div class="row">
                <?php if($settings['field_1']['on_off']):?>
                    <div class="col_6">
    <!-- field_1 -->
                        <div class='pre_input'><?=$settings['field_1']['field_title'] ? $settings['field_1']['field_title'] : "field_1"?></div>
                        <input type='text' name='<?=$table ?>[field_1]' class='input <?=$settings['field_1']['field_style'] ? $settings['field_1']['field_style'] : ""?>' value="<?=$columns['field_1']?>">
                    </div>
                <?php endif;?>
                <?php if($settings['field_3']['on_off']):?>
                    <div class="col_6">
    <!-- field_3 -->
                        <div class='pre_input'><?=$settings['field_3']['field_title'] ? $settings['field_3']['field_title'] : "field_1"?></div>
                        <input type='text' name='<?=$table ?>[field_3]' class='input <?=$settings['field_3']['field_style'] ? $settings['field_3']['field_style'] : ""?>' value="<?=$columns['field_3']?>">
                    </div>
                <?php endif;?>
            </div>
            <div class="row">
                <?php if($settings['field_2']['on_off']):?>
                    <div class="col_6">
    <!-- field_2 -->
                        <div class='pre_input'><?=$settings['field_2']['field_title'] ? $settings['field_2']['field_title'] : "field_2"?></div>
                        <input type='text' name='<?=$table ?>[field_2]' class='input <?=$settings['field_2']['field_style'] ? $settings['field_2']['field_style'] : ""?>' value="<?=$columns['field_2']?>">
                    </div>
                <?php endif;?>
                <?php if($settings['field_4']['on_off']):?>
                    <div class="col_6">
    <!-- field_4 -->
                        <div class='pre_input'><?=$settings['field_4']['field_title'] ? $settings['field_4']['field_title'] : "field_4"?></div>
                        <input type='text' name='<?=$table ?>[field_4]' class='input <?=$settings['field_4']['field_style'] ? $settings['field_4']['field_style'] : ""?>' value="<?=$columns['field_4']?>">
                    </div>
                <?php endif;?>
            </div>

        </section>
        <?php if(isset($_GET['id']) && $settings['images']['on_off']):?>
            <section class='gellery_section'>
                <div class='underlined h2'><?=$settings['images']['field_title'] ? $settings['images']['field_title'] : "Product Images"?>
                    <a href='#' class='clear button || fr || upload_gallery'>Upload<svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_upload'></use></svg></a>
                </div>
                <div class='clearfix || gallery_container'>
                    <?php if ($images): ?>
                        <?php foreach ($images as $image): ?>
                            <div class='gallery_image <?=$settings['images']['field_style']?>' data-image-id="<?=$image['id']?>" id='image_<?=$image['id']?>'>
                                <div class='gallery_bg' style="background-image: url(/admin/image.php?width=250&height=250&cropratio=1:1&image=/pictures/<?=$table?>/<?=$image['image']?>)"></div>
                                <span class='gallery_delete'><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_close'></use></svg></span>
                                <span class='gallery_move'><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_move'></use></svg></span>
                                <div class='gallery_descr'>
                                    <textarea name='image[<?=$image['id']?>]' placeholder='Description' class='input'><?=$image['name'] ?></textarea>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <span class="text">This product gallery is empty</span>
                    <?php endif; ?>
                </div>
            </section>
        <?php endif; ?>
        <a href="#" class="show_meta || primary button || fl">Show Meta Tags</a>
        <section class="meta_tags">
            <div class="underlined h2">Meta Tags</div>
            <div class="row">
                <div class="col_6">
                    <?php if($settings['url']['on_off']):?>
                        <div class="row">
                            <div class="pre_input" data-url-start="<?=$columns['url']?>" data-first-entrance="<?=$url['first-entrance']?>"><?=$settings['url']['field_title'] ? $settings['url']['field_title'] : "META URL"?> <div class="input_count"></div></div>
                            <input type="text" name="<?=$table?>[url]" class="ml_meta || input || to_count <?=$settings['url']['field_style'] ? $settings['url']['field_style'] : ""?>" id="url" value="<?=$columns['url']?>">
                        </div>
                    <?php endif;?>
                    <?php if($settings['title']['on_off']):?>
                        <div class="row">
                            <div class="pre_input"><?=$settings['title']['field_title']?$settings['title']['field_title']:"META TITLE "?><div class="input_count"></div></div>
                            <input type="text" name="<?=$table?>[title]" class="ml_meta || input || to_count <?=$settings['title']['field_style'] ? $settings['title']['field_style'] : ""?>" value="<?=$columns['title']?>">
                        </div>
                    <?php endif;?>
                    <?php if($settings['keywords']['on_off']):?>
                        <div class="row">
                            <div class="pre_input"><?=$settings['keywords']['field_title']?$settings['keywords']['field_title']:"META KEYWORDS "?><div class="input_count"></div></div>
                            <input type="text" name="<?=$table?>[keywords]" class="input || to_count <?=$settings['keywords']['field_style'] ? $settings['keywords']['field_style'] : ""?>" value="<?=$columns['keywords']?>">
                        </div>
                    <?php endif;?>
                    <?php if($settings['description']['on_off']):?>
                        <div class="row">
                            <div class="pre_input "><?=$settings['description']['field_title']?$settings['description']['field_title']:"META DESCRIPTION "?><div class="input_count"></div></div>
                            <textarea name="<?=$table?>[description]" class="ml_meta || input || to_count <?=$settings['description']['field_style'] ? $settings['description']['field_style'] : ""?>"><?=$columns['description']?></textarea>
                        </div>
                    <?php endif;?>

                </div>
                <div class="col_6" style="margin-top: 3rem;">
                    <p class="google_title"><a href="/<?=$url['url']?>"><span class="ml_title" data-input-name="<?=$table?>[title]"><?=$columns['title']?></span></a></p>
                    <p class="google_url"><?=$_SERVER['HTTP_HOST']?>/<span data-input-name="<?=$table?>[url]"><?=$url['url']?></span></p>
                    <p class="google_description"  data-input-name="<?=$table?>[description]">
                        <?=$columns['description']?>
                    </p>
                </div>
            </div>
        </section>
        <button class="button || fr || save_button">Save</button>

        <?php if(isset($_GET['id']) && $_GET['id']!=''):?>
<!--            <a href="/--><?//=$url['url']?><!--" target="_blank" class="button || fr || preview_button">VIEW PAGE</a>-->
        <?php else:?>
            <input type="submit" name="save_close" value="Save and close" class="button || fr || preview_button">
        <?php endif;?>

    </form>
</div>


<div class='upload_container || clearfix'>
    <!-- <a href='#' class='clear button || upload_opened || fr'>Accept</a> -->
    <form action="#" class="dropzone">
        <input type="hidden"  name="gallery" value="1">
        <input type="hidden"  name="upload" value="1">
    </form>
</div>

<script src="js/dropzone.js"></script>
<script>
Dropzone.autoDiscover = false;
    $(".dropzone").dropzone({
        url:  location.href,
        maxFilesize: 3,
        dictFileTooBig: 'File is too big',
        dictDefaultMessage: "<div class='text'>Click, to upload files <br> or just drag and drop them here</div><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_upload'></use></svg>",
        success: function (file, response) {

            res = JSON.parse(response);

            if (res.status == 'error') {
                showErrorMessage(res.message);
            } else {
                var image = JSON.parse(response);

                var html = "<div class='gallery_image' data-image-id='" + image.id + "' id='image_" + image.id + "'>";
                html += "<div class='gallery_bg' style='background-image: url(/admin/image.php?width=250&height=250&cropratio=1:1&image=" + image.image + ")'></div>";
                html += "<span class='gallery_delete'><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_close'></use></svg></span>";
                html += "<span class='gallery_move'><svg class='icon'><use xlink:href='View/img/svgdefs.svg#icon_move'></use></svg></span>";
                html += "<div class='gallery_descr'>";
                html += "<textarea name='image[" + image.id + "]' placeholder='Description' class='input'>" + image.name + "</textarea>";
                html += "</div>";
                html += "</div>";

                $('.gallery_container span.text').hide();
                $('.gallery_container').append(html);
            }
        }
    });
</script>

<!-- Sort stuff -->
<form action="#" id="sformgallery" method="post" data-gallery-sort-pos>
    <input name="sortdatagallery" id="sortdatagallery" type="hidden" value=""/>
</form>

