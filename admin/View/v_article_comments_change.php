<?php
/**
 * @var $table string
 * @var $url array
 * @var $columns array
 * @var $categories array
 */
?>
<div class="wrapper">
    <div class="wrapper" data-table-name="<?=$table?>" data-up-level-id="<?=$columns['id_sect']?>">
        <form data-edit-create="<?=$table?>" action="" method="POST" class="clearfix " enctype="multipart/form-data">

    <!-- якщо це редагування то виводиться id -->
        <input type="hidden" value="<?=$columns['id']?>" name="<?=$table?>[id]">

        <section>

                <!-- виводиться назва таблиці, та дія (створення, редагування) -->
            <span class="h1"><?=$table?> <?=$_GET['id'] ? 'edit' : 'add';?>

            </span>


        <!-- name -->
                <div class="row">
                    <div class="col_6 ">
                        <div class="pre_input">name</div>
                        <input type="text" name="<?=$table?>[name]" class="input" id="name" value="<?=$columns['name']?>">
                    </div>
                    <div class="col_6">
        <!-- article_id -->
                        <div class='pre_input'>Id article</div>
                        <select name='<?=$table ?>[article_id]' class="input">
                            <option value="0" style="display: none;" selected>Select</option>
                            <?php foreach($articles as $article): ?>
                                <option value="<?=$article['id']?>" <?=($article['id'] == $columns['article_id'])?'selected':''?>><?=$article['name']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col_6">
        <!--date-->
                        <div class='pre_input'>date</div>
                        <input type='text' name='<?=$table ?>[date]' class='input || datetimepicker' value="<?=$columns['date']?>">
                    </div>
        <!-- checkbox -->
                    <div class="col_6" style="margin-top: 3rem;">
                        <input value=1" name='<?=$table ?>[checkbox]' type='checkbox' value='<?=$columns['checkbox']?>' id='<?=$table ?>[checkbox]' <?=$columns['checkbox']?'checked':''?>>
                        <label for='<?=$table ?>[checkbox]'>checkbox</label>
                    </div>
                </div>

        <!-- value -->
                <div class="row">
                    <!--ckeditor-->
                    <div class='clearfix'></div>
                    <div class='pre_input'>value</div>
                    <textarea <?=($settings['value']['field_style'] != 'ckeditor' ? "class='".$settings['value']['field_style']."'" : "data-ckeditor id='".$table."_value'")?> name="<?=$table?>[value]"><?=$columns['value']?></textarea>
                </div>

        </section>

        <button class="button || fr || save_button">Save</button>

        </form>
    </div>
</div>