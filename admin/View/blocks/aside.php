<aside class="<?= ($_COOKIE['menu_status'] == 'active') ? "active" : ""?>">

    <a href='/admin' class='logo'>
        <?php include('View/blocks/svg/logo_primary.php'); ?>
    </a>

    <div class='custom_scroll_wrap1'>
        <div class='custom_scroll_bar'></div>
        <nav class='custom_scroll_wrap2'>
            <ul class='custom_scroll'>

                <?php if (constant('SECTIONS')) : ?>
                    <?php if ($_SESSION['authorize']['status'] == 1) : ?>
                    <li data-menu-name="sections">

                        <a href='/admin/?t=sections&c=select&page=1'>
                            <svg class='icon || left_icon'><use xlink:href='View/img/svgdefs.svg#icon_category'></use></svg>
                            <span><?php if(SECTIONS == 1){echo 'Main Sections';}else{echo SECTIONS;} ?></span>
                        </a>
                    </li>
					<?php else: ?>
					<li data-menu-name="sections">

                        <a href='' class='has_submenu'>
                            <svg class='icon || left_icon'><use xlink:href='View/img/svgdefs.svg#icon_category'></use></svg>
                            <svg class='icon || right_icon'><use xlink:href='View/img/svgdefs.svg#icon_chevron_down'></use></svg>
                            <span><?php if(SECTIONS == 1){echo 'Sections';}else{echo SECTIONS;} ?></span>
                        </a>
						<ul>
                                <li class='submenu_head'><?php if(SECTIONS == 1){echo 'Sections';}else{echo SECTIONS;} ?></li>
                                <?php foreach($sections as $section):?>
                                    <li data-menu-up-level="<?=$section['id']?>"><a href='/admin//?t=sections&c=change&id=<?=$section['id']?>'><?=$section['name']?></a></li>
                                <?php endforeach;?>
                            </ul>
                    </li>					
					<?php endif ?>
                <?php endif; ?>

                <?php if (constant('SUBSECTIONS')) : ?>
                    <li data-menu-name="subsections">
                        <a href='' class='has_submenu'>
                            <svg class='icon || left_icon'>
                                <use xlink:href='View/img/svgdefs.svg#icon_subcategory'></use>
                            </svg>
                            <svg class='icon || right_icon'>
                                <use xlink:href='View/img/svgdefs.svg#icon_chevron_down'></use>
                            </svg>
                            <span><?php if(SUBSECTIONS == 1){echo 'Subsections';}else{echo SUBSECTIONS;} ?></span>
                        </a>
                        <?php if ($sections_sub != null) : ?>
                            <ul>
                                <li class='submenu_head'><?php if(SUBSECTIONS == 1){echo 'Subsections';}else{echo SUBSECTIONS;} ?></li>
                                <?php foreach($sections_sub as $subsection):?>
                                    <li data-menu-up-level="<?=$subsection['id']?>"><a href='/admin/?t=subsections&c=select&sect=<?=$subsection['id']?>&page=1'><?=$subsection['name']?></a></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif;?>
                    </li>
                <?php endif; ?>
                <li>
                    <a href='/admin/?t=orders&c=select&page=1'>
                        <svg class='icon || left_icon'>
                            <use xlink:href='View/img/svgdefs.svg#icon_subcategory'></use>
                        </svg>
                        <span>Orders</span>
                    </a>
                </li>
		
<!--                --><?php //if (constant('PRODUCTS')) : ?>
<!--                <li><a href='' class='has_submenu'>-->
<!--                        <svg class='icon || left_icon'><use xlink:href='View/img/svgdefs.svg#icon_product'></use></svg>-->
<!--                        <svg class='icon || right_icon'><use xlink:href='View/img/svgdefs.svg#icon_chevron_down'></use></svg>-->
<!--                        <span>--><?php //if(PRODUCTS == 1){echo 'Products';}else{echo PRODUCTS;} ?><!--</span>-->
<!--                    </a>-->
<!--                    <ul>-->
<!--                        <li class='submenu_head'>--><?php //if(PRODUCTS == 1){echo 'Products';}else{echo PRODUCT_CATS;} ?><!--</li>-->
<!---->
<!--                        --><?php //if (constant('PRODUCT_CATS')) : ?>
<!--                            <li data-menu-name="product_cats"><a href='/admin/?t=product_cats&c=select&page=1'>--><?php //if(PRODUCT_CATS == 1){echo 'Categories';}else{echo PRODUCT_CATS;} ?><!--</a></li>-->
<!--                        --><?php //endif;?>
<!---->
<!--                        --><?php //if (constant('CATEGORIES')) : ?>
<!--                            --><?php //if (count(CATEGORIES)):?>
<!--                                <li data-menu-name="level_2"><a href='' class="has_submenu">--><?php //if(CATEGORIES == 1){echo 'CATEGORIES';}else{echo CATEGORIES;} ?><!--</a>-->
<!--                                    <ul>-->
<!--                                        <div class="custom_scroll_wrap1">-->
<!--                                            <div class="custom_scroll_bar"></div>-->
<!--                                            <div class="custom_scroll_wrap2">-->
<!--                                                <div class="custom_scroll">-->
<!--                                        --><?php //foreach($level_1 as $category):?>
<!--                                            <li data-menu-up-level="--><?//=$category['id']?><!--"><a href="/admin/?c=view&v=table&t=level_2&page=1&up_level_t=level_1&up_level_id=--><?//=$category['id']?><!--">--><?//=$category['name']?><!--</a></li>-->
<!--                                        --><?php //endforeach?>
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </ul>-->
<!--                                </li>-->
<!--                            --><?php //else:?>
<!--                                <li><a href='/admin/?c=view&v=create&t=level_1' class="">--><?php //if(LEVEL_2 == 1){echo 'Subcategory1';}else{echo LEVEL_2;} ?><!--</a></li>-->
<!--                            --><?php //endif?>
<!--                        --><?php //endif;?>
<!---->
<!---->
<!--                        <li data-menu-name="products"><a href='/admin/?t=products&c=select&page=1'>--><?php //if(PRODUCTS == 1){echo 'Products';}else{echo PRODUCTS;} ?><!--</a></li>-->
<!---->
<!--                        --><?php //if (constant('ORDERS')) : ?>
<!--                            <li data-menu-name="orders"><a href='/admin/?t=orders&c=select&page=1' <!--class="--><?///*=($qty_orders <= 0) ? "disabled" : "";*/?><!--">--><?php //if(ORDERS == 1){echo 'Orders';}else{echo ORDERS;} ?><!--</a></li>-->
<!--                        --><?php //endif;?>
<!--                        --><?php //if (constant('CUSTOMERS')) : ?>
<!--                            <li data-menu-name="customers"><a href='/admin/?t=customers&c=select&page=1'>--><?php //if(CUSTOMERS == 1){echo 'Customers';}else{echo CUSTOMERS;} ?><!--</a></li>-->
<!--                        --><?php //endif;?>
<!--                    </ul>-->
<!--                </li>-->
<!--                --><?php //endif;?>
                <?php if (constant('GALLERIES')) : ?>
                <li>
                    <a href='' class='has_submenu'>
                        <svg class='icon || left_icon'><use xlink:href='View/img/svgdefs.svg#icon_image'></use></svg>
                        <svg class='icon || right_icon'><use xlink:href='View/img/svgdefs.svg#icon_chevron_down'></use></svg>
                        <span><?php if(GALLERIES == 1){echo 'Galleries';}else{echo GALLERIES;} ?></span>
                    </a>
                    <ul>
                        <li class='submenu_head'><?php if(GALLERIES == 1){echo 'Galleries';}else{echo GALLERIES;} ?></li>

                        <?php if (constant('GALLERY_CATS')) : ?>
                            <li data-menu-name="gallery_cats"><a href='/admin/?t=gallery_cats&c=select&page=1'><?php if(GALLERY_CATS == 1){echo 'Categories Galleries';}else{echo GALLERY_CATS;} ?></a></li>
                        <?php endif;?>
                        <?php if (constant('GALLERIES')) : ?>
                            <li data-menu-name="galleries"><a href='/admin/?t=galleries&c=select&page=1'><?php if(GALLERIES == 1){echo 'Galleries';}else{echo GALLERIES;} ?></a></li>
                        <?php endif?>
                    </ul>
                </li>
                <?php endif;?>
                <?php if (constant('ARTICLES')) : ?>
                <li><a href='' class='has_submenu'>
                        <svg class='icon || left_icon'><use xlink:href='View/img/svgdefs.svg#icon_blog'></use></svg>
                        <svg class='icon || right_icon'><use xlink:href='View/img/svgdefs.svg#icon_chevron_down'></use></svg>
                        <span><?php if(ARTICLES == 1){echo 'Articles';}else{echo ARTICLES;} ?></span>
                    </a>
                    <ul>
                        <li class='submenu_head'><?php if(ARTICLES == 1){echo 'Articles';}else{echo ARTICLES;} ?></li>
                        <?php if (constant('ARTICLE_CATS')) : ?>
                            <li data-menu-name="article_cats"><a href='/admin/?t=article_cats&c=select&page=1'><?php if(ARTICLE_CATS == 1){echo 'Categories Articles';}else{echo ARTICLE_CATS;} ?></a></li>
                        <?php endif;?>
                        <?php if (constant('ARTICLE_TAGS')) : ?>
                            <li data-menu-name="article_tags"><a href='/admin/?t=article_tags&c=select&page=1'><?php if(ARTICLE_TAGS == 1){echo 'Tags';}else{echo ARTICLE_TAGS;} ?></a></li>
                        <?php endif;?>
                        <?php if (constant('ARTICLE_COMMENTS')) : ?>
                            <li data-menu-name="article_comments"><a href='/admin/?t=article_comments&c=select&page=1'>Comments</a></li>
                        <?php endif;?>

                        <?php if (constant('ARTICLES')) : ?>
                                    <li data-menu-name="articles"><a href='/admin/?t=articles&c=select&page=1' class=""><?php if(ARTICLES == 1){echo 'Articles';}else{echo ARTICLES;} ?></a>
                        <?php endif?>
                    </ul>
                </li>
                <?php endif;?>
                <? if (NEWS         != 0 ||
                       TESTIMONIALS != 0 ||
                       BANNER_1     !== 0 ||
                       SLIDER_1     !== 0 ||
                       SUBSCRIBERS  != 0 ||
                       FAQ     		!= 0 ||
                       MEDIA        != 0 ) : ?>
                <li><a href='' class='has_submenu'>
                        <svg class='icon || left_icon'><use xlink:href='View/img/svgdefs.svg#icon_extras'></use></svg>
                        <svg class='icon || right_icon'><use xlink:href='View/img/svgdefs.svg#icon_chevron_down'></use></svg>
                        <span>Extras</span>
                    </a>
                    <ul>
                        <li class='submenu_head'>Extras</li>
                        <?php if (constant('NEWS')):?>
                            <li data-menu-name="news"><a href='/admin/?t=news&c=select&page=1'><?php if(NEWS == 1){echo 'News';}else{echo NEWS;} ?></a></li>
                        <?php endif;?>
                        <?php if (constant('TESTIMONIALS')):?>
                            <li data-menu-name="testimonials"><a href='/admin/?t=testimonials&c=select&page=1'><?php if(TESTIMONIALS == 1){echo 'Testimonials';}else{echo TESTIMONIALS;} ?></a></li>
                        <?php endif;?>
                        <?php if (constant('BANNER_1') || constant('BANNER_2') || constant('BANNER_2')):?>
                            <li><a href='' class='has_submenu'>
                                    <svg class='icon || right_icon'><use xlink:href='View/img/svgdefs.svg#icon_chevron_down'></use></svg>
                                    Banners
                                </a>
                                <ul>
                                    <?php if (constant('BANNER_1')):?>
                                        <li data-menu-name="banner_1"><a href="/admin/?t=banner_1&c=select&page=1"><?php if(BANNER_1 == 1){echo 'Banner 1';}else{echo BANNER_1;} ?></a></li>
                                    <?php endif?>
                                    <?php if (constant('BANNER_2')):?>
                                        <li data-menu-name="banner_2"><a href="/admin/?t=banner_2&c=select&page=1"><?php if(BANNER_2 == 1){echo 'Banner 2';}else{echo BANNER_2;} ?></a></li>
                                    <?php endif?>
                                    <?php if (constant('BANNER_3')):?>
                                        <li data-menu-name="banner_3"><a href="/admin/?t=banner_3&c=select&page=1"><?php if(BANNER_3 == 1){echo 'Banner 3';}else{echo BANNER_3;} ?></a></li>
                                    <?php endif?>
                                </ul>
                            </li>
                        <?php endif;?>
                        <?php if (constant('SLIDER_1') || constant('SLIDER_2') || constant('SLIDER_3')):?>
                            <li><a href='' class='has_submenu'>
                                    <svg class='icon || right_icon'><use xlink:href='View/img/svgdefs.svg#icon_chevron_down'></use></svg>
                                    Sliders
                                </a>
                                <ul>
                                    <?php if (constant('SLIDER_1')):?>
                                    <li data-menu-name="slider_1"><a href="/admin/?t=slider_1&c=select&page=1"><?php if(SLIDER_1 == 1){echo 'Slider 1';}else{echo SLIDER_1;} ?></a></li>
                                    <?php endif?>
                                    <?php if (constant('SLIDER_2')):?>
                                        <li data-menu-name="slider_2"><a href="/admin/?t=slider_2&c=select&page=1"><?php if(SLIDER_2 == 1){echo 'Slider 2';}else{echo SLIDER_2;} ?></a></li>
                                    <?php endif?>
                                    <?php if (constant('SLIDER_3')):?>
                                        <li data-menu-name="slider_3"><a href="/admin/?t=slider_3&c=select&page=1"><?php if(SLIDER_3 == 1){echo 'Slider 3';}else{echo SLIDER_3;} ?></a></li>
                                    <?php endif?>
                                </ul>
                            </li>
                        <?php endif;?>

                        <?php if (constant('MEDIA')):?>
                            <li data-menu-name="media"><a href='/admin/?t=media&c=select&page=1'><?php if(MEDIA == 1){echo 'Media';}else{echo MEDIA;} ?></a></li>
                        <?php endif;?>

                        <?php if (constant('FAQ')):?>
                            <li data-menu-name="faq"><a href='/admin/?t=faq&c=select&page=1'><?php if(FAQ == 1){echo 'FAQ';}else{echo FAQ;} ?></a></li>
                        <?php endif;?>

                        <?php if (constant('SUBSCRIBERS')):?>
                            <li data-menu-name="subscribers"><a href='/admin/?t=subscribers&c=select&page=1'><?php if(SUBSCRIBERS == 1){echo 'Subscribers';}else{echo SUBSCRIBERS;} ?></a></li>
                        <?php endif;?>

                    </ul>
                </li>
                <?php endif;?>
                <?php if ($_SESSION['authorize']['status'] == 1) : ?>
                    <li>
                        <a href="/admin/?t=settings&c=select">
                            <svg class='icon || left_icon <?php if($_GET['t']=='settings' || ($_GET['t'] != 'user_settings' && $_GET['c']=='settings')) echo 'active_nav'; ?>'><use xlink:href='View/img/svgdefs.svg#icon_settings'></use></svg>
                            <span class="<?php if($_GET['t']=='settings' || ($_GET['t'] != 'user_settings' && $_GET['c']=='settings')) echo 'active_nav'; ?>">Settings</span>
                        </a>
                    </li>
                <?php endif?>

                    <li data-menu-name="users">
                        <a href="/admin/?t=users&c=select&page=1">
                            <svg class='icon || left_icon '><use xlink:href='View/img/svgdefs.svg#icon_group'></use></svg>
                            <span>Users</span>
                        </a>
                    </li>
            </ul>
        </nav>
    </div>
    <div class='copyright'>
<!--        Copyright 2011---><?//=date('Y')?><!-- <b></b>-->
<!--        <a href='http://korzun.com.ua'>Korzun Studio.</a><br/>-->
		Version: <?=JB_VERSION?>
    </div>
</aside>