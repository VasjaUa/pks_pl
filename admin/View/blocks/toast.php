<div class="error toast">
	<span class='toast_close'><svg class='icon'><use xlink:href='/admin/View/img/svgdefs.svg#icon_close'></use></svg></span>
	<div class='table'>
		<div class='tcell'>
			<svg class='icon || toast_icon'><use xlink:href='/admin/View/img/svgdefs.svg#icon_error'></use></svg>
		</div>
		<div class='tcell'>
			<p>Incorrect values!</p>
		</div>
	</div>
</div>

<div class="success toast">
	<span class='toast_close'><svg class='icon'><use xlink:href='/admin/View/img/svgdefs.svg#icon_close'></use></svg></span>
	<div class='table'>
		<div class='tcell'>
			<svg class='icon || toast_icon'><use xlink:href='/admin/View/img/svgdefs.svg#icon_check'></use></svg>
		</div>
		<div class='tcell'>
			<p>Incorrect values!</p>
		</div>
	</div>
</div>