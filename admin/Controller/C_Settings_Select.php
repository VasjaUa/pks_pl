<?php
//include_once('Model/M_Settings.php');
include_once('Controller/C_Base.php');

class C_Settings_Select extends C_Base
{
    public $Sections = array();
    public $HasSubsec = array();

    // конструктор
    function __construct()
    {
        $this->table = $_GET['t'];
    }

    protected function OnInput(){
        parent::OnInput();

        //берем экземпляр класа M_Users
        $users = M_Users::Instance();

        $object = Model::Instance();

            if ($_SESSION['authorize']['status'] != 1) {
                header('Location: /admin');
            }

        if($this->IsPost()) {}

        $this->Sections = $object->Array_where('sections', "WHERE `landing`='1'");
        $this->HasSubsec = $object->Array_where('sections', "WHERE `has_subsect`='1'");

    }

    // виртуальный генератор HTML
    protected function OnOutput()
    {

        $this->content = $this->View('View/v_'.$this->table.'_select.php',
            array(
                'sections'=>$this->Sections,
                'subSections'=>$this->HasSubsec
            ));
        parent::OnOutput();
    }
}