<?php

// SET DB CONFIG
include_once('config.php');
include_once('startup.php');
include_once('Model/M_Users.php');
include_once('Model/Model.php');


//error_reporting(E_ALL & ~E_NOTICE);
// CONNECT TO DB
startup();

// MANAGERS
$mUsers = M_Users::Instance();

// if user is not register - go to register page.
$mUsers->ifLogin();

// CHARSET
header('Content-type: text/html; charset=utf-8');

// SET AUTO CONTROLLERS

$object = Model::Instance();

$subscriber_s = $object->All_rows('subscribers');

if ($subscriber_s != null) {
    foreach ($subscriber_s as $subscriber) {
        $towrite.='"'.$subscriber['name'].'",';
        $towrite.='"'.$subscriber['email'].'",';
        $towrite.="\n";
    }
}

$filename = 'subscribersList_'.date('y-m-d_H:i:s').'.csv';
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename="'.$filename.'"');

echo $towrite;
?>