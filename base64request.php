<?php
include_once ("helpers/server.php");

header('Access-Control-Allow-Origin: www.pks.pl');

$data = $_POST['reserv_number'];
$ch = curl_init();

if (isset($_POST['type']) && $_POST['type'] == 'buy') {
    $ch = curl_init($server . 'CheckReserveTickets?reserv_number=' . base64_encode($data));

} elseif (isset($_POST['type']) && $_POST['type'] == 'cancel') {
    $ch = curl_init($server . 'TicketsCancelation?ticket_number=' . base64_encode($data));
}

curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST['reserv_number']);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: text/html; charset=utf-8',
        'Content-Length: ' . strlen($data)
    )
);

$output = curl_exec($ch);
curl_close($ch);

echo $output;