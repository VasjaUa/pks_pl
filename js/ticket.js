var download = $('#downloadBilet');

setTimeout(function () {
    download.removeClass("m-progress");
    download.parent().removeAttr("onclick");
}, 2000)

/**
 * Preventing this page to make "back" in browser
 **/
(function (global) {

    if (typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        noBackPlease();
    };

    noBackPlease();

})(window);
