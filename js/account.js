$(document).ready(function () {

    /**
     * Check the content in tabs, if null insert standard text
     */
    var $wrapperCheck = function (check, elem, callback) {
        var $saled = $('#sold');
        var $cancelled = $('#canceled');
        var $faktury = $('#faktury');

        if ($saled.find('.block-tickets').length < 1)
            $saled.html('<p style="text-align: center">Brak biletów</p>');

        if ($cancelled.find('.block-tickets').length < 1)
            $cancelled.html('<p style="text-align: center">Brak biletów</p>');

        if ($faktury.find('.block-tickets').length < 1)
            $faktury.html('<p style="text-align: center">Brak zapisów</p>');


        if (check == true) {

            if ($cancelled.find('p').length > 0) {
                $cancelled.find('p').remove();
            }

            elem.slideUp("slow", function () {
                $cancelled.append(elem[0]);
                elem.css('display', 'block');
                elem.find('.col-5').remove();
                callback()
            });

        }
    };

    $wrapperCheck();


    if ($.cookie('logged') == undefined) {
        $('.container>.wrap-btn').css('display', 'block');
    }


    $('.btn-forget-pass').on("click", function (e) {
        swal({
            title: "Tworzenie nowego hasła",
            text: "Wpisz swój email",
            input: "email",
            showCancelButton: true,
            cancelButtonText: "Anuluj",
            // closeOnConfirm: false,
            showLoaderOnConfirm: true,
            animation: "slide-from-top",
            inputPlaceholder: "E-mail",
            preConfirm: function (inputValue) {
                return new Promise(function (resolve, reject) {
                    if (inputValue === "") {
                        // swal.showInputError("Wpisz swój email");
                        reject("Wpisz swój email")
                    } else {
                        setTimeout(function () {
                            $.ajax({
                                url: "/account.php",
                                type: "POST",
                                data: {
                                    resetPassword: true,
                                    email: inputValue
                                },
                                success: function (data) {
                                    var check = data.split(';')[0];
                                    var token = data.split(';')[1];
                                    var id = data.split(';')[2];

                                    if (id != undefined) {
                                        $.ajax({
                                            url: "/mailer.php",
                                            type: "POST",
                                            data: {
                                                resetPassword: true,
                                                email: inputValue,
                                                token: token,
                                                id: id
                                            },
                                            success: function (data) {
                                                swal('Odbierz pocztę ', 'Na Twój adres mailowy została wysłana wiadomość potrzebna do zresetowania hasła. ' + inputValue, 'success')
                                            },
                                            error: function () {
                                                console.log('error')
                                            }
                                        });
                                    } else if (data == "false") {
                                        // swal.showInputError("This email address does not exist!");
                                        reject("Wpisany adres nie istnieje.");
                                    }
                                },
                                error: function () {
                                    console.log('error')
                                }
                            });
                        }, 1500)
                    }
                })
            }
        })
    });

    $('#cancel').click(function () {
        var login = $('#field1').val(),
            password = $('#field2').val();

        $.ajax({
            url: "/account.php",
            type: "POST",
            data: {
                login: login,
                password: password
            },
            success: function (data) {
                if (data == 'true') {
                    location.href = '/user-cabinet';
                } else if (data == 'status') {
                    swal('Potwierdź rejestrację konta', '', 'error')

                } else {
                    swal('Wprowadź poprawne dane', '', 'error')
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

 

    //Код для замены данных в кабинете
    var sex = $('[data="sex"]').text();
    $('#edit_user_cabinet').click(function () {
        $('.edit').each(function () {
            var text = $(this).text();
            var animation;

            if ($(this).parents('.block-0').length > 0)
                animation = "fadeInLeft";
            else if ($(this).parents('.block-1').length > 0)
                animation = "fadeInUp";
            else if ($(this).parents('.block-2').length > 0)
                animation = "fadeInRight";
            if ($(this).attr('data') == 'birthday') {
                $(this).html('<input name="' + $(this).attr('data') + '" value="' + text + '" style="cursor: pointer" id="datetimepicker3" class="caption form-control animated ' + animation + '">');
                $("#datetimepicker3").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1940:+0'
                });
            } else if ($(this).attr('data') == 'phone') { 
                $(this).html('<input type="text" placeholder="+48000000000"  name="' + $(this).attr('data') + '" value="' + text + '" class="caption form-control phoned animated ' + animation + '">')
                $('.phoned').mask('+ 0(000) 000-0000');
            }else if($(this).attr('data') == 'email'){
                $(this).html('<input type="email" name="' + $(this).attr('data') + '" value="' + text + '" class="caption form-control animated ' + animation + '">')     
            }else{
                $(this).html('<input name="' + $(this).attr('data') + '" value="' + text + '" class="caption form-control animated ' + animation + '">')
            }
        });

        $('.block-user-info:eq(0) .row-info:eq(3)').html('<div class="title">Płeć:</div>' +
            '<div class="form-group">' +
            '<div class="radio">' +
            '<input type="radio"  id="male" name="sex" value="male">' +
            '<label for="male">Mężczyzna</label></div>' +
            '<div class="radio">' +
            '<input type="radio"  id="female" name="sex" value="female">' +
            '<label for="female">Kobieta</label></div></div>');

        $('.radio').click(function () {
            $('.radio').removeClass('edit');
            $(this).addClass('edit');
        });

        if (!$('[type="radio"]').is(':checked')) {
            $('.radio').each(function () {
                if ($(this).find('label').text() == sex) {
                    $(this).addClass('edit');
                    $(this).find('input').attr('checked', "checked");
                }
            })
        }

        $(this).attr("style", "display:none");
        $("#save_user_cabinet").attr("style", "display:block");

    });

    //Код для замены данных в кабинете в фактуре
    $('#edit_user_factura').click(function () {
        // $('.edit_faktura').each(function () {
        //     var text = $(this).text();
        //     var animation;
        //     // var data = $(this).attr('data')
        //     animation = "fadeInLeft";
        //     if ($(this).attr('data') == "postal-code") {
        //         $(this).html('<input name="' + $(this).attr('data') + '" value="' + text + '" placeholder="12-345" class="caption form-control animated ' + animation + '">')
        //     } else {
        //         $(this).html('<input name="' + $(this).attr('data') + '" value="' + text + '" class="caption form-control animated ' + animation + '">')
        //     }
        //     $('[data="postal-code"] input').mask('00-000', {clearIfNotMatch: true});
        //     $('[data="NIP"] input').mask('0000000000', {clearIfNotMatch: true});
        // });
        // // $('.row_faktura .title').hide();
        //
        // $(this).attr("style", "display:none");
        // $('.block-user-tabs .wrap_btn_save .checkbox-buy').attr("style", "display:none");
        // $("#save_user_faktury").attr("style", "display:table");
    });

    $('#group_registration input[type="checkbox"] + label span').click(function () {
        $(this).parents('label').toggleClass('checked');
    });

    /**
     * Save data in cabinet
     */
    $('#save_user_cabinet').click(function () {
        var array = [];
        $('.edit').each(function () {
            array.push($(this).find('input').val().replace("\'", "\\'"));
        });

        if (array[0].search(/^[.a-z0-9_-]+@[.a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i) == 0) {
            $.ajax({
                url: "/account.php",
                type: "POST",
                data: {
                    array: array
                },
                success: function (data) {
                    if (data == 'true') {
                        $('#save_user_cabinet').attr("style", "display:none");
                        $('#edit_user_cabinet').attr("style", "display:block");
                        swal('Dane zostały zmienione', '', 'success');
                        setTimeout(function () {
                            window.location.href = "/user-cabinet";
                        }, 1200);


                    } else if (data == 'email') {
                        swal('Wasza wybrana e-mail jest już zarejestrowany', '', 'error')
                    } else {
                        swal('Wprowadź poprawne dane', 'Wprowadź poprawne dane', 'error')
                    }
                },
                error: function () {
                    console.log('error')
                }
            });
        } else {
            swal('Błąd', 'Podaj swój adres e-mail', 'error')
        }
    });


    $('#save_user_faktury').click(function () {
        var $parent = $('#invoice_block');
        var error = !checkFormFactura();

        if (error == false) {
            var obj = {
                firm: $parent.find('[name="firm"]').val().replace("\'", "\\'"),
                nip: $parent.find('[name="nip"]').val().replace("\'", "\\'"),
                f_post_code: $parent.find('[name="f_post_code"]').val().replace("\'", "\\'"),
                f_country: $parent.find('[name="f_country"]').val().replace("\'", "\\'"),
                f_city: $parent.find('[name="f_city"]').val().replace("\'", "\\'"),
                f_street: $parent.find('[name="f_street"]').val().replace("\'", "\\'"),
                firm_or_person: $parent.find('[name="firmOrPerson"]').prop("checked")
            };
            $.ajax({
                url: "/account.php",
                type: "POST",
                data: {
                    faktury: obj
                },
                success: function (response) {
                    console.log(response);
                    swal('Dane zostały zmienione', '', 'success');
                    setTimeout(function () {
                        window.location.href = "/user-cabinet";
                    }, 1200);
                },
                error: function () {}
            });
        }
    });

    // Login
    $('#login').click(function () {
        var login = $('#field1').val(),
            password = $('#field2').val();
        if (login != '') {
            $.ajax({
                url: "/account.php",
                type: "POST",
                data: {
                    login: login,
                    password: password
                },
                success: function (data) {
                    if (data == 'true') {
                        location.href = '/user-cabinet';
                    } else if (data == 'status') {
                        swal('Confirm your account registration', '', 'error')

                    } else {
                        swal('Wprowadź poprawne dane', '', 'error')
                    }
                },
                error: function () {
                    console.log('error')
                }
            });
        } else {
            swal('Wprowadź poprawne dane', '', 'error')
        }
    });
    // Код для замены пароля в кабинете
    $('#change_password').click(function () {
        var password_old = $('#old-password').val(),
            password_new = $('#new-password').val();
        if (password_old != '' && password_new != '') {
            if (password_old != password_new) {
                $.ajax({
                    url: "/account.php",
                    type: "POST",
                    data: {
                        password_old: password_old,
                        password_new: password_new
                    },
                    success: function (data) {
                        if (data == 'true') {
                            $('#old-password').val('');
                            $('#new-password').val('');
                            swal('Hasło zostało zmienione', '', 'success')
                        } else {
                            swal('Wprowadź poprawne dane', '', 'error')
                        }
                    },
                    error: function () {
                        console.log('error')
                    }
                });
            } else {
                swal('Wprowadź poprawne dane', '', 'error')
            }
        } else {
            swal('Wprowadz hasło', '', 'error')
        }
    });

    // Замена изображения
    $("#file-3").fileinput({
        language: "pl",
        showUpload: true,
        showCaption: true,
        browseClass: "btn btn-primary btn-file-photo",
        fileType: "any"
    });

    $('#file-3').click(function () {
        $('.btn-photo').attr("style", "display:block");
    });

    $('.btn-photo').click(function () {
        var form = $('.block-personal').find('form'),
            formData = new FormData(form.get(0));
        var login = $('.login').text();
        formData.append('login', login);

        $.ajax({
            url: "/account.php",
            type: "POST",
            data: formData,
            processData: false, // Не обрабатываем файлы
            contentType: false, // Так jQuery скажет серверу что это строковой запрос
            success: function () {
                $(form)[0].reset();
                window.location.href = "/user-cabinet";
            }
        });

        $(this).attr("style", "display:none");

    });


    /**
     * Cancellation of tickets
     **/
    $('.btn-cancellation').click(function (e) {
        var thsbtn = $(this);
        thsbtn.addClass('m-progress');
        e.preventDefault();
        var $number = $(this).parents('.block-tickets').find('.numb-ticket').text();
        var $elem = $(this).parents('.block-tickets');

        swal({
            title: 'Uwaga!!!',
            text: "Bilet będzie zwrócony z potrąceniem 10% wartości",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Akceptuję',
            cancelButtonText: 'Cofnij'
        }).then(function () {
                $.ajax({
                    url: "/account.php",
                    type: "POST",
                    data: {cancellation: $number},
                    success: function (data) {
                        console.log(data);
                        thsbtn.removeClass('m-progress');
                        if(data != 'ok'){
                            swal({
                                title: 'Uwaga!!!',
                                text: data,
                                type: 'warning',
                                confirmButtonText: 'Ok'
                            })
                        }else{
                            swal("Sukces!", "Twój bilet został pomyślnie anulowany.", "success").then($wrapperCheck(true, $elem, function(){}));
                        }
                    }
                });
            });

    });

    /**
     * checkFacture
     **/


    // $('[checkFacture]').click(function () {
    //     var $parent = $(this).parents('.checkbox-buy');
    //     if ($parent.find('label').hasClass('checked')) {
    //         check('true')
    //     } else {
    //         check('false')
    //     }
    //     function check(check) {
    //         $.ajax({
    //             url: "/account.php",
    //             type: "POST",
    //             data: {
    //                 checkFacture: check
    //             },
    //             success: function () {
    //             },
    //             error: function () {
    //                 console.log('error')
    //             }
    //         });
    //     }
    // });


    /**
     * Filters
     **/

    // var enableFilter = 0;
    //
    // $('[href="#tab-2"]').click(function () {
    //     if(enableFilter == 0){
    //         mixitup('#saled', {
    //             load: {
    //                 sort: 'order:desc'
    //             },
    //             animation: {
    //                 effects: 'fade rotateZ(-180deg)',
    //                 duration: 700
    //             },
    //             classNames: {
    //                 elementSort: 'sort-btn'
    //             },
    //             selectors: {
    //                 target: '.mix-target'
    //             }
    //         });
    //         enableFilter = 1;
    //     }
    //
    // });


});
