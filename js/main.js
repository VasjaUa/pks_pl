$(document).ready(function () {

    if (localStorage.getItem('check') == "test") {
        localStorage.setItem('data', '');
        localStorage.setItem('reserve', '');
        localStorage.setItem('check', null);
    }

    var date = new Date();
    if (localStorage.getItem('data')) {
        basket();
    }

    // This vars are used to store data for destination points and courses info
    var dataStorageA = {},
        dataStorageB = {},
        courses;

var valera = JSON.parse(localStorage.getItem('dataStorageAa')),
    leha = JSON.parse(localStorage.getItem('dataStorageBb'));

    if(valera){
        $('#pointA').val(valera.item.value);
        dataStorageA.localityInfo = valera;
    }
    if(leha){
        $('#pointB').val(leha.item.value);
        dataStorageB.localityInfo = leha;
    }

    // Start search
    $('#search').click(function () {

        // Check data in fields
        if (checkData(dataStorageA, dataStorageB) == true && chkDate() ) {
            // Add attr to change the styles and remove background images
            $('.body').attr('class', 'autoload');
            // Preloader
            $('#render').html('<div class="container"><div class="preloader"><img src="img/preloader.gif"></div></div>');


            // Search available courses
            stationConnection();
        }
        
        
    });

function chkDate(){

    var date = new Date(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        thisDate = new Date(),
        fromwhere = new Date($('#datetimepicker1').val()),
        otherDate = new Date(fromwhere.getFullYear(), fromwhere.getMonth(), fromwhere.getDate()),
        today = new Date(thisDate.getFullYear(), thisDate.getMonth(), thisDate.getDate()),
        nAll = new Date(),
        Hr = $("#datetimepicker2").val().substr(0, 2),
        Min = $("#datetimepicker2").val().substr(3, 2),
        setTime = nAll.setHours(Hr, Min),
        todayHt = new Date(),
        todayTime = todayHt.setHours(date.getHours(),date.getMinutes());

    if(otherDate > today){
        $("#datetimepicker2").removeClass('errr');
        return true;
    }else if(todayTime <= setTime){
        $("#datetimepicker2").next().removeClass('errr');
        return true;
    }else{
        $("#datetimepicker2").next().addClass('errr');
        $("#datetimepicker2").val(hours + ':' + minutes);
        return false;
    }

}

    var current_ticket = false;
    var current_course;
    var ulga = false;
    var ulga_type = false;
    var ulga_opt = false;
    var ulga_code = false;

    function getUlgaType() {
        var type;

        if (ulga_type == 1 && ulga_opt != "Ulgi handlowe") {
            type = "LegalRelief";
        } else if (ulga_type > 1 || ulga_opt == "Ulgi handlowe") {
            type = "CommercialRelief"
        } else {
            type = "Normal"
        }

        return type
    }

    function getUlgaCode() {
        if (getUlgaType() == "LegalRelief") {
            var arr = current_ticket.legal_discounts;

            for (var i = 0; i < arr.length; i++) {
                if (arr[i].discount_type == ulga) {
                    ulga_code = arr[i].discount_code
                }
            }

        } else if (getUlgaType() == "CommercialRelief") {
            var arr = current_ticket.commercial_discounts;

            for (var i = 0; i < arr.length; i++) {
                if (arr[i].discount_type == ulga) {
                    ulga_code = arr[i].discount_code
                }
            }
        }

        return ulga_code
    }


    $('body').on('click', '.start_col .jq-selectbox__dropdown li', function (e) {
        var $this = $(this);
        var new_ = current_ticket.stations_from.find(function (it) {
            return (it.station == $this.text()) ? it.station_id : false
        });

        current_ticket.station_from_id = new_.station_id;
        refreshStations();
    });


    $('body').on('click', '.stop_col .jq-selectbox__dropdown li', function (e) {
        var $this = $(this);
        var new_ = current_ticket.stations_to.find(function (it) {
            return (it.station == $this.text()) ? it.station_id : false
        });

        current_ticket.station_to_id = new_.station_id;
        refreshStations();
    });

    function refreshStations() {
        var date = parseInt($('#datetimepicker1').val().replace(/\D+/g, ""));
        $.ajax({
            url: '/pr2.php',
            type: "post",
            dataType: "json",
            data: {
                url: "GetCourseData",
                course_id: current_course.course_id,
                date: date,
                station_or_locality_from_id: dataStorageA.localityInfo.item.id,
                station_or_locality_to_id: dataStorageB.localityInfo.item.id,
                is_station_from: 0,
                is_station_to: 0,
                station_from_id: current_ticket.station_from_id,
                station_to_id: current_ticket.station_to_id
            },
            success: function (data) {
                current_ticket = data.contents;
                renderStations(data, new TicketInfo(data));
            },
            error: function () {
                swal('Oops ...', 'Błąd podczas wykonywania transakcji', 'error')
            }
        });
    }


    $('body').on('click', '.second_row .jq-selectbox__dropdown li', function () {
        var split = $(this).text().split("- ")[0];
        var len = split.length;
        ulga = split.substr(0, len - 1);
        ulga_type = $(this).prevAll('.optgroup').length;
        ulga_opt = $(this).prevAll('.optgroup').text();

        var originalPrice = $(this).parents('#modalContentId').find('[ticketPrice]').attr('data');

        if ($(this).text() == 'Bez ulgi') {
            $(this).parents('#modalContentId').find('[ticketPrice]').text(originalPrice + ' zł')

        } else {
            var discount = $(this).text().split('- ')[1];

            if (discount.substring(discount.length - 2) == "zł") {
                $(this).parents('#modalContentId').find('[ticketPrice]').text((parseFloat(originalPrice) - parseFloat(discount)).toFixed(2) + " zł")
            } else {
                discount = parseFloat($(this).text().split('- ')[1]);
                if (discount != 100) {
                    $(this).parents('#modalContentId').find('[ticketPrice]').text(parseFloat(originalPrice - originalPrice * discount / 100).toFixed(2) + ' zł')
                } else {
                    $(this).parents('#modalContentId').find('[ticketPrice]').text('bezpłatny')
                }
            }
        }
        swalPrice()
    });


    $("body").on("keyup", "#ticketCount", function () {
        checkCount($(this));
        priceAdd();
    });

    $('body').on('keyup', '[ticketCount]', function () {
        checkCount($(this));
        swalPrice()
    });

    function swalPrice() {
        var $sum = $('[sum-money]');
        var $price = parseFloat($('[ticketPrice]').text());
        var $count = parseInt($('[ticketCount]').val());

        if (!isNaN($price) && !isNaN($count)) {
            $sum.text(($count * $price).toFixed(2) + ' zł');
        } else {
            $sum.text('bezpłatny');
        }
    }

    // validate inputs
    var checkCount = function (count) {
        if (count.val() != "") {
            if (isNaN(parseInt(count.val()))) {
                count.val(1)
            } else if (count.val() < 1) {
                count.val(1)
            } else {
                count.val(parseInt(count.val()))
            }
        }
    };

    var countObj = {};
    var course = false;
    // Event handler for "add to cart" button
    $('body').on('click', '.btn-select', function () {
        var course_id = $(this).attr('data');

        for (var i = 0; i < courses.length; i++) {
            if (courses[i].course_id == course_id) {
                var dateArray = courses[i].departure.split(' ')[0].split('.');
                var timeArray = courses[i].departure.split(' ')[1].split(':');
                countObj.date = dateArray[1] + '.' + dateArray[2];
                countObj.time = timeArray[0] + ':' + timeArray[1];
                localStorage.setItem('course', JSON.stringify(courses[i]));
                course = JSON.parse(localStorage.getItem('course'));
                current_course = courses[i];
                break;
            }
        }

        // $(this).popover();
        setTimeout(function () {
            $('[data-toggle="popover"]').popover('hide');
        }, 3000);


        function showSwal() {
            var ticketInfo;

            swal({
                customClass: 'bilet_modal',
                title: 'Szczegóły biletu',
                showCancelButton: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                showLoaderOnConfirm: true,
                confirmButtonText: "Do koszyka",
                cancelButtonText: "Zamknij",
                width: "640px",
                // minheight: "80vh",
                // maxheight: "97vh",
                html: '<div style="margin: 18% auto;" id="loader" class="single4"></div>',
                onOpen: function () {
                    var date = parseInt($('#datetimepicker1').val().replace(/\D+/g, ""));
                    $.ajax({
                        url: '/pr2.php',
                        type: "post",
                        dataType: "json",
                        data: {
                            url: "GetCourseData",
                            course_id: course_id,
                            date: date,
                            station_or_locality_from_id: dataStorageA.localityInfo.item.id,
                            station_or_locality_to_id: dataStorageB.localityInfo.item.id,
                            is_station_from: 0,
                            is_station_to: 0,
                            station_from_id: current_course.station_from_id,
                            station_to_id: current_course.station_to_id
                        },
                        success: function (data) {
                        console.log(data);    
                            if (data == 406){
                                swal('Oops ...', 'Błąd podczas wykonywania transakcji', 'error');
                            }
                            else {
                                current_ticket = data.contents;
                                ticketInfo = new TicketInfo(data);
                                renderStations(data, ticketInfo);
                            }
                        },
                        error: function () {
                            swal('Oops ...', 'Błąd podczas wykonywania transakcji', 'error')
                        }
                    });
                },
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        

                        if ($('[ticketCount]').val() > course.available_places || $('[ticketCount]').val() == "") {
                            console.log($('[ticketCount]').val() > course.available_places);
                            console.log('here');

                            var text = ($('[ticketCount]').val() > course.available_places) ? "Nie możesz kupić więcej biletów niż dostępnych w tym kursie" : "Podaj liczbę biletów";
                            var $this = $('[ticketCount]');
                            
                            $this.webuiPopover('destroy');
                            $this.webuiPopover({trigger: 'manual', content: text, cache:false});
                            $this.webuiPopover('show');
                            setTimeout(function () {
                                $this.webuiPopover('hide');
                            }, 2500);

                            reject();

                        } else {
                            var $station_from = $('[stationFrom]').val(),
                                $station_to = $('[stationTo]').val(),
                                $station_from_id,
                                $station_to_id;

                            if ($station_from == undefined || $station_to == undefined) {
                                reject();
                            } else {
                                $('[stationFrom]>option').each(function () {
                                    if ($(this).val() == $station_from) {
                                        $station_from_id = $(this).attr('station_id')
                                    }
                                });

                                $('[stationTo]>option').each(function () {
                                    if ($(this).val() == $station_to) {
                                        $station_to_id = $(this).attr('station_id')
                                    }
                                });

                                course.ulga_type = ulga;
                                course.station_from = $station_from;
                                course.station_to = $station_to;
                                course.station_from_id = $station_from_id;
                                course.station_to_id = $station_to_id;
                                course.count = $('[ticketCount]').val();
                                course.current_ulga = $('.discount .jq-selectbox__select-text').text();
                                course.current_price = parseFloat($('[ticketPrice]').text());

                                if (isNaN(course.current_price)) {
                                    course.current_price = 0;
                                }

                                swal.noop();
                                if (localStorage.getItem('data') == null || localStorage.getItem('data') == '') {
                                    checkBasket(
                                        "CreateBasket",
                                        {
                                            callback: function() {
                                                reservTicket(course_id, course)
                                            }
                                        });
                                } else {
                                    reservTicket(course_id, course)
                                }
                            }
                        }
                    })
                }
            })
        }

        // if (countTime(countObj) == true) {
            showSwal();
        // } else {
        //     swal('Biletu nie można już kupić. Jest już po terminie odjazdu.', '', 'error')
        // }

    });


    function TicketInfo(data) {
        // Stations data
        this.ticketData = data;

        // jQuery objects
        this.$pWrap = $(".swal2-container");
        this.$cWrap = $(".swal2-modal");
        this.$icon = this.$cWrap.find(".swal2-success");
        this.$content = this.$cWrap.find(".swal2-content");

        // Inner HTML (strings)
        this.icon = '<span class="line tip animate-success-tip"></span> <span class="line long animate-success-long"></span><div class="placeholder"></div> <div class="fix"></div>';
        this.title = "<h2 class='swal2-title' id='modalTitleId'>Powodzenie</h2>";
        this.content = "<div id='modalContentId' class='swal2-content' style='display: block;'>Bilet został dodany do koszyka.</div>";

        // Arrays with modal data
        this.objArr = [this.$icon, this.$content];
        this.htmlArr = [this.icon, this.content];

        // Helpers
        this.showSuccess = function () {
            var me = this;
            $.each(me.objArr, function (i, val) {
                var $this = $(this);
                // $this.css("display", "none");
                // $this.html(me.htmlArr[i]);
                // $this.css("display", "block");
            })
        };

        this.goBack = function () {}
    }

    function renderStations(data, modal) {
        var obj = data.contents;
        var firm_discounts_collection = obj.commercial_discounts;
        var legal_discounts_collection = obj.legal_discounts;

        var $stationsA = obj.stations_from.map(function (item, i) {
            var selected = (!item.default_station) ? "" : "selected";
            return '<option ' + selected + ' value="' + item.station + '" station_id="' + item.station_id + '">' + item.station + '</option>'
        });

        var $stationsB = obj.stations_to.map(function (item, i) {
            var selected = (!item.default_station) ? "" : "selected";
            return '<option ' + selected + ' value="' + item.station + '" station_id="' + item.station_id + '">' + item.station + '</option>'
        });

        //discount
        var discount = function () {
            var options = '<option selected="selected">Bez ulgi</option>';

            if (!!legal_discounts_collection) {
                var legalDiscount = current_ticket.legal_discounts_collection;
                options += '<optgroup label="Ulgi ustawowe">';

                for (var n = 0; n < legalDiscount.length; n++) {
                    options += '<option type="ustawowa" discount="' + legalDiscount[n].discount_type + '">' + legalDiscount[n].discount_type + ' - ' + legalDiscount[n].discount + '%</option>';
                }

                options += '</optgroup>';
            }

            if (!!firm_discounts_collection) {
                var arr = firm_discounts_collection;
                options += '<optgroup label="Ulgi handlowe">';

                for (var i = 0; i < arr.length; i++) {
                    var symbol = "%";
                    if (!arr[i].percent) {
                        symbol = "zł"
                    }

                    options += '<option type="handlowa" discount="' + arr[i].discount_type + '">' + arr[i].discount_type + ' - ' + arr[i].discount.toFixed(2) + ' ' + symbol + '</option>';
                }

                options += '</optgroup>';
            }

            return options
        };


        // modal.showSuccess();
        $('#modalContentId').html
        (
            '<div class="swal-wrap first_row clearfix">' +
            '<div class="col start_col"><div class="title">Przystanek od</div><select stationFrom class="swal-select"><option value="Select station" disabled="">Wybierz przystanek</option>' + $stationsA + '</select>' +
            '</div><div class="swal-reverse"></div>' +
            '<div class="col stop_col"><div class="title">Przystanek do</div><select stationTo class="swal-select"><option value="Select station" disabled="">Wybierz przystanek</option>' + $stationsB + '</select>' +
            '</div></div><div class="discount_title"><div class="discount_title_wrrap">Rodzaj biletu</div></div><div class="swal-wrap second_row clearfix">' +
            '<div class="col">' +
            '<select discount class="swal-select discount">' + discount() + '</select></div>' +
            '<div class="col"><div class="counter"><span>Ilość</span>' +
            '<input type="text" ticketCount min="0" ticketCount value="1" class="form-control field-count-bilets">' +
            '<span>szt.</span></div>' +
            '<div class="block-sum">' +
            '<div class="sum_title">Cena</div><div data="' + (course.price + course.foreign_price).toFixed(2) + '" ticketPrice class="money">' + (course.price + course.foreign_price).toFixed(2) + '<span> zł</span></div></div>' +
            '</div></div></div></div>' +
            '<div class="swal-wrap sum_row">' +
            '<div class="block-all-sum"><div class="sum_title">Razem</div><div sum-money class="swal-money">' + (course.price + course.foreign_price).toFixed(2) + '<span> zł</span></div></div></div>'
        );

        $('.swal2-modal .swal-select').styler();

        setTimeout(function() {
            $('.second_row .jq-selectbox__dropdown li:first-child').trigger('click');
        }, 1)

    }


    // This function is used to reserv tickets
    function reservTicket(course_id, ticket, resolve) {
        var obj = {};

        obj.station_from_id = ticket.station_from_id;
        obj.station_to_id = ticket.station_to_id;
        obj.date = ticket.departure.split(' ')[0].replace('.', '').replace('.', '');

        $.ajax({
            url: '',
            type: "post",
            dataType: "json",
            data: {
                method: "TicketsReserv",
                course_id: course_id,
                date: obj.date,
                station_from_id: obj.station_from_id,
                station_to_id: obj.station_to_id,
                quantity: parseInt($('[ticketCount]').val()),
                price_type: getUlgaType(),
                discount_code: getUlgaCode()
            },
            success: function (data) {
                if (data == 406){
                    swal('Oops ...', 'Błąd podczas wykonywania transakcji', 'error');
                }
                else if (!!data.error_message) {
                    swal({
                        title: "Błąd rezerwacji biletu!\r\n" + data.error_message,
                        type: "error"
                    });
                } else {
                    swal({
                        title: "Bilet został dodany do koszyka",
                        type: "success"
                    });

                    var reserve = localStorage.getItem('reserve');
                    if (reserve == null) {
                        localStorage.setItem('reserve', JSON.stringify({
                                reserve_num: data.reserv_number,
                                course_id: data.reserv_number,
                                id: course_id
                            }) + ';')
                    } else {
                        localStorage.setItem('reserve', reserve + JSON.stringify({
                                reserve_num: data.reserv_number,
                                course_id: data.reserv_number,
                                id: course_id
                            }) + ';')
                    }

                    ticket.course_id = data.reserv_number;
                    ticket.id = course_id;


                    if (localStorage.getItem('data') != null) {
                        localStorage.setItem('data', localStorage.getItem('data') + JSON.stringify(ticket) + ';')
                    } else {
                        localStorage.setItem('data', JSON.stringify(ticket) + ';')
                    }

                    renderBasket(ticket);
                }
            },
            error: function () {
                swal('Oops ...', 'Błąd podczas wykonywania transakcji', 'error')
            }
        });
    }


    function checkBasket(method, obj) {
        var param = {};
        param[method] = true;

        $.ajax({
            url: "",
            type: "post",
            dataType: "json",
            data: param,
            success: function () {
                obj.callback();
            }
        });
    }

    // Event handler for "remove from cart" button
    $("body").on("click", ".del-goods", function () {
        var tickets = localStorage.getItem('data').split(';');
        var $this = $(this).attr('data');

        tickets.pop();
        $(this).parents('.row-goods').fadeOut('slow', function () {
            deleteTickets(tickets, $this)
            newTickets(tickets)
            $(this).remove();
            priceAdd();
        });
    });

    function deleteTickets(tickets, course_id) {
        for (var i = 0; i < tickets.length; i++) {
            if (JSON.parse(tickets[i]).course_id == course_id) {
                var arr = localStorage.getItem("reserve");
                var reserveNumber = false;

                $.ajax({
                    url: '/pr2.php',
                    type: "post",
                    dataType: "json",
                    data: {
                        url: "TicketsDelete",
                        reserv_number: course_id
                    }
                });

                tickets.splice(i, 1);

                var storage = localStorage.getItem('data').split(';');
                if (storage.length == 2)
                    checkBasket(
                        "DestroyBasket",
                        {
                            callback: function() {}
                        });

                break;
            }
        }
    }

    function newTickets(tickets) {
        var newData = "";
        if (tickets.length > 0) {
            $('.block-user-basket').show('slow');
            $('.count-bilets').text(tickets.length);

            for (var n = 0; n < tickets.length; n++) {
                newData += tickets[n] + ';';
            }
        } else {
            $('.block-user-basket').hide('slow');
        }
        localStorage.setItem('data', newData);
    }


    // Prevent the basket from close
    $('.user-basket').on('hide.bs.dropdown', function () {
        return false
    });

    var basketCount = 0;
    $('.btn-bascket-user').click(function () {
        if (basketCount == 0) {
            $('.open .btn-bascket-user:after').css('display', 'block');
            $('#user-basket').css('display', 'block');
            basketCount = 1;
        } else {
            $('.open .btn-bascket-user:after').css('display', 'none');
            $('#user-basket').css('display', 'none');
            basketCount = 0;
        }
    });

    // Closing the basket
    $('.block-form-header').click(function () {
        if (basketCount == 1) {
            $('.btn-bascket-user').trigger('click');
        }
    });
    $('#render').click(function () {
        if (basketCount == 1) {
            $('.btn-bascket-user').trigger('click');
        }
    });
    $('.block-header').click(function () {
        if (basketCount == 1) {
            $('.btn-bascket-user').trigger('click');
        }
    });
    $('.wrap-middle').click(function () {
        if (basketCount == 1) {
            $('.btn-bascket-user').trigger('click');
        }
    });
    $('.btn-log-out').click(function () {
        $('#user-basket').css('display', 'none')
    });

// This function is used to get the count of bilets in busket
    $("body").on("click", ".btn-buy", function () {
        var check = true;
        if (check) {
            var array = [];
            $('.ticket_count').each(function () {
                array.push($(this).text());
            });
            localStorage.setItem('ticket_number', array);
            location.href = '/page-order.php';
        } else {
            swal('Oops ...', 'The ticket in your basket is overdue, delete please', 'error')
        }
    });
    // ==============================================


    // This function is used to get cities names and their id for points of destination A and B
    if (location.pathname == '/') {
        getCityNames($('#pointA'), 1);
        getCityNames($('#pointB'), 3);

        function getCityNames(element, param) {
            element.autocomplete({
                scroll: true,
                minLength: 1,
                autoFocus: true,
                close: function (event, ui) {
                    if (event.keyCode == 13 && !$(this).is(":button")) {
                     var tabindex = Number($(this).attr('tabindex')) + 1;
                        if(tabindex >= 3){
                            $('#search').trigger('click');
                           }else{
                            $('[tabindex="' + tabindex + '"]').focus();      
                           }
                     
                        
                    }
                },
                source: function (request, response) {
                    var reqData = {
                        url: "GetLocalityid",
                        locality_name: request.term.replace(' ', '%20'),
                        search_type: param
                    };

                    if (param === 3) {
                        try {
                            reqData['locality_from_id'] = dataStorageA.localityInfo.item.id;
                        } catch (e) {}
                    }

                    $.ajax({
                        url: '/pr2.php',
                        type: "post",
                        dataType: "json",
                        data: reqData,
                        success: function (data) {
                            element.removeClass('ui-autocomplete-loading');
                            var ui = data.contents.table;
                            response($.map(ui, function (i) {
                                var woj = (i.locality_province == "")
                                    ? ""
                                    : "<span class='woj-autocomplete'> woj: </span>" +
                                    "<span class='woj-name-autocomplete'>" + i.locality_province + "</span>";

                                var pow = (i.locality_district == "")
                                    ? ""
                                    : "<span class='region-autocomplete'> pow: </span><span class='district-autocomplete'>"
                                    + i.locality_district + "</span>";

                                var gm = (i.locality_commune == "")
                                    ? ""
                                    : "<span class='region-autocomplete'> gm: </span><span class='commune-autocomplete'>"
                                    + i.locality_commune + "</span>";

                                return {
                                    label: '<span class="city-autocomplete">'
                                    + i.locality_name + "</span><br/>"
                                    + "<span class='row-autocomplete'>" +
                                    "<span class='kraj-autocomplete'> kraj: </span>" +
                                    "<span class='kraj-name-autocomplete'>" + i.locality_country + "</span></span>"
                                    + woj + pow + gm,
                                    value: i.locality_name,
                                    province: i.locality_province,
                                    district: i.locality_district,
                                    id: i.locality_id
                                }
                            }))

                        },
                        error: function () {
                            swal('Oops ...', 'Błąd podczas wykonywania transakcji', 'error')
                        }
                    });
                },
                select: function (event, i) {
                    cityId = i.item.id;
                    locality_name = i.item.value;
                    province = i.item.province;
                    district = i.item.district;
                    if (element.selector == "#pointA") {
                        dataStorageA.localityInfo = i;
                        localStorage.setItem('dataStorageAa', JSON.stringify(dataStorageA.localityInfo));
                        localityStations(dataStorageA);
                    } else {
                        dataStorageB.localityInfo = i;
                        localStorage.setItem('dataStorageBb', JSON.stringify(dataStorageB.localityInfo));
                        localityStations(dataStorageB);
                    }
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>").data("item.autocomplete", item).append("<a>" + item.label + "</a>").appendTo(ul);
            };
        }
    }


    // This function is used to get stations names
    function localityStations(dataStorage) {
        var methodURL = "GetLocalityStations";
        $.ajax({
            url: '/pr2.php',
            type: "post",
            dataType: "json",
            data: {
                url: methodURL,
                locality_id: dataStorage.localityInfo.item.id
            },
            success: function (data) {
                dataStorage.localityStation = data;
            },
            error: function () {
                swal('Oops ...', 'Błąd podczas wykonywania transakcji', 'error')
            }
        });
    }


    // Reverse courses
    $('.reverse').click(function () {
        var inputA = $('#pointA').val(),
            inputB = $('#pointB').val();
        $('#pointA').val(inputB);
        $('#pointB').val(inputA);
        var mediator = dataStorageA;
        dataStorageA = dataStorageB;
        dataStorageB = mediator;
    });


    // This function is used to get courses
    function stationConnection() {
        console.log('start');
        var methodURL = "GetLocalityOrStationConnection";
        var date = parseInt($('#datetimepicker1').val().replace(/\D+/g, ""));

        var timeChange = $('#datetimepicker2').val().split(":");
        var timeMinute = Number(timeChange[1]);
        var timeHours = Number(timeChange[0]);

        if (timeMinute >= 60) {
            timeHours += 1;
            timeMinute -= 60;
        }
        if (timeHours == 24) {
            timeHours = '0' + 0;
        }
        if (timeMinute < 10) {
            timeMinute = '0' + timeMinute;
        }
        if (timeHours < 10) {
            timeHours = '0' + timeHours;
        }

        var time = String(timeHours) + String(timeMinute);

        $.ajax({
            url: '/pr2.php',
            type: "post",
            dataType: "json",
            data: {
                url: methodURL,
                station_or_locality_from_id: dataStorageA.localityInfo.item.id,
                station_or_locality_to_id: dataStorageB.localityInfo.item.id,
                is_station_from: 0,
                is_station_to: 0,
                date1: date,
                date2: date,
                time1: "00010101" + time + '00',
                time2: "00010101235959",
                z_bis: 0
            },
            success: function (data) {
                console.log(data);
                courses = data.contents.table;
                if (courses == undefined) {
                    $("#render").html('<div class="container"><div class="preloader">Brak sprzedaży biletów na wybranej trasie</div></div>')
                } else {
                    // coursesStorage(data);
                    renderData(courses);
                }
            },
            error: function () {
                swal('Oops ...', 'Błąd podczas wykonywania transakcji', 'error')
            }
        });
    }

    // This function is used to render the items with data about a courses
    function renderData(data) {
        var content = "<div class='block-route-schedule'><div class='container'><div class='block-filters animated fadeInUp'><div class='row'><div class='col-xs-3 col-sm-3 col-md-4 col-lg-4'>"
            + "<a class='title'>Wyjazd z</a></div><div class='col-xs-3 col-sm-3 col-md-4 col-lg-4'>"
            + "<a class='title'>Przyjazd do</a></div><div class='col-xs-3 col-sm-3 col-md-2 col-lg-2'>"
            + "<a class='title'>Czas podróży</a></div><div class='col-xs-3 col-sm-3 col-md-2 col-lg-2'>"
            + "<a class='title'>Cena</a></div></div></div>"
            + "<div id='accordion' class='panel-group'>";
        for (var i = 0; i < data.length; i++) {
            var departure = data[i].departure.split(' ')[1].match(/\d{1,2}:\d{1,2}/);
            var arrival = data[i].arrival.split(' ')[1].match(/\d{1,2}:\d{1,2}/);
            var test;
            (data[i].with_legal_discount == true) ? test = 'Bilet został dodany do koszyka. <br> <em style="color: lightcoral">Wybór ulgi będzie można dokonać przy finalizacji zakupu</em>' : test = 'Bilet został dodany do koszyka';
            content += "<div data='" + available(data[i]) + "' id='" + discount(data[i], i) + "' class='panel panel-default animated fadeInUp passive'><div class='panel-heading'><div id='row' class='row'><div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'><div class='row'><div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 start_block'><div class='title_row'>Start</div><div class='top-row'><div class='col-1'>"
                + "<div id='timeOne" + i + "' class='block-time'>" + departure + "</div>"
                + "<div class='col-2'><div class='route'>" + data[i].station_from
                + "</div></div></div>"
                + "<div class='start-date date'>" + showDate(data[i].departure.split(' ')[0]) + "</div></div>"
                + "</div><div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 stop_block'><div class='title_row'>Stop</div><div class='top-row'><div class='col-1'>"
                + "<div id='timeTwo" + i + "' class='block-time'>" + arrival[0] + "</div>"
                + "<div class='col-2'><div class='route'>" + data[i].station_to
                + "</div></div></div>"
                + "<div class='stop-date date'>" + showDate(data[i].arrival.split(' ')[0]) + "</div></div>"
                + "</div>"
                + "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4 right_part'><div class='col-xs-4 col-sm-4 col-md-4 col-lg-4 col-time'>"
                + "<div class='title_row'>Czas podróży: </div><div class='block-time-bus'>" + timeDiff(departure[0], arrival[0]) + "</div> </div> <div class='col-xs-8 col-sm-8 col-md-8 col-lg-8 price_col'>"
                + "<div style='width: 80%;' class='col-xs-5 col-sm-5 col-md-6 col-lg-6 up_part'>"
                + "<div class='title_row'>Cena: </div><div class='price'>" + (data[i].price + data[i].foreign_price).toFixed(2) + " zł"
                + "</div></div>"
                + "<div class='col-xs-7 col-sm-7 col-md-6 col-lg-6 down_part'>"
                + "<div class='wrap-btn'>"
                + "<a data-easein='slideUpIn' data-placement='top' data-html='true' data-content='" + test + "' class='btn btn-select' data='" + data[i].course_id + "'>Wybierz"
                + "<span style='display:none'>" + (data[i].price + data[i].foreign_price).toFixed(2) + "</span>"
                + "</a>"
                + "</div></div></div></div><div class='col-xs-12 col-sm-12 col-md-8 col-lg-8'>"
                + "<div class='places_row block-passengers'>" + "dostępne bilety: " + data[i].available_places + " szt.</div>"
                + "<div class='bottom-row clearfix'><div class='mobile_wrrap'><div class='col-1'>"
                + "<div class='model-bus'><div class='shadow_wrapp'>" + data[i].carrier.description + "</div></div><div class='hover_text'>" + data[i].carrier.description + " <br>Adres: " + data[i].carrier.address + "<br><em style='color: #1bbcef'>tel: " + data[i].carrier.phone + "</em></div></div><div class='col-3 trip_col'>"
                // + '<span class="title">Rodzaj kursu: </span>'
                + '<span class="trip-type">' + data[i].course_type + '</span>'
                + "</div></div><div class='col-3'>"
                + "<div class='start-station'>" + data[i].begin_station_locality + "</div></div><div class='col-4'>"
                + "<div class='hor-line'> - </div><div class='end-station'>" + data[i].end_station_locality + "</div></div><div class='col-2'><div class='category'>"
                + "<a href='#collapse" + i + "' data-toggle='collapse' data-parent='#accordion' class='link collapsed' data='" + data[i].course_id + "'>Szczegóły<span style='display: none'>" + data[i].course_type + "</span></a></div></div></div></div></div></div>"
                + "</div></div>"
                + "<div id='collapse" + i + "' class='panel-collapse collapse'> </div></div>";
        }
        content += "</div></div></div>";
        $('#render').html(content);

        $('div[data="non"]').each(function () {
            var panel = $(this).find('.panel-heading');
            panel.css('opacity', '0.3');
            panel.find('a').css('pointer-events', 'none');
            panel.find('a').css('cursor', 'default');
            // panel.find('.wrap-btn>a').css({'background': '#848484 url(../img/star-1.png) 90px center no-repeat;'})
        })
    }

    function discount(data) {
        if (data.with_discount == true) {
            return 'with-discount'
        }
    }

    function available(data) {
        if (data.sale_closed == false) {
            return 'available'
        } else {
            return 'non'
        }
    }

    // This functions is used to get course details
    $("body").on("click", ".link", function () {
        connectionFeatures($(this).attr('data'), $(this).attr('href'), $(this).find('span').text(), $(this).attr('data'))
    });

    function connectionFeatures(data, href, course_type, id) {
        var methodURL = "GetPartCourseConnectionFeatures";
        // var methodURL = "GetCourseData";
        var date = parseInt($('#datetimepicker1').val().replace(/\D+/g, ""));

        $.ajax({
            url: '/pr2.php',
            type: "post",
            dataType: "json",
            data: {
                url: methodURL,
                course_id: data,
                date: date,
                station_or_locality_from_id: dataStorageA.localityInfo.item.id,
                station_or_locality_to_id: dataStorageB.localityInfo.item.id,
                is_station_from: 0,
                is_station_to: 0
            },
            success: function (data) {
                var timeOne = $('#timeOne' + href.replace('#collapse', '')).text();
                var timeTwo = $('#timeTwo' + href.replace('#collapse', '')).text();
                var content = '<div class="panel-body">'
                    + '<div class="row-t"><div class="row"><div class="col-xs-12 col-sm-12 col-md-8 col-lg-7"><div class="row"> <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">'
                    + '<div class="title">Start</div></div><a href="" onclick="return false;" class="active-station"><div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">'
                    + '<div class="city-dots">' + data.contents.table[0].departure.split(' ')[1].match(/\d{1,2}:\d{1,2}/) + '</div></div><div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">'
                    + '<div class="city-name">' + data.contents.table[0].station_locality + ' / ' + data.contents.table[0].station + '</div>'
                    + '</div></div></a></div><div class="col-xs-12 col-sm-12 col-md-4 col-lg-5"><div class="row"><div class="col-xs-12 col-sm-12 col-md-6"><div class="bl-trip-type">'
                    + '</div></div></div></div></div>'
                    + '</div>';
                for (var i = 1; i < data.contents.table.length; i++) {

                    if (data.contents.table[i].arrival.split(' ')[1].match(/\d{1,2}:\d{1,2}/) == timeTwo) {
                        content += '<div class="row-t"><div class="row"><div class="col-xs-12 col-sm-12 col-md-8 col-lg-7"><div class="row"> <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">'
                            + '<div class="title">Stop</div></div><a href="" onclick="return false;" class="active-station"><div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">'
                            + '<div class="city-dots">' + data.contents.table[i].arrival.split(' ')[1].match(/\d{1,2}:\d{1,2}/) + '</div></div><div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">'
                            + '<div class="city-name">' + data.contents.table[i].station_locality + ' / ' + data.contents.table[i].station + '</div>'
                            + '</div></div></a></div></div><div class="col-xs-12 col-sm-12 col-md-4 col-lg-5"></div></div></div>';
                        i = 10000;
                    } else {
                        content += '<div class="row-t"><div class="row"><div class="col-xs-12 col-sm-12 col-md-8 col-lg-7"><div class="row"> <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">'
                            + '</div><a href="test" onclick="return false;"><div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">'
                            + '<div class="city-dots">' + data.contents.table[i].arrival.split(' ')[1].match(/\d{1,2}:\d{1,2}/) + '</div></div><div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">'
                            + '<div class="city-name">' + data.contents.table[i].station_locality + ' / ' + data.contents.table[i].station + '</div>'
                            + '</div></a></div></div><div class="col-xs-12 col-sm-12 col-md-4 col-lg-5"></div></div></div>';
                    }
                }
                content += "</div>";
                $('#collapse' + href.replace('#collapse', '')).html(content);
                connectionFeaturesStorage(data, id);

                station1 = data.contents.table[0].station_locality;
                station2 = data.contents.table[data.contents.table.length - 1].station_locality;
                differentStations(station1, station2);
            },
            error: function () {
                swal('Oops ...', 'Błąd podczas wykonywania transakcji', 'error')
            }
        });
    }

    /*function differentStations(station1, station2) {
     $('.city-name').each(function () {
     if (station1 != $(this).text().split(' /')[0] && station2 != $(this).text().split(' /')[0]) {
     $(this).parents('.col-xs-12').find('.col-lg-2').unwrap();
     }
     })
     }*/

    function connectionFeaturesStorage(data, id) {
        var storage = localStorage.getItem('features'),
            obj = {};

        obj.data = data.contents;
        obj.id = id;
        localStorage.setItem('features', JSON.stringify(obj));

    }


    // This function is used to select a station from and station to
    var station1;
    var station2;
    // $('body').on('click', '.panel-body a', function () {
    //     if ($(this).find('.city-name').text().split(' /')[0] == station1) {
    //         $(this).parents('.panel-body').find('.active-station:eq(0)').removeClass('active-station');
    //         $(this).addClass('active-station');
    //         newPrice($(this).parents('.panel-body'))
    //     } else {
    //         $(this).parents('.panel-body').find('.active-station:eq(1)').removeClass('active-station');
    //         $(this).addClass('active-station');
    //         newPrice($(this).parents('.panel-body'))
    //     }
    // });


//################################ ADDITIONAL FUNCTIONS ###################################################

    function basket() {
        // var newData = '';

        if (localStorage.getItem('data') != '') {
            var array = localStorage.getItem('data').split(';');
            var arrayDelete = [];
            array.pop();
            for (var i = 0; i < array.length; i++) {
                var countObj = {
                    date: /\d{2}.\d{2}$/.exec(JSON.parse(array[i]).departure.split(' ')[0])[0],
                    time: /\d{2}:\d{2}/.exec(JSON.parse(array[i]).departure.split(' ')[1])[0]
                };
                if (countTime(countObj) == true) {
                    renderBasket(JSON.parse(array[i]));
                } else {
                    arrayDelete.push(JSON.parse(array[i]).course_id);
                }
            }
            for (var n = 0; n < arrayDelete.length; n++) {
                deleteTickets(array, arrayDelete[n])
            }
            newTickets(array)
        } else {
            $('.block-user-basket').hide('slow');
        }
    }


    function renderBasket(ticket) {
        if (ticket.current_price != 'bezpłatny') {
            ticket.current_price = ticket.current_price.toFixed(2) + ' zł';
        }
        var content = '<div class="row-goods"><div class="row-g"><div class="title-distance">'
            + '<div class="col-1">'
            + '<div class="start-city">' + ticket.station_from + '</div>'
            + '<div class="start-time">' + ticket.departure.split(' ')[1].split(':')[0] + ':' + ticket.departure.split(' ')[1].split(':')[1] + '</div>'
            + '<div class="start-date date">' + ticket.departure.split(' ')[0] + '</div></div><div class="col-2">'
            + '<div class="end-city">' + ticket.station_to + '</div>'
            + '<div class="end-time">' + ticket.arrival.split(' ')[1].split(':')[0] + ':' + ticket.arrival.split(' ')[1].split(':')[1] + '</div>'
            + '<div class="stop-date date">' + ticket.departure.split(' ')[0] + '</div></div></div><div class="wrap-field">'
            + '<div class="amount_block"><div class="block_count"><span class="colored_title">Ilość: </span><div id="ticketCount" class="ticket_count">' + ticket.count + '</div>' + '<span>szt.</span></div>'
            + '<div class="price_block"><span class="colored_title">Cena: </span><div class="price">' + ticket.current_price + '</div></div></div><a onclick="return false;" href="" class="del-goods" data="'
            + ticket.course_id + '"></a></div>'
            + '<div class="row-g">'
            + '<div class="discount_row"><span class="discount_title colored_title">Rodzaj biletu: </span>' + ticket.current_ulga + '</div></div>'
            + '<div class="row-g">'
            + '<div class="model-detail">'
            + '<div class="basket-route">' + ticket.begin_station_locality + ' - ' + ticket.end_station_locality + '</div>'
            + '<div class="model-bus">' + ticket.carrier.description + '</div></div>'
            + '</div></div>';

        $('#renderBasket').append(content);

        var array = localStorage.getItem('data').split(';');
        array.pop();

        $('.block-user-basket').show('slow');
        $('.count-bilets').text(array.length);

        priceAdd();
    }


    // This function is used to get the summary price in the basket
    function priceAdd() {
        var sum = 0;
        $('.wrap-field').each(function () {
            var count = $(this).find('#ticketCount').text();
            var price = parseFloat($(this).find('.price').text());

            if (!isNaN(price)) {
                sum += parseFloat($(this).find('.price').text()) * count;
            }

        });
        $('.sum-money').text(sum.toFixed(2) + ' zł');
    }

    priceAdd();

    function hoverText() {
        $('.model-bus').each(function () {
            $(this).popover();
        });
    }


    // This function is used to sort rendered data on the page
    function dynamicSortASC(property) {
        var sortOrder = 1;
        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a, b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        };
    }


    // This function is used to sort rendered data on the page
    function dynamicSortDESC(property) {
        var sortOrder = 1;
        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a, b) {
            var result = (a[property] > b[property]) ? -1 : (a[property] < b[property]) ? 1 : 0;
            return result * sortOrder;
        };
    }


    // This function is used to calculate the difference between departure and arrival time
    function timeDiff(oclockone, oclocktwo) {
        var hour_one = Number(oclockone.match(/\d{1,2}/));
        var minute_one = Number(oclockone.match(/\d{2}$/));
        var hour_two = Number(oclocktwo.match(/\d{1,2}/));
        var minute_two = Number(oclocktwo.match(/\d{2}$/));

        if (hour_one > hour_two) {
            var hours = (hour_two + 24) - hour_one;
        } else {
            var hours = hour_two - hour_one;
        }
        var minutes = minute_two - minute_one;
        if (minutes < 0) {
            minutes += 60;
            hours -= 1;
        }
        if (hours < 0) {
            hours *= -1;
        }
        return hours + 'h ' + minutes + 'min';
    }


    // This function is used to calculate the selected date in datepicker
    function showDate(date) {
        var monthsArr = ["styczeń", "luty", "martha", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"];
        var days = Number(date.match(/\d{1,2}$/));
        var month = parseInt(date.split('.')[1]);
        return days + " " + monthsArr[month - 1];
    }


    function checkData(a, b) {
        if (a.localityInfo == undefined || $('#pointA').val() == '' || $('#pointA').val() !== a.localityInfo.item.value) {
            swal('Błąd', 'Proszę wybrać miejscowość z listy', 'error');
            return false
        }
        else if (b.localityInfo == undefined || $('#pointB').val() == '' || $('#pointB').val() !== b.localityInfo.item.value) {
            swal('Błąd', 'Proszę wybrać miejscowość z listy', 'error');
            return false
        }
        else {
            return true
        }
    }

    // Filters
    // This vars are used for filters
    var start = 0,
        stop = 0,
        time = 0,
        price = 0,
        btn = 0;

    // Start
    $("body").on("click", ".title:eq(1)", function () {
        if (start == 0) {
            courses.sort(dynamicSortDESC("departure"));
            start = 1;
            stop = 1;
            price = 0;
        } else {
            courses.sort(dynamicSortASC("departure"));
            start = 0;
            stop = 0;
            price = 0;
        }
        renderData(courses)
    });
    // Stop
    $("body").on("click", ".title:eq(2)", function () {
        if (stop == 0) {
            courses.sort(dynamicSortDESC("arrival"));
            stop = 1;
            start = 1;
            price = 0;
        } else {
            courses.sort(dynamicSortASC("arrival"));
            stop = 0;
            start = 0;
            price = 0;
        }
        renderData(courses)
    });
    // Time
    $("body").on("click", ".title:eq(3)", function () {
        if (time == 0) {
            courses.sort(dynamicSortDESC("duration_minutes"));
            price = 1;
            start = 1;
            stop = 1;
            time = 1;
        } else {
            courses.sort(dynamicSortASC("duration_minutes"));
            price = 0;
            start = 0;
            stop = 0;
            time = 0;
        }
        renderData(courses)
    });
    // Price
    $("body").on("click", ".title:eq(4)", function () {
        if (price == 0) {
            courses.sort(dynamicSortASC("price"));
            price = 1;
            start = 1;
            stop = 1;
        } else {
            courses.sort(dynamicSortDESC("price"));
            price = 0;
            start = 0;
            stop = 0;
        }
        renderData(courses)
    });
});