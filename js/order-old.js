$(document).ready(function () {
    var tickets = localStorage.getItem('data').split(';');
    tickets.pop();

    function numberTickets(i) {
        var number = localStorage.getItem('ticket_number').split(',');
        return number[i];
    }

    renderBasket();
    function renderBasket() {
        var content = "";
        for (var i = 0; i < tickets.length; i++) {
            content += '<div data="' + JSON.parse(tickets[i]).course_id + '" class="row-goods"><div class="row-g"><div class="title-distance"><div class="col-1">'
                + '<div class="col-title mobile_title">Z</div><div class="start-city">' + JSON.parse(tickets[i]).station_from + '</div>'
                + '<div class="start-time">' + JSON.parse(tickets[i]).departure.split(' ')[1].match(/\d{1,2}:\d{1,2}/) + '</div>'
                + '<div class="start-date date">' + JSON.parse(tickets[i]).departure.split(' ')[0] + '</div></div><div class="col-2">'
                + '<div class="col-title mobile_title">Do</div><div class="end-city">' + JSON.parse(tickets[i]).station_to + '</div>'
                + '<div class="end-time">' + JSON.parse(tickets[i]).arrival.split(' ')[1].match(/\d{1,2}:\d{1,2}/) + '</div>'
                + '<div class="stop-date date">' + JSON.parse(tickets[i]).departure.split(' ')[0] + '</div></div><div class="col-3"><div class="checkbox-buy"> <input type="checkbox" id="bilet-zbiorczy' + i + '"></div></div></div>' +
                '<div class="wrap-field">' +
                '<div class="count-tickets"><div class="col-title mobile_title">Ilość</div>' +
                '<div class="count-tickets value form-control" >' + numberTickets(i) + '</div>' +
                '<span>szt.</span> </div>'
                + '<div class="price_block"><div class="col-title mobile_title">Cena</div><div class="price" data="' + JSON.parse(tickets[i]).current_price.toFixed(2) + '">' + JSON.parse(tickets[i]).current_price.toFixed(2) + ' zł</div></div></div></div><div class="row-g"><div class="model-detail">'
                + '<div class="block-discount"><span class="discount_title colored_title">Rodzaj biletu: </span>' + JSON.parse(tickets[i]).current_ulga + ''
                + '</div><div class="basket-route">' + JSON.parse(tickets[i]).station_from + ' - ' + JSON.parse(tickets[i]).station_to + '</div>'
                + '<div class="model-bus">' + JSON.parse(tickets[i]).carrier.description + '</div>'
                + '</div></div></div>';
        }
        content += "";

        $.when($('#details').html(content)).done(function () {
            $('#renderTickets').attr('style', 'display:none')
        });
    }

    allPrice();
    $('body').on('keyup', '.count-tickets>input', function () {
        allPrice();
    });

    function allPrice() {
        var arr = [];
        var sum = 0;
        $('.value').each(function () {
            var count = Number($(this).text()),
                price = parseFloat($(this).parents('.row-goods').find('.price').text()) * count;
            if (!isNaN(price)) {
                arr.push(price);
                sum += price;
                $('.all-sum').text(sum.toFixed(2) + ' zł');
            } else {
                arr.push("0.00");
            }
        });
        $('.all-sum').text(parseFloat(sum).toFixed(2) + ' zł');
        return arr;
    }

    // $('#group_registration input').blur(function () {
    //     if ($(this).val().length < 3) {
    //         $(this).addClass('error_invoice');
    //     } else {
    //         $(this).removeClass('error_invoice');
    //     }
    // });


    $('.btn-go-payment').click(function () {
        var obj = {};
        var $this = $(this);
        var reserve = localStorage.getItem('reserve').split(';');
        reserve.pop();

        var array = [],
            reserveArray = [],
            arrayError = [];

        $('.block-form .row').find('input').focusin(function () {$(this).removeClass("error_invoice")});

        /** VALIDATION PROMISE */
        var validation = new Promise(function (resolve, reject) {
            var emailPattern = /^[.a-z0-9_-]+@[.a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i;
            var errors = [];

            $('.block-form .row').find('input').each(function () {
                var $this = $(this);
                var $id = $this.attr('id');

                return ($id === "passeg-email")
                    ? ($this.val().search(emailPattern) == 0) ? true : errors.push("#" + $id)
                    : ($this.val().length > 2) ? true : errors.push("#" + $id)
            });

            return (!errors.length) ? resolve() : reject(errors)
        });

        /** REJECT */
        validation.catch(function (result) {
            /** animate to fields */
            $('html, body').animate({
                scrollTop: $(".block-header").offset().top
            }, 500, false, function () {
                swal('Błąd', 'Proszę uzupełnić wszystkie wymagane informacje', 'error')
            });

            /** show errors */
            $.each(result, function(key, obj) {
                $(obj).addClass("error_invoice")
            });
        });

        /** RESOLVE */
        validation.then(function () {
            // if ($('.checkbox_faktury').is(':checked') == true && validationFacture($('[data="nip"]'), $('[data="code"]')) == false || $('.checkbox_faktury').is(':checked') == false) {

            if ($('#bilet-zbiorczy').is(':checked') == true && checkFormFactura() || $('.checkbox_faktury').is(':checked') == false )  {

                var errorInvoice = false;
                if (!errorInvoice) {
                    $this.addClass("m-progress");
                    $this.css("opacity", "0.6");

                    $this.click(function () {
                        return false
                    });

                    // if (arrayError == '') {

                        for (var i = 0; i < reserve.length; i++) {

                            for (var n = 0; n < tickets.length; n++) {

                                if (JSON.parse(reserve[i]).course_id == JSON.parse(tickets[n]).course_id) {
                                    var t = JSON.parse(tickets[n]);
                                    var r = JSON.parse(reserve[i]);
                                    var r_num = r.reserve_num;

                                    $('.row-goods').each(function () {

                                        if ($(this).attr('data') == r.course_id) {
                                            var discount_text = $(this).find('.block-discount').text();

                                            if (discount_text === 'Bez ulgi') {
                                                var a = 0;
                                            } else {
                                                var a = 6;
                                            }

                                            array.push({
                                                reserve_num: r_num,
                                                course_id: r.course_id,
                                                newPrice: $(this).find('.price').text(),
                                                discount: discount_text
                                            });

                                            reserveArray.push({reserve_num: r_num});
                                        }
                                    })
                                }
                            }
                        }

                        for (var i = 0; i < reserveArray.length; i++) {
                            if (reserveArray[i] == undefined) {
                                reserveArray.splice(i, 1)
                            }
                        }

                        $.ajax({
                            url: '/base64request.php',
                            type: "post",
                            data: {
                                reserv_number: JSON.stringify({reserv_number: reserveArray}),
                                type: "buy"
                            },
                            success: function (data) {
                                        
                                if (data == "Обработчик запроса вернул некорректное значение") {

                                } else {
                                    
                                    

                                    var table = JSON.parse(data).tickets_numbers;
                                    var equal = [];

                                    if (table.length) {
                                        for (var i = 0; i < table.length; i++) {
                                            if (table[i].saled == true) {
                                                equal.push(table[i].ticket_number)
                                            }
                                        }
                                    }

                                    // if (equal.length == array.length) {

                                    var dataToDB = [],
                                        invoise = [],
                                        datas = {},
                                        tickets_list = [];

                                    for (i = 0; i < tickets.length; i++) {

                                        for (var n = 0; n < array.length; n++) {

                                            if (JSON.parse(tickets[i]).course_id == array[n].course_id) {
                                                var obj = {};
                                                var parsed = JSON.parse(tickets[i]);

                                                obj.course_id = parsed.course_id;
                                                obj.reserve_num = array[n].reserve_num;
                                                obj.arrival = parsed.arrival;
                                                obj.departure = parsed.departure;
                                                obj.duration_minutes = parsed.duration_minutes;
                                                obj.carrier = parsed.carrier;
                                                obj.station_from = parsed.station_from;
                                                obj.station_to = parsed.station_to;
                                                obj.begin_station_locality = parsed.begin_station_locality;
                                                obj.end_station_locality = parsed.end_station_locality;
                                                obj.discount = array[n].discount;
                                                obj.locality_from = parsed.locality_from;
                                                obj.locality_to = parsed.locality_to;
                                                obj.email = $('#passeg-email').val();
                                                obj.fullPrice = parsed.price;
                                                obj.distance = parsed.distance;
                                                obj.price = allPrice();
                                                obj.ulga_type = parsed.ulga_type;
                                                obj.name = $('#name').val();
                                                obj.surname = $('#surname').val();
                                                obj.phone = $('#phone').val();
                                                obj.price_vat = parsed.price_vat;
                                                obj.count = getCountOfTickets(parsed.course_id);
                                                obj.current_price = parsed.current_price;
                                                obj.current_ulga = parsed.current_ulga;
                                                obj.foreign_price = parsed.foreign_price;
                                                dataToDB.push(obj);
                                                tickets_list.push(parsed.course_id);
                                            }
                                        }
                                        localStorage.setItem("tickets", JSON.stringify(dataToDB));
                                        

                                    }
                                
                                    
                                      if($('#bilet-zbiorczy').is(':checked') == true){
                                            datas.description = $('input[name="firm"]').val();
                                            datas.nip = $('input[name="nip"]').val();
                                            datas.postal_code = $('input[name="f_post_code"]').val();
                                            datas.country = $('input[name="f_country"]').val();
                                            datas.city = $('input[name="f_city"]').val();
                                            datas.street = $('input[name="f_street"]').val();
                                            datas.tickets_list = tickets_list;

                                            invoise.push(datas);
                                            
                                            localStorage.setItem("invoices_c", JSON.stringify(invoise));
                                        }else{
                                            localStorage.removeItem("invoices_c");
                                        }


                                    setTimeout(function () {
                                        localStorage.setItem('checkTicket', 'true');
                                        // location.href = '/ticket.php';

                                        (function () {
                                            var email = $('#passeg-email').val();
                                            var amount = parseFloat($('.all-sum').text().replace('.', ''));
                                            // if (amount.length < 4) amount += '0';

                                            $.ajax({
                                                url: '/payment.php',
                                                type: "post",
                                                data: {
                                                    payment: true,
                                                    email: email,
                                                    amount: amount,
                                                    tickets: localStorage.getItem("tickets"),
                                                    invoices: localStorage.getItem("invoices_c")
                                                },
                                                success: function (res) {
                                                    
                                                    location.href = res;
                                                }
                                            })
                                        })();

                                    }, 400)
                                }
                            }
                        });
                } else {
                    swal('Błąd', 'Jeden lub więcej pól w fakturze zostanie wprowadzony niepoprawnie', 'error')
                }
            }
            else {
                $('html, body').animate({
                    scrollTop: $(".faktura_title").offset().top - 450
                }, 500, false, function () {
                    swal('Błąd', 'Podaj dane do faktury', 'error')
                });
            }
        });

    });

    function getCountOfTickets(id) {
        var obj = {
            count: $('.row-goods[data="' + id + '"]').find('.form-control').text(),
            check: $('.row-goods[data="' + id + '"]').find('.checkbox-buy input[type = checkbox]').is(':checked')
        };
        return obj;
    }

    // $('.page-order .checkbox-buy input[type="checkbox"] + label span').click(function () {
    $('.page-order .block-form > .checkbox-buy > label > span').click(function () {
        $(this).parents('.checkbox-buy input[type="checkbox"] + label').toggleClass('checked');
        $('.page-order #group_registration').slideToggle();
    });

    $('.btn_download_pdf').on('click', function(){
        
    });
    
});