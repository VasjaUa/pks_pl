function validationFacture(nip, code) {
    var error = false;
    if (nip.val().length == 10) {
        nip.removeClass('error_invoice');
    } else {
        nip.addClass('error_invoice');
        error = true;
    }
    if (code.val().length < 6) {
        code.addClass('error_invoice');
        error = true;
    } else {
        code.removeClass('error_invoice');
    }
    return error;
}

function countTime(obj) { // time:00:00, date:00.00
    var check = false;
    var dateObj = new Date();
    var newBestDate = new Date(
        dateObj.getFullYear(),
        Number(obj.date.split('.')[0]) - 1,
        Number(obj.date.split('.')[1]),
        Number(obj.time.split(':')[0]),
        Number(obj.time.split(':')[1])
    ).valueOf();

    $.ajax({
        url: '/helpers/date.php',
        type: "post",
        async: false,
        dataType: "json",
        success: function (data) {
            if (((newBestDate / 1000) - data) >= 900) {
                check = true;
            }
        },
        error: function () {
            console.log('error')
        }
    });
    return check;
}