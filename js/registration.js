$(document).ready(function () {


    $("#btn_group_registration").click(function () {
        var display = $("#invoice_block").css("display");
        $("#invoice_block").slideToggle('300');
        if(display!="none") {
            $("#invoice_block").attr("style", "display:none");
        }else {
            $("#invoice_block").attr("style", "display:block");
        }
    });

    // Умолчания для валидации
    $.validator.setDefaults({
        debug: true,
        success: "valid"
    });

    $.validator.addMethod("matches", function (value, element, params) {
        var reg = new RegExp(params);
        var result = reg.exec(value);

        return !isNaN(result[0]);

    }, "Please specify a valid number");

    // Формы для валидации
     validation($('.block-registration'));

    // Функция валидации
    function validation(modal) {
        var form = modal.find('form');
        $(form).validate({
            errorClass: 'modal-error-text',
            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true,
                    matches: /[+0-9]+/
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                repeat_password: {
                    equalTo: "#password"
                },
                'agree-to-receive': {
                    required: true
                },
                regulamin: {
                    required: true
                }
            },
            submitHandler: function () {

                var response = grecaptcha.getResponse();
                if (response.length > 0) {

                    var login = $('#login1').val(),
                        email = $('#email').val(),
                        errors = false;
                    if ( checkAgreement() )
                        errors = true;


                    $.ajax({
                        url: "/registration.php",
                        type: "POST",
                        data: {
                            login1: login,
                            email1: email
                        },
                        success: function (data) {
                            if ( (($('#btn_group_registration').is(':checked') == true && checkFormFactura() ) || $('#btn_group_registration').is(':checked') == false  ) && !errors )  {
                                console.log(84);
                                console.log(!errors);
                                if ($('#recaptcha-anchor').find('span[aria-checked="true"]')) {
                                    if (data == 'true') {
                                        ajaxSend(modal);
                                    } else if (data == 'login') {
                                        swal('Ten login jest już zarejestrowany', '', 'error')
                                    } else {
                                        swal('Ten email jest już zarejestrowany', '', 'error')
                                    }
                                } else {
                                    swal('Błąd', 'Aby kontynuować, musisz zgodzić się na warunki w miejscu', 'error')
                                }
                            }
                            else {
                                console.log(98);
                                  swal('Błąd', 'Niektóre pola są wypełnione źle', 'error')
                            }
                        },
                        error: function () {

                        }
                    });
                } else {
                    alert('Zaznacz "Nie jestem robotem"')
                }
            },
            invalidHandler: function (event, validator) {
                if ($('#btn_group_registration').is(':checked'))
                    checkFormFactura();
                var errors = validator.numberOfInvalids();

                checkAgreement();

                if (errors) {
                    swal('Błąd', 'Niektóre pola są wypełnione źle', 'error')
                }
            }
        });
    }

    function checkAgreement(){
        var errors = false;
        $('.req').each(function () {
            var t = $(this);
            var label = $('[for="'+ t.attr("name") +'"]');
            var err = label.next();

            console.log(132);
            console.log(t.is(":checked"));
            if (!t.is(":checked")) errors = true;

            // return (t.is(":checked"))
            //     ? err.css("display", "none")
            //     : err.css("display", "block");
            (t.is(":checked"))
                 ? err.css("display", "none")
                 : err.css("display", "block");
        });
        return errors;//My
    }

    // Функция отправки данных по ajax
    function ajaxSend(modal) {
        var form = $(modal).find('form'),
            formData = new FormData(form.get(0));
        formData.append('firm', $('[data="firm"] input').val());
        formData.append('nip', $('[data="nip"] input').val());
        formData.append('f_post_code', $('[data="f_post_code"] input').val());
        formData.append('f_country', $('[data="f_country"] input').val());
        formData.append('f_city', $('[data="f_city"] input').val());
        formData.append('f_street', $('[data="f_street"] input').val());

        $.ajax({
            url: "/registration.php",
            type: "POST",
            data: formData,
            processData: false, // Не обрабатываем файлы
            contentType: false, // Так jQuery скажет серверу что это строковой запрос
            success: function () {
                $(form)[0].reset();
                swal('Dziękuję Ci', 'Zarejestrowałeś się pomyślnie. E-mail z aktywacją został wysłany pocztą elektroniczną', 'success');
            }
        });

        $.ajax({
            url: "/mailer.php",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false
        });
    }


    // Настройка своих уведомлений при заполнении формы
    $.extend($.validator.messages, {
        required: "To pole jest wymagane",
        email: "Nie poprawny e-mail",
        number: "Nie poprawny numer",
        minlength: "Wprowadź co najmniej {0} znaków",
        equalTo: "Proszę wprowadzić ponownie tę samą wartość hasła."
    });
});

