$(document).ready(function () {
    // $('body').on('click', '.ui-timepicker-hours', function () {
    //     console.log('test')
    /*var Month = new Date().getMonth() + 1,
     Day = new Date().getDate(),
     Hour = new Date().getHours(),
     Minute = new Date().getMinutes();


     console.log(this)
     console.log('test')
     $('.ui-timepicker-hours a').each(function () {
     if (Number($(this).text()) < Hour) {

     $(this).parent().replaceWith('<td><span class="ui-state-default ui-state-disabled">' + $(this).text() + '</span></td>');
     // console.log(Number($(this).text()))

     }

     });*/
    // <td><span class="ui-state-default ui-state-disabled  ">09</span></td>
    // console.log('test');

    // })
    // $(".ui-timepicker-hours").on("click", function () {
    //     console.log('test')

    // });
    //Цели для Яндекс.Метрики и Google Analytics
    $(".count_element").on("click", (function () {
        ga("send", "event", "goal", "goal");
        yaCounterXXXXXXXX.reachGoal("goal");
        return true;
    }));

    //SVG Fallback
    if (!Modernizr.svg) {
        $("img[src*='svg']").attr("src", function () {
            return $(this).attr("src").replace(".svg", ".png");
        });
    }


    $('#menu').slicknav({
        label: '',
        closeOnClick: true
    });


    //Chrome Smooth Scroll
    try {
        $.browserSelector();
        if ($("html").hasClass("chrome")) {
            $.smoothScroll();
        }
    } catch (err) {

    }

    $("img, a").on("dragstart", function (event) {
        event.preventDefault();
    });

});


$(function () {
    var date = new Date(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        month = date.getMonth() + 1,
        day = date.getDate();

    if (minutes < 0) {
        minutes += 60;
        hours -= 1;
    }
    hours = String(hours);
    minutes = String(minutes);
    if (hours < 10) {
        hours = '0' + hours;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }

    $("#datetimepicker1").datepicker({
        onSelect: function (e) {
            var date = e;
            $("#datetimepicker2").val(hours + ':' + minutes);
        },
        dateFormat: 'yy-mm-dd',
        maxDate: "+1m",
        changeMonth: true,
        changeYear: true,
        minDate: 0
    }).datepicker("setDate", date);


    $("#datetimepicker2").val(hours + ':' + minutes);


    $("#datetimepicker3").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: '1940:+0'
    });

    const checkOffset = $.datepicker._checkOffset;

    $.extend($.datepicker, {
        _checkOffset: function (inst, offset, isFixed) {
            if (isFixed) {
                return checkOffset.apply(this, arguments);
            } else {
                return offset;
            }
        }
    });

    /*$(window).on('resize orientationchange', function (e) {
     if ($.datepicker._datepickerShowing) {
     var datepicker = $.datepicker._curInst;
     dpInput = datepicker.input;
     dpElem = datepicker.dpDiv;
     dpElem.position({
     my: 'left top',
     of: dpInput,
     at: 'left bottom'
     });
     }
     });*/
    /*$('#datetimepicker1').datepicker({
     language: "pl",
     defaultDate: new Date(),
     "autoclose": true
     });*/

    /*$('.slider-middle').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: false,
     fade: true,
     autoplay: true,
     autoplaySpeed: 4000,
     speed: 1500,
     dots:false,
     cssEase: 'linear'
     });*/

    $('.carousel-company').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 1500,
        dots: false,
        vertical: false,
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    //$('.slider-middle').show();

    /*if ($("body").hasClass("body")) {
     var $slider = $('.slider-middle')
     .on('init', function(slick) {
     //console.log('fired!');
     $('.slider-middle').fadeIn(1000);
     })
     .slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: false,
     fade: true,
     autoplay: true,
     autoplaySpeed: 4000,
     speed: 1500,
     dots:false,
     cssEase: 'linear',
     fade: true,
     focusOnSelect: true,
     speed: 1000
     });
     }*/

    function checkSlider() {
        if ($("body").hasClass("body")) {
            var $slider = $('.slider-middle')
                .on('init', function (slick) {
                    //console.log('fired!');
                    $('.slider-middle').fadeIn(1000);
                })
                .slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    autoplay: true,
                    autoplaySpeed: 4000,
                    speed: 1500,
                    dots: false,
                    cssEase: 'linear',
                    fade: true,
                    focusOnSelect: true,
                    speed: 1000
                });
        } else {
            $('.slider-middle').css("display", "none");
        }
    }

    checkSlider();
    $('#search').click(function () {
        checkSlider();
    });


    /*$("#file-3").fileinput({
     showUpload: false,
     showCaption: false,
     browseClass: "btn btn-primary btn-file-photo",
     fileType: "any"
     });*/

    $("#file-3").fileinput({
        language: "pl",
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-primary btn-file-photo",
        fileType: "any",
    });

    $('.dropdown-submenu a.drop-link').on("click", function (e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });


});
/*
 $(window).on('load resize', function(){
 if ($(window).width() > 991){

 }
 })*/

new WOW().init();