<?PHP

// Change these configuration options if needed, see above descriptions for info.
$enable_jsonp = false;
$enable_native = false;
$valid_url_regex = '/.*/';

//$log_file = "/home/frido/public_html/log/pr.log";
$log_file = "/r2/log/pr.log";

// ############################################################################

function write_log($message, $logfile = '')
{
    // Determine log file
    if ($logfile == '') {
        // checking if the constant for the log file is defined
        if (defined(DEFAULT_LOG) == TRUE) {
            $logfile = DEFAULT_LOG;
        } // the constant is not defined and there is no log file given as input
        else {
            error_log('No log file defined!', 0);
            return array(status => false, message => 'No log file defined!');
        }
    }

    // Get time of request
    if (($time = $_SERVER['REQUEST_TIME']) == '') {
        $time = time();
    }

    // Get IP address
    if (($remote_addr = $_SERVER['REMOTE_ADDR']) == '') {
        $remote_addr = "REMOTE_ADDR_UNKNOWN";
    }

    // Get requested script
    if (($request_uri = $_SERVER['REQUEST_URI']) == '') {
        $request_uri = "REQUEST_URI_UNKNOWN";
    }

    // Format the date and time
    $date = date("Y-m-d H:i:s", $time);

    // Append to the log file
    if ($fd = @fopen($logfile, "a")) {
        $result = fputcsv($fd, array($date, $remote_addr, $request_uri, $message));
        fclose($fd);

        if ($result > 0)
            return array(status => true);
        else
            return array(status => false, message => 'Unable to write to ' . $logfile . '!');
    } else {
        return array(status => false, message => 'Unable to open log ' . $logfile . '!');
    }
}

header('Access-Control-Allow-Origin: www.pks.pl');

if ($_POST['url']) {
    $_GET = $_POST;
}

include_once ("helpers/server.php");
//$url = "http://Administrator:Inkognito@192.168.0.5:8080/mda_test/hs/api/" . $_GET['url'];
//$url = "http://Administrator@46.164.134.94:15152/MDA/hs/api/" . $_GET['url'];
$url = $server . $_GET['url'];

//$url = 'http://82.160.155.29:10059/TicketsSale?reserv_number='.base64_encode($_GET['reserv_number']);

if (!$url) {

    // Passed url not specified.
    $contents = 'ERROR: url not specified';
    $status = array('http_code' => 'ERROR');

} else if (!preg_match($valid_url_regex, $url)) {

    // Passed url doesn't match $valid_url_regex.
    $contents = 'ERROR: invalid url';
    $status = array('http_code' => 'ERROR');

} else {
//    print_r('3');

    $tmp_arg = $_GET;
    unset ($tmp_arg['url']);

    if ($_GET['url'] == "TicketsSale"){
          $url = 'http://82.160.155.29:10059/TicketsSale?reserv_number='.base64_encode($_GET['reserv_number']);
    } else {
        $tmp_param = "";
        foreach ($tmp_arg as $name => $tmp_a) {
            $tmp_param = $tmp_param . $name . "=" . $tmp_a . "&";
        }
        $url = $url . "?" . $tmp_param;
    }

    write_log($url,$log_file);

    // Start request
    $ch = curl_init($url);

    if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
    }

    if ($_GET['send_cookies']) {
        $cookie = array();
        foreach ($_COOKIE as $key => $value) {
            $cookie[] = $key . '=' . $value;
        }
        if ($_GET['send_session']) {
            $cookie[] = SID;
        }
        $cookie = implode('; ', $cookie);

        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    }

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_USERAGENT, $_GET['user_agent'] ? $_GET['user_agent'] : $_SERVER['HTTP_USER_AGENT']);

    //  list( $header, $contents ) = preg_split( '/([\r\n][\r\n])\\1/', curl_exec( $ch ), 2 );
    $header = "";
//    print_r($url);
//    die();
    $contents = curl_exec($ch);

    $status = curl_getinfo($ch);

    write_log($contents, $log_file);

    curl_close($ch);
}

// Split header text into an array.
$header_text = preg_split('/[\r\n]+/', $header);

if ($_GET['mode'] == 'native') {
    if (!$enable_native) {
        $contents = 'ERROR: invalid mode';
        $status = array('http_code' => 'ERROR');
    }

    // Propagate headers to response.
    foreach ($header_text as $header) {
        if (preg_match('/^(?:Content-Type|Content-Language|Set-Cookie):/i', $header)) {
            header($header);
        }
    }

    print $contents;

} else {

    function cmp($a, $b){
        return strcmp($a->name, $b->name);
    }

    // $data will be serialized into JSON data.
    $data = array();

    // Propagate all HTTP headers into the JSON data object.
    if ($_GET['full_headers']) {
        $data['headers'] = array();

        foreach ($header_text as $header) {
            preg_match('/^(.+?):\s+(.*)$/', $header, $matches);
            if ($matches) {
                $data['headers'][$matches[1]] = $matches[2];
            }
        }
    }

    // Propagate all cURL request / response info to the JSON data object.
    if ($_GET['full_status']) {
        $data['status'] = $status;
    } else {
        $data['status'] = array();
        $data['status']['http_code'] = $status['http_code'];
    }

    // Set the JSON data object contents, decoding it from JSON if possible.
    $decoded_json = json_decode($contents);
    $data['contents'] = $decoded_json ? $decoded_json : $contents;

    // Generate appropriate content-type header.
    $is_xhr = strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    header('Content-type: application/' . ($is_xhr ? 'json' : 'x-javascript'));

    // Get JSONP callback.
    //  $jsonp_callback = $enable_jsonp && isset($_GET['callback']) ? $_GET['callback'] : null;

    // Generate JSON/JSONP string
    $json = json_encode($data);

//  print $jsonp_callback ? "$jsonp_callback($json)" : $json;
    print $json;
}
?>
