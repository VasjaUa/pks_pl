<?php
require_once './class_przelewy24.php';
include_once('invoice.php');
include_once('helpers/server.php');


//ini_set('display_errors', 1);
$connection = mysql_connect("localhost", "pkskrakow_4", "Dond33st4S!");
$db = mysql_select_db("pkskrakow_4");
mysql_query("SET NAMES 'utf8'");

if (!$connection || !$db) exit(mysql_error());

$select = "SELECT t.* FROM transactions t left JOIN userdata u on u.idUser = t.user_id WHERE t.id = " . $_POST['p24_session_id'];

$query = mysql_query($select);
$transaction = mysql_fetch_assoc($query);

if (!$transaction) exit();

$P24 = new Przelewy24(58945, 58945, "bab489994ba3d2e3", true);
//$P24 = new Przelewy24(58945, 58945, "7f5dfcb9e436e4fd");

//Result verification:
$P24->addValue("p24_session_id", $transaction['id']);
$P24->addValue("p24_amount", $transaction['amount']);
$P24->addValue("p24_currency", $transaction['currency']);
$P24->addValue("p24_order_id", $_POST["p24_order_id"]);
$RET = $P24->trnVerify();

if (isset($RET["error"]) and $RET["error"] === '0') {
// Transaction correct
    $query = "update transactions set status = 1, p24_order_id = " . $_POST["p24_order_id"] . " where id = " . $transaction['id'];
    mysql_query($query);
    $transaction_with_all_data = commitTransaction($transaction, true);
    sendMail($transaction_with_all_data);
    saveTickets($transaction_with_all_data);
} else {
// Error, error message in field $RET["errorMessage"]
    $query = "update transactions set p24_order_id = " . $_POST["p24_order_id"] . ", error = '" . json_encode($RET) . "' where id = " . $transaction['id'];
    mysql_query($query);
    commitTransaction($transaction, false);
}


function sendMail($transaction)
{
    include_once 'libs/php_mailer/class.phpmailer.php';
    include_once 'libs/php_mailer/class.smtp.php';
    include_once('admin/config.php');
    include_once('admin/startup.php');
    include_once('admin/Model/Model.php');
    startup();

    $db = Model::Instance();
    $letter = $db->Array_where('sections', "WHERE id='8'")[0];

    $MailErrors = array();
    $mailer = new PHPMailer(true);

    try {
        //server settings
        $mailer->isSMTP();
        $mailer->Host = 'pkskrakow.nazwa.pl';
        $mailer->SMTPDebug = 2;
        $mailer->SMTPAuth = true;
        $mailer->Port = 25;
        $mailer->Username = 'ticket@pks.pl';
        $mailer->Password = 'SZczot3czk4!';
        $mailer->CharSet = 'UTF-8';
        $mailer->isHTML(true);
        //recipients
        $mailer->setFrom('ticket@pks.pl');
        $mailer->FromName = 'PKS.PL';

        $tickets = json_decode($transaction['tickets'], true);
        $ticket_numbers = " ";
        $i = 0;

        foreach ($tickets as $ticket) {
            $comma = ($i == 0) ? "" : ",";
            $ticket_numbers .= $comma . $ticket["Number"];
            $i++;
        }

        $mailer->Subject = $letter["value"] . $ticket_numbers;
        $mailer->Body = "<html lang='pl'>" . str_replace('{bilet}', $ticket_numbers, $letter['value1']) . "</html>";
        $mailer->addAddress($transaction['email']);
        //$mailer->addAddress('avitlauak@gmail.com');
        /**
         * attachment
         */
        try {
            $mailer->addAttachment(generatePDF($transaction, false, null));
        } catch (Exception $e) {
            file_put_contents('log/error', print($e));
        }

        if (isset($transaction['invoices']) && $transaction['invoices'] != '') {
            generateInvoice($transaction);

            $buffer = needSomeInv($transaction);

            $name = 'faktura_' . $transaction['id'] . '.pdf';

            $tmpName = tempnam(sys_get_temp_dir(), $name);
            $file = fopen($tmpName, 'w');

            fwrite($file, $buffer);
            fclose($file);

            $mailer->addAttachment(realpath($tmpName));
        }

        $mailer->send();
        echo 'message has been sent.';
    } catch (Exception $e) {
        file_put_contents('log/error', print($mailer->ErrorInfo));
    }
}


/**
 * Function for generating pdf
 * @param $transaction
 */


function saveTickets($transaction)
{
    $tickets = json_decode($transaction['tickets'], true);
    $sql = "insert into tickets (
                    transaction_id, Number, FiscalNumber, Bidder, number_ticket, discount, 
                    station_from, station_to, departure_time, 
                    arrival_time, total_time_road, price, 
                    status, id, carrier, begin_station_locality, end_station_locality) values ";

    foreach ($tickets as $key => $ticket) {
        $sql .= "(" . $transaction['id'] . ",'" . $ticket['Number'] . "','" . $ticket['FiscalNumber'] . "','" . $ticket['Bidder'] . "','" . $ticket['course_id'] . "','" . $ticket['discount'] . "','" .
            $ticket['station_from'] . "','" . $ticket['station_to'] . "','" . $ticket['departure'] . "','" .
            $ticket['arrival'] . "','" . $ticket['duration_minutes'] . "','" . $ticket['current_price'] * $ticket['count']['count'] . "','" .
            $transaction['status'] . "','" . $transaction['user_id'] . "','" . $ticket['carrier']['description'] . "','" .
            $ticket['begin_station_locality'] . "','" . $ticket['end_station_locality'] . "')";
        if (isset($tickets[$key + 1])) $sql .= ',';
    }

    mysql_query($sql);
}

/**
 * @param $transaction array
 * @param $isSale bool
 */
function commitTransaction($transaction, $isSale)
{
    $tickets = json_decode($transaction['tickets'], true);
    $data = [];
    $customer = [
        "passager_firstname" => $tickets[0]['name'],
        "passager_lastname" => $tickets[0]['surname'],
        "passager_telephone" => $tickets[0]['phone'],
        "passager_mail" => $tickets[0]['email']
    ];

    foreach ($tickets as $ticket) {
        $data[] = $ticket['reserve_num'];
    }

    $data = base64_encode(json_encode($data));
    $sold = ($isSale) ? 'TicketsSale' : 'TicketsNotSale';

    $url = "http://82.160.155.29:10059/" . $sold . "?reserv_number=" . $data . "&" . http_build_query($customer);
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, "reserv_number=" . $data . "&" . http_build_query($customer));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);
    $server_output = json_decode($server_output, true)['tickets_numbers'];

    for ($i = 0; $i < count($server_output); $i++) {
        $tickets[$i]["Number"] = $server_output[$i]["Number"];
        $tickets[$i]["FiscalNumber"] = $server_output[$i]["FiscalNumber"];
        $tickets[$i]["Bidder"] = $server_output[$i]["Bidder"];
        $tickets[$i]["MarketDescription"] = $server_output[$i]["MarketDescription"];
    }

    curl_close($ch);

    $transaction['tickets'] = json_encode($tickets, true);

    return $transaction;
}

function needSomeInv($transaction){

    $perem = json_decode($transaction['invoices'], true);

    include_once("helpers/session/Helper.php");

    $helper = new Helper();

    $reqData = array(
        "method" => "CreateSalesInvoice",
        "tickets_list" => $perem[0]['tickets_list'],
        "description" => $perem[0]['description'],
        "nip" => $perem[0]['nip'],
        "postal_code" => $perem[0]['postal_code'],
        "country" => $perem[0]['country'],
        "city" => $perem[0]['city'],
        "street" => $perem[0]['street']
    );

    $output = json_decode($helper::apiRequest($reqData), true);

    $udp_query = 'UPDATE `transactions` SET `response` = "'. $output['PrintFile'] .'", `invoice_number` =  "'. $output['InvoiceNumber'] .'", `firm` = "'. $output['Firm'] .'", `uuid` = "'. $output['UUID'] .'", `SalesDate` = "'. $output['SalesDate'] .'", `Exhibitor` = "'. $output['Exhibitor'] .'", `DocumentTotal` = "'. $output['DocumentTotal'] .'"  WHERE `id` = '. $transaction['id'];

mysql_query($udp_query);

return base64_decode($output['PrintFile']);

}