<?php
ini_set('display_errors', 1);
$connection = mysql_connect("localhost", "pkskrakow_4", "Dond33st4S!");
$db = mysql_select_db("pkskrakow_4");
mysql_query("SET NAMES 'utf8'");


if (!$connection || !$db) {
    exit(mysql_error());
}
$select = "SELECT t.* FROM transactions t left JOIN userdata u on u.idUser = t.user_id WHERE t.id =675";
//$selectTicket = "SELECT * FROM tickets WHERE number_ticket ='K0000897732'";

//$query2 = mysql_query($selectTicket);
//$ticket = mysql_fetch_assoc($query2);

$query = mysql_query($select);
$transaction = mysql_fetch_assoc($query);
generatePDF($transaction, '$additionalData');

function generatePDF($transaction, $ticket)
{
    include_once('libs/mpdf50/mpdf.php');

    session_start();
    $mpdf = new mPDF('utf-8', '', 0, 'ebrima', 10, 10, 7, 7, 10, 10, "P");
    $mpdf->charset_in = 'utf-8';

    $mpdf->WriteHTML(file_get_contents('./css/ticket.css'), 1);
    $mpdf->WriteHTML(renderTicketPdf($transaction, $ticket), 2);
    $mpdf->Output("testpdf.pdf", 'I');
}

function getTicketTypePl($ticket)
{
    if ($ticket['fullPrice'] == $ticket['current_price']) return 'Normalny';
    else return 'Ulga '.$ticket['ulga_type'] .' - '. $ticket['current_ulga'];
}

function getTicketTypeEn($ticket)
{
    if ($ticket['fullPrice'] == $ticket['current_price']) return 'Normal ticket';
    else return 'Reduced-fare ticket';
}

function renderTicketPdf($transaction, $ticketNumber)
{
    $connection = mysql_connect("localhost", "pkskrakow_4", "Dond33st4S!");
    $db = mysql_select_db("pkskrakow_4");
    mysql_query("SET NAMES 'utf8'");

    $tickets = json_decode($transaction['tickets'], true);
    $html = '';

    for ($i = 0; $i < 3; $i++) {

        foreach ($tickets as $ticket) {

            if ($ticketNumber !== null) {
                $selectTicket = "SELECT * FROM tickets WHERE number_ticket ='K0000897732'";
                $query = mysql_query($selectTicket);
                $additionalData = mysql_fetch_assoc($query);
                $ticket["FiscalNumber"] = $additionalData["FiscalNumber"];
                $ticket["Number"] = $additionalData["Number"];
            }

            $full_price = number_format($ticket['current_price'] * $ticket['count']['count'], 2);
            $vat_price = number_format($full_price * 8 / 108, 2);
            $net_price = number_format($full_price - $vat_price, 2);

            $brutto = array(
                'text' => '',
                'price' => ''
            );

            if (($ticket['foreign_price'] != 0)) {
                $brutto['text'] = 'Brutto.zagr';
                $brutto['price'] = number_format($ticket['foreign_price'], 2) . ' zł';
            } else {
                $i = 3;
            }

            $total_price = number_format($vat_price + $net_price + $ticket['foreign_price'], 2);

            $html .= '
<table border="1" style="width: 100%;">
    <tr style="display: none;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="1" style="text-align: center; border: 1px solid black;height: 40px;"><img src="./img/logo-pks-pl.png" /></td>
        <td colspan="5" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Nr biletu</small><p><b class="sm-f">' . $ticket['Number'] . '</b></p></span></td>
        <td colspan="4" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Sposób sprzedaży</small><p><b class="sm-f">Internet</b></p></span></td>
        <td colspan="4" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Data sprzedaży</small><p><b class="sm-f">' . date('Y-m-d') . '</b></p></span></td>
        <td colspan="4" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Nr paragonu fiskalnego</small><p><b class="sm-f">' . $ticket["FiscalNumber"] . '</b></p></span></td>
    </tr>

    <tr>
        <td colspan="3" style="border-left:1px solid black;padding-left:3px">
            <small>Sprzedawca (Przewoźnik)</small>
        </td>
        <td colspan="3"></td>
        <td colspan="4"><small class="vsm-f">NIP: ' . $ticket['carrier']['vat_number'] . '</small></td>
        <td style="border-left:1px solid black;padding-left: 3px;border-right: 1px solid black" colspan="8">
            <small class="vsm-f">Nabywca (Pasażer)</small>
        </td>
    </tr>

    <tr>
        <td colspan="8" style="border-left:1px solid black;padding-left:3px"><b>' . $ticket['carrier']['description'] . '</b></td>
        <td colspan="2"></td>
        <td  style="border-left:1px solid black;padding-left:3px" colspan="6"><b style="font-size:14px">' . $ticket['name'] . '  ' . $ticket['surname'] . '</b></td>
        <td style="border-right: 1px solid black" colspan="2"></td>
    </tr>

    <tr>
        <td colspan="8" style="border-left:1px solid black;padding-left:3px"><small class="vsm-f">' . $ticket['carrier']['address'] . '</small></td>
        <td colspan="2"></td>
        <td style="border-left:1px solid black;padding-left:3px" colspan="6"><small>email : ' . $ticket['email'] . '</small></td>
        <td style="border-right: 1px solid black" colspan="2"></td>
    </tr>

    <tr>
        <td colspan="3" style="border-left:1px solid black;padding-left:3px"><small class="vsm-f">Tel: +48 146 371 777</small></td>
        <td colspan="3"></td>
        <td colspan="4"><b class="vsm-f">' .$ticket["MarketDescription"]. '</b></td>
        <td style="border-right:1px solid black;border-left:1px solid black;padding-left:3px" colspan="8"><small>Tel: ' . $ticket['phone'] . '</small></td>
    </tr>

    <tr>
        <td colspan="2" style="border-left:1px solid black; border-bottom:1px solid black; border-top:1px solid black;padding-left:3px">
            <small>Oferent:</small>
        </td>
        <td colspan="8" style="border-bottom:1px solid black; border-top:1px solid black;"><small style="font-size: 10px;"> ' . $ticket["Bidder"] . '</small></td>
        <td style="border:1px solid black;border-right: 0px;padding-left:3px" colspan="5">
            <b style="font-size: 11px;">Telefon do kierowcy:</b>
        </td>
        <td style="border-top:1px solid black;border-right: 1px solid black" align="right" colspan="3"><!--<small>+48146371777,+48146371777</small>--></td>
    </tr>

    <tr>
    <tr>
        <td class="gray no-borders-gray" style="border-top:1px solid black;border-right: 1px solid black;border-left:1px solid black" colspan="15" align="center">
            <span style="font-size:12px">Nazwa linii(kursu): </span>
            <b>'. $ticket["begin_station_locality"] .' - '. $ticket["end_station_locality"] .'</b>
        </td>
        <td style="border-right: 1px solid black" align="right" colspan="3" rowspan="1"><!-- <small>+48146371777,+48146371777</small>--></td>
    </tr>

    <tr style="padding:0px">
        <td class="gray no-borders-gray" style="border-left:1px solid black;padding-left:3px" colspan="2"><small class="vsm-f">Data</small></td>
        <td class="gray no-borders-gray" colspan="2" align="right" style="padding-right: 8px;"><small class="vsm-f">Godzina</small></td>
        <td class="gray no-borders-gray" colspan="4" style="padding:0"><small class="vsm-f">Miejscowość</small></td>
        <td class="gray no-borders-gray" colspan="2"></td>
        <td class="gray no-borders-gray" colspan="2" align="right"><small class="vsm-f">Długość trasy :</small></td>
        <td class="gray no-borders-gray" colspan="1" align="right"></td>
        <td style="border-right: 1px solid black" class="gray no-borders-gray" colspan="2"><b>' . $ticket['distance'] . ' km</b></td>
        <td colspan="3" style="border-right: 1px solid black"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px" class="gray no-borders-gray" colspan="2"><b>' . explode(' ', $ticket['departure'])[0] . '</b></td>
        <td class="gray no-borders-gray" colspan="2" align="right" style="padding-right: 8px"><b>' . date_create_from_format('Y.m.d H:i:s', $ticket['departure'])->format('H:i') . '</b></td>
        <td class="gray no-borders-gray" colspan="4"><b>' . $ticket['locality_from'] . '</b></td>
        <td class="gray no-borders-gray" colspan="2"></td>
        <td style="border-right: 1px solid black" class="gray no-borders-gray" colspan="5"></td>
        <td style="border:1px solid black;" colspan="3" rowspan="13"><img src="' . getQrCode($ticket) . '"></td>

    </tr>

    <tr>
        <td style="border-left:1px solid black;" class="gray no-borders-gray" colspan="2"></td>
        <td colspan="2" class="gray no-borders-gray"></td>
        <td class="gray no-borders-gray" colspan="6"><small>' . $ticket['station_from'] . '</small></td>
        <td class="gray no-borders-gray" colspan="5"></td>
        <td style="border-right: 1px solid black" colspan="1"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px" class="gray no-borders-gray" colspan="2"><b>' . explode(' ', $ticket['arrival'])[0] . '</b></td>
        <td class="gray no-borders-gray" colspan="2" align="right" style="padding-right: 8px"><b>' . date_create_from_format('Y.m.d H:i:s', $ticket['arrival'])->format('H:i') . '</b></td>
        <td class="gray no-borders-gray" colspan="7"><b>' . $ticket['locality_to'] . '	</b></td>
        <td class="gray no-borders-gray" colspan="2"></td>
        <td class="gray no-borders-gray" colspan="2"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black; border-bottom: 1px solid black" class="gray no-borders-gray" colspan="2"></td>
        <td style="border-bottom:1px solid black;" colspan="2" class="gray no-borders-gray"></td>
        <td style="border-bottom:1px solid bottom;" class="gray no-borders-gray" colspan="7"><small>' . $ticket['station_to'] . '</small></td>
        <td class="gray no-borders-gray" colspan="2"></td>
        <td style="border-bottom:1px solid bottom;" class="gray no-borders-gray" colspan="2"></td>
    </tr>


    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px; vertical-align: top;" colspan="1"><small class="vsm-f">Rodzaj biletu</small></td>
        <td colspan="6" style="vertical-align: top;"><b class="vsm-f">'.getTicketTypePl($ticket).'</b></td>
        <td colspan="2" style="vertical-align: top;"><small class="vsm-f">Ilość pasażerów</small></td>
        <td style="border:1px solid black" rowspan="2" width="5px" align="center"><b>' . $ticket['count']['count'] . '</b></td>
        <td style="border-left:1px solid black;padding-left:3px" colspan="5"><small class="vsm-f">Cena:</small></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;border-bottom: 1px solid black;padding-left:3px" ><b class="vsm-f"></b></td>
        <td style="border-bottom:1px solid black" colspan="5"></td>
        <td style="border-bottom:1px solid black" colspan="4"></td>
        <td style="border-left:1px solid black" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px" colspan="4"><small class="vsm-f">Uwagi</small></td>
        <td colspan="6"></td>
        <td style="border-left:1px solid black;padding-left:3px" colspan="1"><small class="vsm-f">Netto PL</small></td>
        <td colspan="3"></td>
        <td colspan="1" align="right" style="padding-right: 3px"><b class="vsm-f">' . $net_price . ' zł</b></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;border-bottom:1px solid black" colspan="10"></td>
        <td style="border-left:1px solid black;padding-left:3px" colspan="1"><small class="vsm-f">VAT 8% PL</small></td>
        <td colspan="3"></td>
        <td colspan="1" align="right" style="padding-right: 3px"><b class="vsm-f" style="font-weight: bold">' . $vat_price . ' zł</b></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;padding-left:3px" colspan="10"><small class="vsm-f">Administrator Platformy Komunikacji Samochodowej PKS.pl</small></td>
        <td colspan="3" style="border-left:1px solid black;padding-left: 3px"><small class="vsm-f">'.$brutto["text"].'</small></td>
        <td colspan="2" align="right" style="padding-right: 3px;"><b class="vsm-f">' . $brutto["price"] . '</b></td>
    </tr>

    <tr>
        <td colspan="3" style="border-left:1px solid black;padding-left:3px"><small class="vsm-f">NIP: 6750001219</small></td>
        <td style="padding: 5px" colspan="7"></td>
        <td style="border-left:1px solid black;" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black"></td>
        <td colspan="9"></td>
        <td style="border-left:1px solid black" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black"></td>
        <td style="border-left:1px solid black;" colspan="9"></td>
        <td style="border-left:1px solid black" colspan="1"></td>
        <td colspan="3"></td>
        <td colspan="1"></td>
    </tr>

    <tr >
        <td style="border:1px solid black" rowspan="1" colspan="1" width="90px" align="center"><img src="./img/mda_logo.png" height="30px"></td>
        <td style="border-left:1px solid black; border-bottom: 1px solid black;padding-left:3px" colspan="9"><b class="vsm-f">Małopolskie Dworce Autobusowe S.A.</b><br>
            <small class="vsm-f">31-514 Kraków, al. Beliny-Prażmowskiego 6A/6</small>
        </td>
        <td style="border-left:1px solid black;border-bottom: 1px solid black; vertical-align: bottom;padding-left: 3px" colspan="2"><b style="font-size: 14px">Razem</b></td>
        <td style="border-bottom: 1px solid black" colspan="2"></td>
        <td style="border-bottom: 1px solid black; vertical-align: bottom; padding-right: 3px" colspan="1" align="right"><b style="font-size:14px">' . $total_price . ' zł</b></td>
    </tr>
</table>
<div class="indent"></div>
        ';
        }
    }

    return $html;
}

function getQrCode($ticket)
{
    include_once('libs/phpqrcode/qrlib.php');

    // how to save PNG codes to server

    $tempDir = 'qrcodes/';

    $codeContents =
        'fullPrice - ' . $ticket['fullPrice'] .
        'currentPrice= ' . $ticket['current_price'] .
        'course_id=' . $ticket['course_id'] .
        'station_from=' . $ticket['station_from'] .
        'station_to=' . $ticket['station_to'];

    // we need to generate filename somehow,
    // with md5 or with database ID used to obtains $codeContents...
    $fileName = $ticket['course_id'] . '.png';

    $pngAbsoluteFilePath = $tempDir . $fileName;

    QRcode::png($codeContents, $pngAbsoluteFilePath);

    return $pngAbsoluteFilePath;
}
