<?php

header('Access-Control-Allow-Origin: www.pks.pl');
set_include_path("/home/pkskrakow/ftp/www/pks_pl");
define("PATH", get_include_path());
ini_set('display_errors', 1);


include_once ('helpers/session/session.php');
include_once('admin/config.php');
include_once('admin/startup.php');
include_once('admin/Model/Model.php');

startup();

define("SERVER_URL", "");

if (isset($_COOKIE['logged'])) {
    $connection = mysql_connect("localhost", "pkskrakow_4", "Dond33st4S!");
    $db = mysql_select_db("pkskrakow_4");
    mysql_query("SET NAMES 'utf8'");

    if (!$connection || !$db) {
        exit(mysql_error());
    }

    $id = $_COOKIE['logged'];
    $query = mysql_query("SELECT * FROM `userdata` WHERE `idUser` LIKE '$id'");
    $resulted = mysql_fetch_assoc($query);
    $_COOKIE["customer"] = $resulted;
    
    $_SESSION['user_id'] = $id;
}

if ($_SERVER['SCRIPT_URL'] == SERVER_URL.'/user-cabinet.php' && $id == '' || $_SERVER['SCRIPT_URL'] == SERVER_URL.'/user-cabinet' && $id == '') { ?>
    <script>
        alert('Zaloguj się ponownie na stronie sprzedaży biletów MDA.');
        location.href = "/";
    </script>
<?php } ?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />    <title></title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" href="/img/checkbox.png" type="image/x-icon">
    <link rel="stylesheet" href="/libs/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/libs/animate/animate.css">
    <link rel="stylesheet" href="/libs/slick/slick.css">
    <link rel="stylesheet" href="/libs/slick/slick-theme.css">
    <link rel="stylesheet" href="/libs/slicknav/slicknav.min.css">
    <link rel="stylesheet" href="/libs/ui/jquery-ui.css">
    <link rel="stylesheet" href="/libs/ui/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="/libs/ui/jquery.ui.timepicker.css">
    <link rel="stylesheet" href="/libs/bootstrap-fileinput/css/fileinput.min.css">
    <link rel="stylesheet" href="/css/fonts.css">
    <link rel="stylesheet" href="/css/style.css?v6">
    <link rel="stylesheet" href="/css/media.css">
    <?php
    if ($_SERVER['REQUEST_URI'] == '/user-registration.php' || $_SERVER['REQUEST_URI'] == '/user-registration') {

    } else {
        echo '<link rel="stylesheet" href="/css/build.css">';
    }
    ?>
    <script src='https://www.google.com/recaptcha/api.js?hl=pl'></script>
    <script src="<?= SERVER_URL ?>/libs/modernizr/modernizr.js"></script>
    <script src="<?= SERVER_URL ?>/libs/jquery/jquery-1.9.1.min.js"></script>
    <script src="<?= SERVER_URL ?>/libs/jquery-mask/jquery.mask.js"></script>
    <script src="<?= SERVER_URL ?>/js/globals.js"></script>
    <link rel="stylesheet" href="/libs/sweetalert/sweetalert.css">
    <script src="<?= SERVER_URL ?>/libs/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" href="css/swalCustom.css">
</head>
<body class="body<?= $_SERVER['REQUEST_URI'] == '/page-order.php' ? ' page-order' : '' ?><?= $_SERVER['REQUEST_URI'] == '/page-order' ? ' page-order' : '' ?> <?= $_SERVER['REQUEST_URI'] == '/' ? ' main_page' : '' ?>">
<!--.top-line-->
<div class="top-header default">
<!--        <h1 class="marquee"><span>Pamiętaj! przerwa technologiczna w systemie w czasie 23:30 - 1:00 zakup biletów w tym czasie jest niemożliwy.</span></h1>-->
    <div class="wrap-btn-logo">
        <a href="<?= SERVER_URL ?>/" class="btn-logo">
            <img class="hidden-sm hidden-xs" src="<?= SERVER_URL ?>/img/logo_1.png">
            <img class="hidden-md hidden-lg" src="<?= SERVER_URL ?>/img/logo_04.png">
        </a>
    </div>
    <div class="container">


        <div class="block-lang">
            <div class="form-group">
                <select class="form-control">
                    <option value="1">Polski</option>
<!--                    <option value="2">Russian</option>-->
<!--                    <option value="3">German</option>-->
<!--                    <option value="4">English</option>-->
                </select>
            </div>
        </div>
        <div class="wrap-btn dropdown" style="display:none">
            <a href="#" data-toggle="dropdown" class="btn btn-log-out dropdown-toggle"> <span class="text">Logowanie</span></a>
            <ul id="login-dp" class="dropdown-menu">
                <li>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block-title">Zaloguj si&eogon;</div>
                            <form onsubmit="return false;">
                                <div class="form-group">
                                    <label for="field1" class="sr-only">Login | e-mail</label>
                                    <input id="field1" type="text" placeholder="Login | e-mail" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="field2" class="sr-only">Hasło</label>
                                    <input id="field2" type="password" placeholder="Hasło" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button id="login" class="btn btn-primary btn-block btn-log">Zaloguj się</button>
                                </div>
                                <!--<div class="checkbox">
                                    <input id="forget-pass" type="checkbox">
                                    <label for="forget-pass"><span> </span>Nie pamiętam hasła?</label>
                                </div>-->
                                <div class="forget-pass">
                                    <a href="#" class="btn-forget-pass">Nie pamiętasz hasła?</a>
                                </div>
                            </form>
                        </div>
                        <div class="bottom"><a href="<?= SERVER_URL ?>/user-registration" class="btn-registar">Rejestracja</a></div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="block-user-basket" style="display:none">
            <div id="dropdown" class="user-basket dropdown"><a href="#" data-toggle="dropdown"
                                                               class="btn btn-bascket-user dropdown-toggle"><span
                        class="count-bilets">0</span><span class="text">Biletów</span></a>
                <ul id="user-basket" class="dropdown-menu animated fadeInDown">
                    <li>
                        <div class="row">
                            <form onsubmit="return false">
                                <div class="col-md-12">
                                    <div id="renderBasket"></div>
                                    <div class="row-sum">
                                        <div class="bl-sum">
                                            <div class="title">Razem:</div>
                                            <div class="sum-money">138,00 zł</div>
                                            <a href="#" onclick="return false" class="btn btn-buy">Kup bilet</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <?php if (isset($_COOKIE['logged'])) { ?>
            <div class="block-user dropdown">
                <a href="#" data-toggle="dropdown" class="btn-user-cabinet dropdown-toggle">
                    <div class="box-img">
                        <img  src="<?= $resulted['url'] ?>" alt="">
                    </div>
                    <div class="name"><?= $resulted['name'] ?></div>
                </a>
                <ul id="user-cabinet" class="dropdown-menu">
                    <li>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="item"><a href="<?= SERVER_URL ?>/">Do wyszukiwarki</a></div>
                                <div class="item"><a href="<?= SERVER_URL ?>/user-cabinet">Moje konto</a></div>
                                <div class="item"><a id="delete_cookie" href="<?= SERVER_URL ?>/"> Wyloguj się</a></div>
                                <script>
                                    $("#delete_cookie").click(function () {
                                        $.removeCookie('logged');
                                    });
                                </script>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        <?php } ?>
    </div>
</div>