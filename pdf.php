<script src="libs/jquery/jquery-1.9.1.min.js"></script>
<div class="ticket-new">
    <div class="container">
        <a onclick="return false" href="#" id="download">Download pdf</a>
        <div id="renderTickets"></div>
    </div>
</div>


<div style="display: none" id="canvas"></div>
<script>
    console.log('test1');
    $(document).ready(function () {
        console.log('test');
        var tickets = localStorage.getItem('tickets').split(';');

        /**
         *  Main function for rendering tickets on this page
         **/
        var content = '';
        render();
        function render() {
            for (var i = 0; i < JSON.parse(tickets[0]).length; i++) {
                if (JSON.parse(tickets[0])[i].count.check == false) {
                    for (var n = 0; n < JSON.parse(tickets[0])[i].count.count; n++) {
                        renderCount(JSON.parse(tickets[0])[i], i);
                    }
                } else {
                    renderCount(JSON.parse(tickets[0])[i], i);
                }
            }
            content += "";
            $('#renderTickets').append(content);
            $.ajax({
                url: "/mailer.php",
                type: "POST",
                data: {
                    pdf: 'pdf',
                    content: content
                }
            });
            $('#renderTickets > .ticket').each(function () {
                html2canvas($(this)[0], {
                    background: "#ffffff",
                    onrendered: function (canvas) {
                        $('#canvas').append(canvas);
                    }
                });
            });
        }

        // Secondary function for rendering the count of tickets
        function renderCount(data, i) {
            content += '<div class="ticket"> <div class="header"> <span>Nr biletu<br>Ticket no</span>' +
                '<span class="ticket_number">EL ' + data.reserve_num + '</span> </div> <div class="content"> <div class="col first_col"> <div class="ticket_row"> <div class="left_part"><strong>Przewoźnik</strong><br>Operated by</div> <div class="right_part">' +
                '<div class="title">' + data.carrier + '</div> <div class="adress"> 78-300 Śniatyń, ul. Trudowa 1/G<br> Tel. 801 22 33 44, +48 77 443 44 44<br> NIP: 754-296-42-70 </div> </div> </div> <div class="ticket_row route_row"> <div class="left_part"><strong>Podróż</strong><br>Route</div> <div class="right_part"> <div class="col date_col"> <div class="head"><strong>Data</strong> | Date</div>' +
                '<div class="mark_text">' + data.departure.split(' ')[0] + '</div> <br>' +
                '<div class="mark_text">' + data.arrival.split(' ')[0] + '</div> <br> </div> <div class="col hour_col"> <div class="head"><strong>Godzina</strong> | Hour</div>' +
                '<div class="mark_text">' + data.departure.split(' ')[1].substr(0, data.departure.split(' ')[1].length - 3) + '</div> <br>' +
                '<div class="mark_text">' + data.arrival.split(' ')[1].substr(0, data.arrival.split(' ')[1].length - 3) + '</div> <br> </div> <div class="col city_col"> <div class="head"><strong>Miasto</strong> | City</div>' +
                '<div class="mark_text">' + data.locality_from + '</div>' +
                '<div>' + data.station_from + '</div>' +
                '<div class="mark_text">' + data.locality_to + '</div>' +
                '<div>' + data.station_to + '</div> </div> </div> </div> <div class="ticket_row"> Małopolskie Dworce Autobusowe S.A.<br> ul. Bosacka 18, 31-505 Kraków, tel. 12 351 82 08 </div> </div> <div class="col second_col"> <div class="ticket_row"> <div class="head"><strong>Nabywca</strong> | Buyer</div>' +
                '<div class="name">' + data.name + '</div>' +
                '<div class="tel">e-mail: ' + data.email + '</div>' +
                '</div> <div class="ticket_row">' +
                '<div class="bold">' + getDiscount(data.ulga[i]).pl + '</div>' +
                '<div>' + getDiscount(data.ulga[i]).eng + '</div> </div> <div class="ticket_row"> <div class="stroke"> <div class="left">' +
                '<span class="bold">Ulga</span>' +
                '<span> | Discount</span> </div>' +
                '<div class="right">' + getDiscount(data.ulga[i]).percent + '</div> </div> <div class="stroke"> <div class="left"> <span class="bold">Netto PL</span>' +
                '<span> | Net price:</span> </div>' +
                '<div class="right">' + getDiscount('', false, data.fullPrice, data.price_vat) + ' zł</div> </div> <div class="stroke"> <div class="left">' +
                '<span class="bold">VAT PL:</span> </div>' +
                '<div class="right">' + data.price_vat + ' zł</div> </div> <div class="stroke"> <div class="left"> <span class="bold">Brutto</span>' +
                '<span> | Gross price</span> </div>' +
                '<div class="right">' + data.fullPrice + ' zł</div> </div> <div class="stroke"> <div class="left"> <span class="bold">Rabat</span>' +
                '<span> | Discount </span> </div>' +
                '<div class="right">' + getDiscount(data.ulga[i], true, data.fullPrice) + ' zł</div> </div> <div class="stroke"> <div class="left"> <span class="bold">RAZEM</span>' +
                '<span> | Total</span> </div>' +
                '<div class="right mark_text">' + data.price[i] + ' zł</div> </div> </div> </div> <div class="col third_col"> <div class="ticket_row"> <div class="head"> <strong>Data sprzedaży</strong><br>' +
                getDate() + '</div> ' +
                '<div class="date bold"> </div> </div> <div class="ticket_row"> <div class="head"><strong>Odległość</strong> | Distance</div> <div class="distance">' + +data.distance + ' km</div> </div> <div class="ticket_row"> <div class="bold">Sposób zakupu</div> <p>Purchase method</p> <p> <strong>I - Internet</strong> </p> <p class="tab_text">Internet</p> <p> <strong>O - Kasa</strong> </p> <p class="tab_text">Ticket office</p> <p> <strong>A - Automat biletowy</strong> </p> <p class="tab_text">Ticket machine</p> </div> <div class="ticket_row">I</div> </div> </div>' +
                '</div>';
        }


        function getDiscount(check, sum, price, price_vat) {
            var ulga;
            if (sum == true) {
                price = parseFloat(price);
                console.log(price);
                ulga = parseFloat(check.substring(check.length - 3));
                price = price * ulga / 100;
                console.log(ulga);
                if (!isNaN(price)) {
                    return price.toFixed(2);
                } else {
                    return "0.00"
                }
            } else {
                if (price_vat != undefined) {
                    return parseFloat(price - price_vat).toFixed(2);
                } else {
                    ulga = {};
                    if (check == 'Bez ulgi') {
                        ulga.pl = "BILET NORMALNY";
                        ulga.eng = "Normal ticket";
                        ulga.percent = '0%'
                    } else {
                        ulga.pl = "BILET ULGOWY";
                        ulga.eng = "Reduced-fare ticket";
                        ulga.percent = check.substring(check.length - 3);
                    }
                    return ulga;
                }
            }
        }

        function getDate() {
            var date = new Date();
            var day = date.getDay();
            if (day < 10) {
                day = '0' + day
            }
            var result = date.getFullYear() + '.' + date.getMonth() + 1 + '.' + day;
            return result;
        }

        $('#download').click(function () {
            var arr = [];
            $('#canvas > canvas').each(function () {
                arr.push($(this)[0]);
            });
            exportPDF(arr);
        });

        function exportPDF(arr) {
            var doc = new jsPDF(),
                docInternals = doc.internal,
                docPageSize = docInternals.pageSize,
                docScaleFactor = docInternals.scaleFactor,
                docPageWidth = docPageSize.width,
                docPageHeight = docPageSize.height,
                docPageWidthPx = docPageWidth * docScaleFactor,
                docPageHeightPx = docPageHeight * docScaleFactor;

            console.log("width " + docPageWidthPx + " height " + docPageHeightPx);
            var count = 1;
            for (var i = 0; i < arr.length; i++) {
                var canvas = arr[i];
                var imgData = canvas.toDataURL("image/jpeg");
                var imgWidth = (canvas.width * 25.4) / 130;
                var imgHeight = (canvas.height * 25.4) / 130;

                if (count % 4 == 0) {
                    doc.addPage();
                    doc.addImage(imgData, 'JPEG', 0, 0, docPageWidth, imgHeight);
                    count = 1;
                } else {
                    if (count == 1) {
                        doc.addImage(imgData, 'JPEG', 0, 0, docPageWidth, imgHeight);
                    } else {
                        count--;
                        doc.addImage(imgData, 'JPEG', 0, count * 100, docPageWidth, imgHeight);
                        count++;
                    }
                }
                count++
            }
            doc.output('dataurlnewwindow');
        }

        localStorage.setItem('check', 'test')
    })
</script>