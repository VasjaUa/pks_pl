<?php

class Request {

    public $_method;
    public $_data;
    public $_server_output;
    public $_server = "http://82.160.155.29:10059/";


    function __construct($method)
    {
        $this->_method = $method;
    }


    public function setData(array $data)
    {
        if (isset($data['method'])) unset($data['method']);

        $this->_data = $data;
        return $this;
    }


    public function addData($key, $value)
    {
        $this->_data[$key] = $value;
        return $this;
    }


    public function callApi()
    {
        $url = $this->_server . $this->_method ."?". http_build_query($this->_data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $this->_server_output = curl_exec($ch);
        curl_close($ch);

        return $this;
    }

    public function extractOutput()
    {
        return json_decode($this->_server_output, true);
    }
}