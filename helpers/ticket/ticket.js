$(document).ready(function(){
  var invoices = JSON.parse(localStorage.getItem('invoices'));
$('.call-to-factura').on('click', function(event){
		event.preventDefault();
        var ths = $(this);
    	$.ajax({
                url: "",
                type: "POST",
                dataType: "json",
                data: {
                    method: "CreateSalesInvoice",
                    tickets_list: invoices[0].tickets_list,
                    description: invoices[0].description,
                    nip: invoices[0].nip,
                    postal_code: invoices[0].postal_code,
                    country: invoices[0].country,
                    city: invoices[0].city,
                    street: invoices[0].street
                },
            beforeSend: function(){
                ths.addClass('m-progress');
            },
                success: function (r1) {
                    ths.removeClass('m-progress');
                    $.ajax({
                        url: "generate-save-pdf.php",
                        type: "POST",
                        data: {
                            type: "s",
                            id: ths.attr('data-session_id'),
                            invoice: r1.PrintFile,
                            invoice_firm: r1.Firm,
                            invoice_number: r1.InvoiceNumber,
                            uuid: r1.UUID
                        },
                        success: function (link){
                        	ths.attr('href', link).removeClass('call-to-factura');
                             window.location.href = link;
                        }
                    });
                }
            });  
    });  
})