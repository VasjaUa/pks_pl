var $_invoice_form_data = false;
var $nameCompany = $('#invoice_block > div.row-info.row_faktura1 > div.caption.edit_faktura > input');
var $nip = $('#invoice_block > div.row-info.row_faktura2 > div > input');
var $region = $('#invoice_block > div.row-info.row_faktura3 > div:nth-child(1) > input');
var $postalCode = $('#invoice_block > div.row-info.row_faktura3 > div:nth-child(2) > input');
var $city = $('#invoice_block > div.row-info.row_faktura3 > div:nth-child(3) > input');
var $address = $('#invoice_block > div.row-info.row_faktura4 > div > input');
var $buttonBlockNip = $('#invoice_block > div.row-info.row_faktura1 > div.checkbox ');
var $buttonDownloadNip = $('#invoice_block > div.row-info.row_faktura2 > div > div > #downloadNip');

var $arrayFields = [$nameCompany,/*$nip,*/,$region,$city,$address];

$buttonDownloadNip.click(function () {
    if ($('#firmOrPerson').prop("checked"))
        return false;
    
        $(this).removeClass('error_invoice');
    console.log('stert');
        var thisNip = $('input[name="nip"]').val();
        if (thisNip != '') {
             $.ajax({
                url: "",
                type: "POST",
                dataType: "json",
                data: {
                    method: "TicketsCancelation",
                    ticket_number :'NET-0450-18-00000062'
                },
                beforeSend: function(){
                   $('#invoice_block').addClass('start_load');
                },
                success: function (response) {
                   $('#invoice_block').removeClass('start_load');
                    console.log(response);
                    if (response.NIP == thisNip) {
                        $('input[name="f_country"]').val(response.Country); 
                        $('input[name="f_street"]').val(response.Address); 
                        $('input[name="f_post_code"]').val(response.PostCode); 
                        $('input[name="f_city"]').val(response.City); 
                        if(!response.IsForCompany){
                            $('input[name="firm"]').val(response.FirstName + ' ' + response.LastName); 
                        }else{
                         $('input[name="firm"]').val(response.CompanyName);    
                        }
                    }else{
                        $(this).addClass('error_invoice');
                    }

                }
            });           
        }

});

$(document).ready(function(){
    var element = $('#invoice_block > div.row-info.row_faktura3 > div:nth-child(1) > input');
    element.autocomplete({
                scroll: true,
                minLength: 1,
                autoFocus: true,
                source: function (request, response) {

                    $.ajax({
                        url: "",
                        type: "POST",
                        dataType: "json",
                        data: {
                            method: "GetCountries",
                            text: element.val()
                        },
                        success: function (res) {
                             response($.map(res, function (i) {
                        return {
                                    label: i.Description
                                }

                             }));

                        }
                    });
                },
                select: function (i) {
// што тут надо?!
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>").data("item.autocomplete", item).append("<a class='city_clk'>" + item.label + "</a>").appendTo(ul);
            };
});



$buttonBlockNip.click(function () {
    if ($('#firmOrPerson').prop("checked")){
        $('#invoice_block > div.row-info.row_faktura2 > div.caption.edit_faktura').css('opacity','0.4');
        $('#invoice_block > div.row-info.row_faktura2 > div.caption.edit_faktura > input').prop('disabled','false');
        $('#invoice_block > div.row-info.row_faktura2 > div.caption.edit_faktura > input').val("");
        $('#invoice_block > div.row-info.row_faktura2 > div.caption.edit_faktura > input').removeClass('error_invoice');
        console.log('53(1)' + $('#downloadNip').prop( "checked" ));
        //if ($('#downloadNip').prop( "checked" )) {
            $('#downloadNip').prop('checked', 'false');
        //}
    }
    else {
        console.log('59' + $('#downloadNip').prop( "checked" ));
        $('#invoice_block > div.row-info.row_faktura2 > div.caption.edit_faktura').css('opacity','1');
        $('#invoice_block > div.row-info.row_faktura2 > div.caption.edit_faktura > input').removeAttr('disabled');
    }
});

$nip.keypress(function () {
    $nip.mask('0000000000', {clearIfNotMatch: false});
});
$nip.focusout(function () {
    checkWord($nip,9);
});
$postalCode.keypress(function () {
    $postalCode.mask('00-000', {clearIfNotMatch: false});
});
$postalCode.focusout(function () {
    checkWord($postalCode,6);
});

$nameCompany.focusout(function () {
    checkWord($nameCompany,3);
});
$address.focusout(function () {
    checkWord($address,3);
});
$region.focusout(function () {
    checkWord($region,3);
});
$city.focusout(function () {
    checkWord($city,3);
});

function checkWord($field,num){
    if ($field.val().length < num) {
        $field.addClass('error_invoice');
        return false;
    } else {
        $field.removeClass('error_invoice');
        return true;
    }
}

function checkFormFactura(){
    console.log('is = ' + $('#btn_group_registration').is(':checked'));
    console.log("prop = " + $('#btn_group_registration').prop("checked"));
    var $parent = $('#invoice_block');
    var error = false;

    $arrayFields.forEach(function ($field) {
        if ( !checkWord($field,3) ){
            error = true;
        }
    });

    if(!checkWord($postalCode,6))
        error = true;

    if ( !$('#firmOrPerson').prop("checked") )
        if ( !checkWord($nip, 9) )
            error = true;

    if (error == false) {
        $_invoice_form_data = {
            firm: $('[name="firm"]').val().replace("\'", "\\'"),
            nip: $('[name="nip"]').val().replace("\'", "\\'"),
            f_post_code: $('[name="f_post_code"]').val().replace("\'", "\\'"),
            f_country: $('[name="f_country"]').val().replace("\'", "\\'"),
            f_city: $('[name="f_city"]').val().replace("\'", "\\'"),
            f_street: $('[name="f_street"]').val().replace("\'", "\\'"),
            is_company: $('#firmOrPerson').prop("checked")
        };

        //localStorage

        return true
    }
    else
        return false
}