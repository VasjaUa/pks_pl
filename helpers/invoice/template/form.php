<?php $c = $_COOKIE["customer"]; ?>
<div id="invoice_block" class="part right_part">
    <div class="row-info row_faktura1 clearfix">
        <div class="checkbox">
            <input name="firmOrPerson" type="checkbox" id="firmOrPerson" title="Pole wymagane" value="1" <?php echo( $c['is_company'] == 1 ? 'checked="checked"' : '');?>>
            <label for="firmOrPerson" class="checked"><span></span><strong>Osoba prywatna</strong></label>
        </div>
        <p>Nazwa firmy lub os. fiz</p>
        <div data="firm" class="caption edit_faktura">

            <input value="<?= $c['firm'] ?>"
                   type="text"
                   name="firm"
                   class="caption form-control">

        </div>
    </div>
    <div class="row-info row_faktura2 clearfix">
        <p>NIP</p>
        <div data="nip" class="caption edit_faktura" <?php echo( $c['is_company'] == 1 ? 'style="opacity: 0.4;"' : '');?> ">

            <input value="<?= $c['nip'] ?>"
                   name="nip"
                   type="number"
                   class="caption form-control"
                   maxlength="10" <?php echo( $c['is_company'] == 1 ? 'disabled' : '');?>>
            <div></div>
            <div class="checkbox-buy">
            <button class="checkbox_faktury btn" id="downloadNip">Pobierz z GUS</button>

                <span>(Dotyczy firm polskich)</span>
            </div>
        </div>
    </div>
    <div class="row-info row_faktura3 clearfix">
        <div data="f_country" class="caption edit_faktura">
            <p>Kraj</p>

            <input value="<?= $c['f_country'] ?>"
                   name="f_country"
                   type="text"
                   class="caption form-control">
        </div>
        <div data="f_post_code" class="caption edit_faktura"><p>Kod pocztowy</p>
            <input value="<?= $c['f_post_code'] ?>"
                   name="f_post_code"
                   class="caption form-control"
                   maxlength="6">
        </div>
        <div data="f_city" class="caption edit_faktura"><p>Miejscowość</p>
            <input value="<?= $c['f_city'] ?>"
                   name="f_city"
                   type="text"
                   class="caption form-control">
        </div>
    </div>
    <div class="row-info row_faktura4 clearfix">
        <div data="f_street" class="caption edit_faktura"><p>Adres</p>
            <input value="<?= $c['f_street'] ?>"
                   name="f_street"
                   type="text"
                   class="caption form-control">
        </div>
    </div>
</div>