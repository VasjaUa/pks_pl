<?php

include_once(PATH . '/helpers/api/Request.php');
include_once('Handler.php');
include_once('SessionHelper.php');

$handler = new Handler();
$helper = new SessionHelper();

$handler->gc(900);
session_set_save_handler($handler, true);
session_set_cookie_params(900);
session_start();
$sid = session_id();


if (isset($_POST['DestroyBasket']))
{
    $helper::unsetBasket($handler, $sid);
    exit();
}
elseif (isset($_POST['CreateBasket']))
{
    $api = new Request("CreateBasket");
    $api->setData(['SessionID' => $sid]);
    $api->callApi();

    $_SESSION["bid"] = $api->extractOutput()['BasketID'];
    setcookie(
        "basketExist",
        true,
        time() + 900
    );

    exit(!!$_SESSION["bid"]);
}
elseif (isset($_POST['method']))
{
    $api = new Request($_POST['method']);
    $data = $_POST;
    $data['SessionID'] = $sid;
    $data['BasketID'] = $_SESSION['bid'];

    $api->setData($data)->callApi();
    $result = $api->extractOutput();

    exit(json_encode($result));
}
else
{
    if (isset($_COOKIE['basketExist']) && isset($_SESSION['bid'])) {
        $api = new Request('CheckBasket');
        $api->setData([
            'BasketID' => $_SESSION['bid'],
            'SessionID' => $sid
        ])->callApi();

        $result = $api->extractOutput();

        $_SESSION['isActive'] = $result['Active'];

        if (!$_SESSION['isActive'] && $helper->checkRoute()) {
            $helper::unsetBasket($handler, $sid);
            echo $helper->initJS();
        }

    }
    elseif ($helper->checkRoute()) {
        echo $helper->initJS();
    }
}
