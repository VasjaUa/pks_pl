<?php

class SessionHelper
{
    protected $_route;

    protected $_isRedirectPage = false;

    protected $_redirectPagesList = array(
        0 => "/page-order.php",
        1 => "/ticket.php"
    );

    function __construct()
    {
        $this->_route = $_SERVER["PHP_SELF"];
        $this->checkRoute();
    }

    static public function unsetBasket($handler, $sid)
    {
        unset($_COOKIE['basketExist']);
        setcookie('basketExist', null, -1);
        $handler->destroy($sid);
        session_destroy();
    }

    public function initJS()
    {
        $js = ($this->_isRedirectPage) ? "location.href = '/'" : "";
        return "<script>
                localStorage.setItem('data', '');
                localStorage.setItem('reserve', '');
                localStorage.setItem('check', null);
                alert('Okres ważności rezerwacji upłynął!!!');
                $js
            </script>";
    }

    public function checkRoute()
    {
        foreach ($this->_redirectPagesList as $page) {
            if ($this->_route == $page):
                $this->_isRedirectPage = true;
            endif;
        }

        return $this->_isRedirectPage;
    }
}