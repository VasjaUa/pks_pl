<?php

class Helper
{
    protected $_route;

    protected $_isRedirectPage = false;

    protected $_redirectPagesList = array(
        0 => "/page-order.php",
        1 => "/ticket.php"
    );

    function __construct()
    {
        $this->_route = $_SERVER["PHP_SELF"];
        $this->checkRoute();
    }

    static public function apiRequest(array $data)
    {
        $server = "http://82.160.155.29:10059/";
        $method = $data['method'];
        unset($data['method']);

        $url = $server . $method ."?". http_build_query($data);
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        curl_close($ch);

        return $server_output;
    }

    static public function unsetBasket($handler, $sid)
    {
        unset($_COOKIE['bid']);
        setcookie('bid', null, -1);
        $handler->destroy($sid);
        session_destroy();
    }

    public function initJS()
    {
        $js = ($this->_isRedirectPage) ? "location.href = '/'" : "";
        return "<script>
                localStorage.setItem('data', '');
                localStorage.setItem('reserve', '');
                localStorage.setItem('check', null);
                alert('Okres ważności rezerwacji upłynął!!!');
                $js
            </script>";
    }

    public function checkRoute()
    {
        foreach ($this->_redirectPagesList as $page) {
            if ($this->_route == $page):
                $this->_isRedirectPage = true;
            endif;
        }

        return $this->_isRedirectPage;
    }
}