<?php include_once('header.php');
$per_page = 2; //6

$pagesPaid = (isset($_GET['sale']) ? ($_GET['sale'] - 1) : 0);
$pagesAnnulled = (isset($_GET['annulled']) ? ($_GET['annulled'] - 1) : 0);
$pagesInvoices = (isset($_GET['invoices']) ? ($_GET['invoices'] - 1) : 0);

$startPaid = abs($pagesPaid * $per_page);
$startAnnulled = abs($pagesAnnulled * $per_page);
$startInvoices = abs($pagesInvoices * $per_page);


$sqlPaidTickets = "SELECT * FROM `tickets` WHERE `id` = '$id' ORDER BY `number_ticket` DESC LIMIT $startPaid,$per_page";
$paidTickets = mysql_query($sqlPaidTickets) or die(mysql_error());


$sqlCountPaidTickets = "SELECT COUNT(*)  FROM `tickets` WHERE `id` = '$id'";
$countPaidTickets = mysql_fetch_row(mysql_query($sqlCountPaidTickets))[0] or die(mysql_error());
$numPagesPaidTickets = ceil($countPaidTickets / $per_page);


$sqlAnnulledTickets = "SELECT * FROM `cancelledTickets` WHERE `id_user` = '$id' ORDER BY `number_ticket` DESC LIMIT $startAnnulled,$per_page";
$AnnulledTickets = mysql_query($sqlAnnulledTickets) or die(mysql_error());

$sqlCountAnnulledTickets = "SELECT COUNT(*)  FROM `cancelledTickets` WHERE `id_user` = '$id'";
$countAnnulledTickets = mysql_fetch_row(mysql_query($sqlCountAnnulledTickets))[0] or die(mysql_error());
$numPagesAnnulledTickets = ceil($countAnnulledTickets / $per_page);


$sqlInvoices = "SELECT i.* FROM `invoices` AS i JOIN transactions AS tr ON (tr.id = i.transaction_id) WHERE  tr.user_id = '$id' ORDER BY i.id DESC LIMIT $startInvoices,$per_page";
$Invoices = mysql_query($sqlInvoices) or die(mysql_error());

$sqlCountInvoices = "SELECT COUNT(*)  FROM `invoices` AS i JOIN transactions AS tr ON (tr.id = i.transaction_id) WHERE  tr.user_id = '$id'";
$countInvoices = mysql_fetch_row(mysql_query($sqlCountInvoices))[0] or die(mysql_error());
$numPagesInvoices = ceil($countInvoices / $per_page);

?>
<div class="block-header">
    <div class="container">
        <div class="block-title">Moje konto</div>
    </div>
</div>
<div class="block-user-tabs">
    <div class="container">
        <div class="block-tabs">
            <ul menuCabinet class="nav nav-tabs">
                <li page="home" class="<?= ($_GET['page'] == 'home' || !isset($_GET['page'])) ? 'active' : '' ?>">
                    <a href="#tab-5" data-toggle="tab">Moje konto</a>
                </li>

                <li page="sale" class="<?= ($_GET['page'] == 'sale') ? 'active' : '' ?>">
                    <a href="#tab-2" data-toggle="tab">Opłacone</a>
                </li>

                <li page="annulled" class="<?= ($_GET['page'] == 'annulled') ? 'active' : '' ?>">
                    <a href="#tab-3" data-toggle="tab">Bilety anulowane</a>
                </li>

                <li page="invoices" class="<?= ($_GET['page'] == 'invoices') ? 'active' : '' ?>">
                    <a href="#tab-4" data-toggle="tab">Dane do faktury</a>
                </li>
            </ul>
            <script>
                $('[menuCabinet] a').click(function () {
                    var $href = $(this).parents('li').attr('page');
                    window.history.replaceState(null, null, location.pathname + '?page=' + $href);
                })
            </script>
            <div class="tab-content">
                <div id="tab-5"
                     class="tab-pane fade<?= ($_GET['page'] == 'home' || !isset($_GET['page'])) ? 'in active' : '' ?>">
                    <div class="block-user-cabinet">
                        <div class="row">
                            <div class="col col-xs-4 col-sm-4 col-md-4 col-lg-4 block-0">
                                <div class="block-personal">
                                    <div class="box-img">
                                        <img style="height: 150px; width: 150px; border-radius: 50%"
                                             src="<?= (!$resulted['url']) ? "/img/userImg.png" : $resulted['url'] ?>"
                                             alt="">
                                    </div>
                                    <div class="wrap-btn">
                                        <form enctype="multipart/form-data" action="" method="post">
                                            <input name="file" id="file-3" type="file">
                                        </form>
                                        <a href="#" style="display: none" onclick="return false" class="btn-photo">Akceptuję</a>
                                    </div>
                                    <div class="bl-info">
                                        <div class="row-user">
                                            <span class="title">Login:</span>
                                            <span data="login" class="login"><?= $resulted['login'] ?></span>
                                        </div>
                                        <div class="row-user">
                                            <span class="title">e-mail:</span>
                                            <span data="email" class="e-mail edit"><?= $resulted['email'] ?></span>
                                        </div>
                                        <div class="row-user">
                                            <span class="title">Nr telefonu:</span>
                                            <span data="phone" class="phone edit"><?= $resulted['phone'] ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-4 col-sm-4 col-md-4 col-lg-4 block-1">
                                <div class="block-user-info">
                                    <div class="row-info">
                                        <div class="title">Imię:</div>
                                        <div data="name" class="caption edit"><?= $resulted['name'] ?></div>
                                    </div>
                                    <div class="row-info">
                                        <div class="title">Nazwisko:</div>
                                        <div data="surname" class="caption edit"><?= $resulted['surname'] ?></div>
                                    </div>
                                    <div class="row-info">
                                        <div class="title">Data urodzenia:</div>
                                        <div data="birthday" class="caption edit"><?= $resulted['birthday'] ?></div>
                                    </div>
                                    <div class="row-info">
                                        <div class="title">Płeć:</div>
                                        <div data="sex" class="caption"><?= $resulted['gender'] ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-4 col-sm-4 col-md-4 col-lg-4 block-2">
                                <div class="block-user-info">
                                    <div class="row-info">
                                        <div class="title">Kraj:</div>
                                        <div class="caption">
                                            <span class="edit" data="country"><?= $resulted['country'] ?></span></div>
                                    </div>
                                    <div class="row-info">
                                        <div class="title">Miasto:</div>
                                        <div class="caption">
                                            <span data="city" class="edit"><?= $resulted['city'] ?></span></div>
                                    </div>
                                    <div class="row-info">
                                        <div class="title">Zawód:</div>
                                        <div data="professions"
                                             class="caption edit"><?= $resulted['profession'] ?></div>
                                    </div>
                                    <div class="wrap-btn wrap-btn-1">
                                        <a onclick="return false" style="display: block" id="edit_user_cabinet" href="#"
                                           class="btn-edit">Edytuj</a>
                                    </div>
                                    <div class="wrap-btn wrap-btn-1">
                                        <a onclick="return false" style="display: none" id="save_user_cabinet" href="#"
                                           class="btn-edit btn-save">Zapisz</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="group_registration" class="block-registration">
                        <div class="part left_part">
                            <div class="wrap-block">
                                <div class="caption">
                                    <h3 class="faktura_title">Dane do faktury</h3>
                                    <div>(wypełnij, jeśli nie chcesz uzupełniać danych dla każdej faktury)</div>
                                </div>

                            </div>
                        </div>
                        <div class="part right_part">
                            <div class="row-info row_faktura">
                                <div class="title">Nazwa firmy lub os. fiz:</div>
                                <div data="name-company"
                                     class="caption edit_faktura"><?= $resulted['name_company'] ?></div>
                            </div>
                            <div class="row-info row_faktura">
                                <div class="title">NIP:</div>
                                <div data="NIP" class="caption edit_faktura"><?= $resulted['nip'] ?></div>
                            </div>
                            <div class="row-info row_faktura">
                                <div class="title">Kod pocztowy:</div>
                                <div data="postal-code"
                                     class="caption edit_faktura"><?= $resulted['postal_code'] ?></div>
                            </div>
                            <div class="row-info row_faktura">
                                <div class="title">Kraj:</div>
                                <div data="region" class="caption edit_faktura"><?= $resulted['region'] ?></div>
                            </div>
                            <div class="row-info row_faktura">
                                <div class="title">Miasto:</div>
                                <div data="city" class="caption edit_faktura"><?= $resulted['company_city'] ?></div>
                            </div>
                            <div class="row-info row_faktura">
                                <div class="title">Ulica:</div>
                                <div data="street" class="caption edit_faktura"><?= $resulted['company_street'] ?></div>
                            </div>
                            <div class="wrap_btn_save">
                                <a onclick="return false" style="display: inline-block" id="edit_user_factura" href="#"
                                   class="btn-edit">Edytuj</a>
                                <button type="submit" id="save_user_faktury" class="btn">Zapisz dane do faktury</button>
                                <div class="checkbox-buy">
                                    <input type="checkbox" <?= $resulted['checkFacture'] == 'true' ? 'checked' : '' ?>
                                           class="checkbox_faktury" id="bilet-zbiorczy">
                                    <label for="bilet-zbiorczy"
                                           class="<?= $resulted['checkFacture'] === 'true' ? 'checked' : '' ?>">
                                        <span checkFacture></span><strong>Uzupełniać dane dla każdej faktury</strong>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-password-change">
                        <div class="container">
                            <div class="form-change-password">
                                <div class="col">
                                    <div class="title">Zmiana hasła:</div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <input id="old-password" type="password" placeholder="Stare hasło"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <input id="new-password" type="password" placeholder="Nowe hasło"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="wrap-btn">
                                        <button id="change_password" type="submit" class="btn btn-change-password">
                                            Zapisz
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="tab-2" class="tab-pane fade<?= ($_GET['page'] == 'sale') ? ' in active' : '' ?>">
                    <div class="block-filters"
                         style="display: <?= ($paidTickets) ? "block" : "none" ?>">

                        <div class="row-f">
                            <div class="col col-1">
                                <div data-sort="order:asc" class="title sort-btn">Bilet</div>
                            </div>
                            <div class="col col-2">
                                <div class="title">Relacja</div>
                            </div>
                            <div class="col col-3">
                                <div id="sortByDate" class="title">Data</div>
                            </div>
                            <div class="col col-4">
                                <div id="sortByPrice" class="title">Cena</div>
                            </div>
                            <div class="col col-5"></div>
                        </div>
                    </div>
                    <div id="saled">
                        <?php while ($row = mysql_fetch_assoc($paidTickets)) { ?>
                            <!--  One ticket-->
                            <div class="block-tickets fadeInUp mix-target undergraduate"
                                 data-order="<?= $row['number_ticket']; ?>" data-year="2">
                                <div class="row-ticket">
                                    <div class="row-t">
                                        <div class="col col-1">
                                            <div class="numb-ticket"><?= $row['number_ticket']; ?></div>
                                            <div class="city-ticket"><?= $row['ticket_type']; ?></div>
                                        </div>
                                        <div class="col col-2">
                                            <div class="ticket-distance"><?= $row['station_from']; ?>
                                                / <?= $row['station_to']; ?></div>
                                        </div>
                                        <div class="col col-3">
                                            <div class="ticket-date">
                                                <div class="ticket-date-row">
                                                    <span class="title">Wijazd: </span>
                                                    <span class="caption"><?php $data = str_split($row['departure_time'], 16);
                                                        echo $data[0] ?></span>
                                                </div>
                                                <div class="ticket-date-row">
                                                    <span class="title">Przyjazd: </span>
                                                    <span class="caption"><?php $data = str_split($row['arrival_time'], 16);
                                                        echo $data[0] ?></span>
                                                </div>
                                                <div class="ticket-date-row">
                                                    <span class="title">Czas podróźy: </span>
                                                    <span class="caption time"><?= $row['total_time_road']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-4">
                                            <div class="ticket-price">
                                                <span class="price"><?= $row['price']; ?> zł</span>
                                                <span class="valute"></span>
                                            </div>
                                        </div>
                                        <div class="col col-5">
                                            <div class="wrap-btn">
                                                <a href="#" class="btn-cancellation">Anulowanie</a>
                                            </div>
                                        </div>
                                        <div class="col col-5">
                                            <div class="wrap-btn">
                                                <a href="download.php?transaction=<?= $row['transaction_id'] ?>&number=<?= $row['number_ticket'] ?>"
                                                   download="bilet.pdf" class="btn-pay">Pobierz</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php
                    if ($numPagesPaidTickets > 1) { ?>
                        <div class='pagination'>
                            <?php if ($_GET['sale'] > 1) { ?>
                                <a onclick="paginationHref('sale','<?= $_GET['sale'] - 1 ?>')" href='#'>≪</a>
                            <?php } ?>
                            <?php for ($i = 1; $i <= $numPagesPaidTickets; $i++) { ?>
                                <?php if ($i == 1 && ($_GET['sale'] - 3) > 1) { ?>
                                    <a onclick="paginationHref('sale','1')" href='#'>1</a>

                                    <?php $i = $_GET['sale'] - 3; ?>
                                    <a onclick="paginationHref('sale','<?= $i ?>')" href='#'>...</a>

                                <?php } elseif ($i == ($_GET['sale'] + 3) && $i != $numPagesPaidTickets) { ?>
                                    <a onclick="paginationHref('sale','<?= $i ?>')" href='#'>...</a>
                                    <a onclick="paginationHref('sale','<?= $numPagesPaidTickets ?>')"
                                       href='#'><?= $numPagesPaidTickets ?></a>
                                    <?php $i = $numPagesPaidTickets; ?>
                                <?php } else { ?>
                                    <a onclick="paginationHref('sale','<?= $i ?>')"
                                       class='<?= ($_GET['sale'] == $i || !isset($_GET['sale']) && $i == 1) ? "active" : "" ?>'
                                       href='#'><?= $i ?></a>
                                <?php } ?>

                            <?php } ?>

                            <?php if (!($_GET['sale'] >= $numPagesPaidTickets)) { ?>
                                <a onclick="paginationHref('sale','<?= $_GET['sale'] + 1 ?>')" href='#'>≫</a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>


                <div id="tab-3"
                     class="tab-pane anulowane_tab fade<?= ($_GET['page'] == 'annulled') ? ' in active' : '' ?>">
                    <div class="block-filters"
                         style="display: <?= ($AnnulledTickets) ? "block" : "none" ?>">
                        <div class="row-f">
                            <div class="col col-1">
                                <div class="title">Bilet</div>
                            </div>
                            <div class="col col-2">
                                <div class="title">Relacja</div>
                            </div>
                            <div class="col col-3">
                                <div class="title">Data</div>
                            </div>
                            <div class="col col-4">
                                <div class="title">Cena</div>
                            </div>
                        </div>
                    </div>
                    <div id="cancelled">
                        <?php while ($row = mysql_fetch_assoc($AnnulledTickets)) { ?>
                            <!--  One ticket-->
                            <div class="block-tickets fadeInUp" data-order="<?= $row['number_ticket']; ?>">
                                <div class="row-ticket">
                                    <div class="row-t">
                                        <div class="col col-1">
                                            <div class="numb-ticket"><?= $row['number_ticket']; ?></div>
                                            <div class="city-ticket"><?= $row['ticket_type']; ?></div>
                                        </div>
                                        <div class="col col-2">
                                            <div class="ticket-distance"><?= $row['station_from']; ?>
                                                / <?= $row['station_to']; ?></div>
                                        </div>
                                        <div class="col col-3">
                                            <div class="ticket-date">
                                                <div class="ticket-date-row">
                                                    <span class="title">Wijazd: </span>
                                                    <span class="caption"><?php $data = str_split($row['departure_time'], 16);
                                                        echo $data[0] ?></span>
                                                </div>
                                                <div class="ticket-date-row">
                                                    <span class="title">Przyjazd: </span>
                                                    <span class="caption"><?php $data = str_split($row['arrival_time'], 16);
                                                        echo $data[0] ?></span>
                                                </div>
                                                <div class="ticket-date-row">
                                                    <span class="title">Czas podróźy: </span>
                                                    <span class="caption time"><?= $row['total_time_road']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-4">
                                            <div class="ticket-price">
                                                <span class="price"><?= $row['price']; ?> zł</span>
                                                <span class="valute"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php
                    if ($numPagesAnnulledTickets > 1) { ?>
                        <div class='pagination'>
                            <?php if ($_GET['annulled'] > 1) { ?>
                                <a onclick="paginationHref('annulled','<?= $_GET['annulled'] - 1 ?>')" href='#'>≪</a>
                            <?php } ?>
                            <?php for ($i = 1; $i <= $numPagesAnnulledTickets; $i++) { ?>
                                <?php if ($i == 1 && ($_GET['annulled'] - 3) > 1) { ?>
                                    <a onclick="paginationHref('annulled','1')" href='#'>1</a>

                                    <?php $i = $_GET['annulled'] - 3; ?>
                                    <a onclick="paginationHref('annulled','<?= $i ?>')" href='#'>...</a>

                                <?php } elseif ($i == ($_GET['annulled'] + 3) && $i != $numPagesAnnulledTickets) { ?>
                                    <a onclick="paginationHref('annulled','<?= $i ?>')" href='#'>...</a>
                                    <a onclick="paginationHref('annulled','<?= $numPagesAnnulledTickets ?>')"
                                       href='#'><?= $numPagesAnnulledTickets ?></a>
                                    <?php $i = $numPagesAnnulledTickets; ?>
                                <?php } else { ?>
                                    <a onclick="paginationHref('annulled','<?= $i ?>')"
                                       class='<?= ($_GET['annulled'] == $i || !isset($_GET['annulled']) && $i == 1) ? "active" : "" ?>'
                                       href='#'><?= $i ?></a>
                                <?php } ?>
                            <?php } ?>

                            <?php if (!($_GET['annulled'] >= $numPagesAnnulledTickets)) { ?>
                                <a onclick="paginationHref('annulled','<?= $_GET['annulled'] + 1 ?>')"
                                   href='#'>≫</a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>

                <div id="tab-4"
                     class="tab-pane anulowane_tab fade<?= ($_GET['page'] == 'invoices') ? ' in active' : '' ?>"
                ">
                <div class="block-filters"
                     style="display: <?= ($Invoices) ? "block" : "none" ?>">
                    <div class="row-f">
                        <div class="col col-1">
                            <div class="title">Bilet</div>
                        </div>
                        <div class="col col-2">
                            <div class="title">Relacja</div>
                        </div>
                        <div class="col col-3">
                            <div class="title">Data</div>
                        </div>
                        <div class="col col-4">
                            <div class="title">Cena</div>
                        </div>
                    </div>
                </div>
                <div id="cancelled">
                    <?php while ($row = mysql_fetch_assoc($Invoices)) { ?>
                        <!--  One ticket-->
                        <div class="block-tickets fadeInUp" data-order="<?= $row['id']; ?>">
                            <div class="row-ticket">
                                <div class="row-t">
                                    <div class="col col-1">
                                        <div class="numb-ticket"><?= $row['id']; ?></div>
                                        <div class="city-ticket"><?= $row['ticket_type']; ?></div>
                                    </div>
                                    <div class="col col-2">
                                        <div class="ticket-distance"><?= $row['station_from']; ?>
                                            / <?= $row['station_to']; ?></div>
                                    </div>
                                    <div class="col col-3">
                                        <div class="ticket-date">
                                            <div class="ticket-date-row">
                                                <span class="title">Wijazd: </span>
                                                <span class="caption"><?php $data = str_split($row['departure_time'], 16);
                                                    echo $data[0] ?></span>
                                            </div>
                                            <div class="ticket-date-row">
                                                <span class="title">Przyjazd: </span>
                                                <span class="caption"><?php $data = str_split($row['arrival_time'], 16);
                                                    echo $data[0] ?></span>
                                            </div>
                                            <div class="ticket-date-row">
                                                <span class="title">Czas podróźy: </span>
                                                <span class="caption time"><?= $row['total_time_road']; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-4">
                                        <div class="ticket-price">
                                            <span class="price"><?= $row['price']; ?> zł</span>
                                            <span class="valute"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
                if ($numPagesInvoices > 1) { ?>
                    <div class='pagination'>
                        <?php if ($_GET['invoices'] > 1) { ?>
                            <a onclick="paginationHref('invoices','<?= $_GET['invoices'] - 1 ?>')" href='#'>≪</a>
                        <?php } ?>
                        <?php for ($i = 1; $i <= $numPagesInvoices; $i++) { ?>
                            <?php if ($i == 1 && ($_GET['invoices'] - 3) > 1) { ?>
                                <a onclick="paginationHref('invoices','1')" href='#'>1</a>

                                <?php $i = $_GET['invoices'] - 3; ?>
                                <a onclick="paginationHref('invoices','<?= $i ?>')" href='#'>...</a>

                            <?php } elseif ($i == ($_GET['invoices'] + 3) && $i != $numPagesInvoices) { ?>
                                <a onclick="paginationHref('invoices','<?= $i ?>')" href='#'>...</a>
                                <a onclick="paginationHref('invoices','<?= $numPagesInvoices ?>')"
                                   href='#'><?= $numPagesInvoices ?></a>
                                <?php $i = $numPagesInvoices; ?>
                            <?php } else { ?>
                                <a onclick="paginationHref('invoices','<?= $i ?>')"
                                   class='<?= ($_GET['invoices'] == $i || !isset($_GET['invoices']) && $i == 1) ? "active" : "" ?>'
                                   href='#'><?= $i ?></a>
                            <?php } ?>

                        <?php } ?>

                        <?php if (!($_GET['invoices'] >= $numPagesInvoices)) { ?>
                            <a onclick="paginationHref('invoices','<?= $_GET['invoices'] + 1 ?>')" href='#'>≫</a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>


        </div>
    </div>
</div>
</div>
<?php include_once('footer.php') ?>
<div id="footer">
    <div class="bg-contact"></div>
    <div class="footer">
        <div class="container">
            <div class="row-f">
                <div>
                    <div class="footer-carousel">
                        <div class="carousel-company">
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-1.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-2.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-3.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-4.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-5.png" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-6.jpg" alt=""></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-text">
                <div>Nasz serwis wykorzystuje pliki cookies. Korzystanie z witryny oznacza zgodę na ich
                    zapis lub odczyt zgodnie z ustawieniami przeglądarki.
                </div>
                <div class="copy">Copyright © MDA S.A Kraków</div>
            </div>
        </div>
    </div>
</div>
<script>

    function paginationHref(type, number) {
        console.log(type);
        console.log(number);
        var href = location.search.split('&')[0];
        if (location.search.match(new RegExp('&' + type + '=')) == '&' + type + '=') {
            console.log('if')
            href = location.search.replace(new RegExp('&' + type + '=\\d'), '&' + type + '=' + number);
        } else {
            console.log('ele')
            href += '&' + type + '=' + number;
        }
        console.log(href);
        location.search = href;
    }
</script>
<script src="libs/jquery/jquery-1.9.1.min.js"></script>
<script src="libs/bootstrap/js/bootstrap.min.js"></script>
<script src="libs/waypoints/waypoints.min.js"></script>
<script src="libs/animate/animate-css.js"></script>
<script src="libs/plugins-scroll/plugins-scroll.js"></script>
<script src="libs/WOW/wow.js"></script>
<script src="libs/slick/slick.min.js"></script>
<script src="libs/slicknav/jquery.slicknav.min.js"></script>
<!--<script src="libs/equalheight/jquery.equalheight.js"></script>-->
<script src="libs/jquery.matchHeight-min.js"></script>
<script src="libs/ui/jquery-ui.js"></script>
<script src="libs/ui/jquery.ui.timepicker-pl.js"></script>
<script src="libs/ui/datepicker-pl.js"></script>
<script src="libs/ui/datepicker-de.js"></script>
<script src="libs/ui/jquery.ui.timepicker.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/purify.min.js"></script>
<script src="libs/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="libs/bootstrap-fileinput/themes/fa/theme.js"></script>
<script src="libs/bootstrap-fileinput/js/locales/pl.js"></script>
<script src="libs/jquery-cookie/cookie.js"></script>
<script src="libs/sweetalert/sweetalert.min.js"></script>
<script src="libs/jq-validation/jquery.validate.min.js"></script>
<script src="libs/jq-validation/additional-methods.min.js"></script>
<script src="js/common.js"></script>
<script src="js/account.js"></script>
<script src="js/registration.js"></script>
<script src="js/main.js"></script>
</body>
</html>