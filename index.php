<?php
include_once("header.php");
$db = Model::Instance();
$slider = $db->All_rows('slider_1');
?>

<link rel="stylesheet" href="libs/jQueryFormStyler/jquery.formstyler.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.css">
<script src="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.js"></script>

<div class="block-header">
    <div class="container">
        <div class="block-title">Wyszukaj połączenie</div>
    </div>
</div>
<style>
    .ui-autocomplete-loading {
        background: white url("<?= SERVER_URL ?>/img/preloader.gif") 3% center no-repeat !important;
        background-size: 9% !important;
    }
</style>
<div class="block-form-header">
    <div class="container">
        <div stationInputs class="form-trail">
            <div class="form-group input-from-trail">
                <input id="pointA" type="text" tabindex="1" placeholder="Wyjazd z ..." class="from-trail form-control"
                       list="A" autocomplete="off">
                <datalist id="A"></datalist>
            </div>
            <button class="reverse"></button>
            <div class="form-group input-to-trail">
                <input id="pointB" type="text" tabindex="2" placeholder="Przyjazd do ..." class="to-trail form-control"
                       list="B" autocomplete="off">
                <datalist id="B"></datalist>
            </div>

            <div class="form-group">
                <input id="datetimepicker1"  readonly='true' type="text" tabindex="3" value="" class="form-control data-input">
            </div>
            <div class="form-group">
                <input id="datetimepicker2" type="text" tabindex="4" value="00:00" class="form-control data-input">
                <div class="wrongTile">Zla godzina</div>
            </div>
            <div class="form-group" style="display: none">
                <div class="checkbox">
                    <input type="checkbox" id="without-transfers">
                    <label for="without-transfers"><span></span>Bez przesiadek</label>
                </div>
            </div>
            <button id="search" type="submit" tabindex="5" class="btn btn-search">Wyszukaj</button>
        </div>
    </div>
</div>

<div id="render"></div>

<div class="wrap-middle">
    <div class="slider-middle">
        <?php
            foreach ($slider as $slide) {
            ?>
            <div class="slide" style="background: url(pictures/slider_1/<?= $slide["image1"] ?>) 0 0 no-repeat;"></div>
            <?
        }
        ?>
    </div>
    <div class="bg-bottom"></div>
    <?php include_once('footer.php') ?>
</div>
<div id="footer">
    <div class="bg-contact"></div>
    <div class="footer">
        <div class="container">
            <div class="row-f">
                <div>
                    <div class="footer-carousel" style="display: none;">
                        <div class="carousel-company">
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-1.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-2.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-3.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-4.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-5.png" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-6.jpg" alt=""></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-text">
                <div>Nasz serwis wykorzystuje pliki cookies. Korzystanie z witryny oznacza zgodę na ich ich zapis lub odczyt zgodnie z ustawieniami przeglądarki.</div>
                <div class="copy">Copyright © MDA S.A Kraków</div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#datetimepicker1').mask('0000-00-00');
        $('#datetimepicker2').mask('00:00');
    })
</script>
<!--<script src="libs/jquery/jquery-1.9.1.min.js"></script>-->
<script src="libs/bootstrap/js/bootstrap.min.js"></script>
<script src="libs/waypoints/waypoints.min.js"></script>
<script src="libs/animate/animate-css.js"></script>
<script src="libs/plugins-scroll/plugins-scroll.js"></script>
<script src="libs/WOW/wow.js"></script>
<script src="libs/slick/slick.min.js"></script>
<script src="libs/slicknav/jquery.slicknav.min.js"></script>
<script src="libs/equalheight/jquery.equalheight.js"></script>
<script src="libs/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.js"></script>
<script src="libs/ui/jquery.ui.timepicker-pl.js"></script>
<script src="libs/ui/datepicker-pl.js"></script>
<script src="libs/ui/datepicker-de.js"></script>
<script src="libs/ui/jquery.ui.timepicker.js"></script>
<script src="libs/jQueryFormStyler/jquery.formstyler.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/purify.min.js"></script>
<script src="libs/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="libs/bootstrap-fileinput/themes/fa/theme.js"></script>
<script src="libs/bootstrap-fileinput/js/locales/pl.js"></script>

<!--<script src="libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>-->
<!--<script src="libs/bootstrap-datepicker/locales/bootstrap-datepicker.pl.min.js"></script>-->
<script src="libs/jquery-cookie/cookie.js"></script>
<script src="libs/jq-validation/jquery.validate.min.js"></script>
<script src="libs/jq-validation/additional-methods.min.js"></script>
<!--<script src="js/common.js"></script>-->

<script src="js/registration.js?v=1"></script>
<script src="js/account.js?v=1"></script>
<script src="js/main.js?v=2"></script>
<script src="js/common.js?v=1"></script>
</body>
</html>