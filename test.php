<?php

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"http://82.160.155.29:10059/apiServer?locality_name=kark&search_type=1&");
curl_setopt($ch, CURLOPT_HEADER, 0); //выводим заголовки
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_USERAGENT, 'MSIE 5'); //эта строчка как-бы говорит: "я не скрипт, я IE5" :)
curl_setopt($ch, CURLOPT_REFERER, "http://pkskrakow.nazwa.pl"); //а вдруг там проверяют наличие рефера

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

for ($i=1,$n=0;$n<$i;$i++,$n++){
//print_r('test');
    $res = curl_exec($ch);
}
if (!$res) {
    $error = curl_error($ch) . '(' . curl_errno($ch) . ')';
    echo $error;
} else {
    print_r(json_decode($res));
}

curl_close ($ch);

?>

<?'
<table style="border-collapse: collapse; border-top-left-radius: 30px; border: 1px solid #326599;" class="tg">
    <tr style="background: #326599; border-top-left-radius: 30px;">
        <th class="tg-yw4l"><img width="20px" src="img/bg-header-ticket.png"> </th>
        <th class="tg-yw4l" style="color: #fff">Nr biletu<br>Ticket no</th>
        <th class="tg-glis" style="color: #fff"  align="left" colspan="5">EL ' . $ticket['course_id'] . '</th>
    </tr>
    <tr>
        <td class="tg-yw4l" rowspan="2"><img width="35px" src="img/rotate_block_1.png"> </td>
        <td class="tg-yw4l" style="padding: 15px;" colspan="2" rowspan="2"><b>' . $ticket['carrier']['description'] . '</b><br>
            ' . $ticket['carrier']['address'] . '<br>
            Tel. ' . $ticket['carrier']['phone'] . '<br>
            NIP: ' . $ticket['carrier']['vat_number'] . '</td>
        <td class="tg-yw4l" rowspan="2" style=" border-right: 1px solid #000;"><img width="60px" src="img/bg-bus-ticket.png"> <br></td>
        <td class="tg-3we0" align="left" colspan="2" style="padding-left: 5px; border-bottom: 1px solid #000;border-right: 1px solid #000;" rowspan="2"><strong>Nabywca</strong> | Buyer ' . $ticket['name'] . '<br> e-mail: ' . $ticket['email'] . '</td>
        <td class="tg-yw4l" align="left" style="padding-left: 5px; border-bottom: 1px solid #000; "><strong>Data sprzedaży</strong><br><span>' . date('Y-m-d') . '</td>
    </tr>
    <tr>
        <td class="tg-yw4l" style="padding-left: 5px; border-bottom: 1px solid #000; " ><strong>Odległość</strong> | Distance<br><b>' . $ticket['distance'] . '</b> km</td>
    </tr>
    <tr >
        <td class="tg-yw4l" style="border-top: 2px solid #326599; border-bottom: 2px solid #326599;" rowspan="8"><img width="35px" src="img/rotate_block_2.jpg"></td>
        <td class="tg-yw4l" style="padding-left: 5px; background: #eceded; border-top: 2px solid #326599;"><strong>Data</strong> | Date</td>
        <td class="tg-yw4l" style="background: #eceded; border-top: 2px solid #326599;" align="center"><strong>Godzina</strong> | Hour</td>
        <td class="tg-yw4l" style="background: #eceded;  border-top: 2px solid #326599; border-right: 2px solid #326599"><strong>Miasto</strong> | City</td>
        <td class="tg-yw4l" style="padding-left: 5px;padding-top: 5px; padding-bottom:5px; border-bottom: 1px solid #000;" colspan="2"><b>' . getTicketTypePl($ticket) . '</b><br>' . getTicketTypeEn($ticket) . '</td>
        <td class="tg-yw4l"  rowspan="9" style="padding-left: 5px; border-left: 1px solid #000"><div id="qrcode-' . $ticket['course_id'] . '" class="ticket_row"><img style="padding-left: 4px; padding-bottom: 10px;"  width="70px" src="' . getQrCode($ticket) . '"></div></td>
    </tr>
    <tr>
        <td class="tg-yw4l" rowspan="3" style="padding-left: 5px; background: #eceded; color: #326599; font-size: 12px;"><b>' . explode(' ', $ticket['departure'])[0] . '</b></td>
        <td class="tg-yw4l" rowspan="3" style=" background: #eceded; color: #326599; font-size: 12px;" align="center"><b>' . date_create_from_format('Y.m.d H:i:s', $ticket['departure'])->format('H:i') . '</b></td>
        <td class="tg-yw4l" style=" background: #eceded; border-right: 2px solid #326599; " rowspan="3"><span style="color: #326599; font-size: 12px;"><b>' . $ticket['locality_from'] . '</b></span><br>' . $ticket['station_from'] . '</td>
        <td class="tg-yw4l" style="padding-left: 3px; padding-top: 10px;"><b>Ulga </b>| Discount</td>
        <td class="tg-yw4l" style="padding-right: 5px; padding-top: 10px;" align="right">' . $ticket['current_ulga'] . '</td>
    </tr>
    <tr>
        <td class="tg-yw4l" style="padding-left: 3px;"><b>Ilość</b> | Count</td>
        <td class="tg-yw4l" style="padding-right: 5px;" align="right">' . $ticket['count']['count'] . '</td>
    </tr>
    <tr>
        <td class="tg-yw4l" style="padding-left: 3px;"><b>Netto PL</b> | Net price:</td>
        <td class="tg-baqh" style="padding-right: 5px;" align="right">' . $net_price . ' zł</td>
    </tr>
    <tr>
        <td class="tg-6kuv" rowspan="4" style="padding-left: 5px; background: #eceded; border-bottom: 2px solid #326599; color: #326599; font-size: 12px;"><b>' . explode(' ', $ticket['arrival'])[0] . '</b></td>
        <td class="tg-yw4l" rowspan="4" style="background: #eceded; border-bottom: 2px solid #326599; color: #326599; font-size: 12px;" align="center"><b>' . date_create_from_format('Y.m.d H:i:s', $ticket['arrival'])->format('H:i') . '</b></td>
        <td class="tg-yw4l" style="border-right: 2px solid #326599; background: #eceded; border-bottom:2px solid #326599;" rowspan="4"><span style="color: #326599; font-size: 12px;"><b>' . $ticket['locality_to'] . '</b></span><br>' . $ticket['station_to'] . '</td>
        <td class="tg-yw4l" style="padding-left: 5px;"><b>VAT PL:</b></td>
        <td class="tg-yw4l" style="padding-right: 5px;" align="right">' . number_format(floatval($ticket['price_vat']), 2) . ' zł</td>
    </tr>
    <tr>
        <td class="tg-yw4l" style="padding-left: 5px;"><b>Brutto</b>  | Gross price</td>
        <td class="tg-yw4l" style="padding-right: 5px;" align="right">' . number_format($ticket['fullPrice'], 2) . ' zł</td>
    </tr>
    <tr>
        <td class="tg-yw4l" style="padding-left: 5px;"><b>Rabat</b> | Discount </td>
        <td class="tg-yw4l" style="padding-right: 5px;" align="right">' . number_format($discount, 2) . ' zł</td>
    </tr>
    <tr>
        <td class="tg-yw4l" style="padding-left: 5px; padding-top: 15px;"><b>RAZEM</b> | Total</td>
        <td class="tg-yw4l" align="right" style="padding-right: 5px; padding-top: 15px; color: #326599;"><b>' . $total_price . '</b> zł</td>
    </tr>
    <tr>
        <td class="tg-yw4l" colspan="2"><img width="50px" style="padding-left: 5px; " src="img/logo-mda-ticket.png"> </td>
        <td class="tg-yw4l" style="padding-top: 5px;  padding-bottom: 5px;"> Małopolskie Dworce Autobusowe S.A.<br> ul. Bosacka 18, 31-505 Kraków, tel. 12 351 82 08</td>
        <td class="tg-yw4l"style="border-right: 1px solid #000;"></td>
        <td class="tg-yw4l" ></td>
        <td class="tg-yw4l"></td>
    </tr>
</table>
<br>



';
?>

<table border="1" style="width: 100%;">
    <tr style="display: none;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="1" style="text-align: center; border: 1px solid black;height: 40px;"><img src="./img/logo-pks-pl.png" /></td>
        <td colspan="5" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Nr biletu</small><p><b class="sm-f"><!--EL--> ' . $ticket['course_id'] . '</b></p></span></td>
        <td colspan="4" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Sposób sprzedaży</small><p><b class="sm-f">Internet</b></p></span></td>
        <td colspan="4" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Data sprzedaży</small><p><b class="sm-f">' . date('Y-m-d') . '</b></p></span></td>
        <td colspan="4" style="text-align: center; border: 1px solid black;"><span style="text-align: center"><small>Nr paragonu fiskalnego</small><p><b class="sm-f">' . $ticket['course_id'] . '</b></p></span></td>
    </tr>

    <tr>
        <td colspan="3" style="border-left:1px solid black;">
            <small>Sprzedawca (Przewoźnik)</small>
        </td>
        <td colspan="5"></td>
        <td colspan="2"><small class="vsm-f">NIP: ' . $ticket['carrier']['vat_number'] . '</small></td>
        <td style="border-left:1px solid black" colspan="3">
        <small class="vsm-f">Nabywca (Pasażer)</small></td>
        <td style="border-right: 1px solid black" colspan="5"></td>
    </tr>

    <tr>
        <td colspan="8" style="border-left:1px solid black;"><b>' . $ticket['carrier']['description'] . '</b></td>
        <td colspan="2"></td>
        <td  style="border-left:1px solid black" colspan="6"><b style="font-size:11px">' . $ticket['name'] . '  ' . $ticket['surName'] . '</b></td>
        <td style="border-right: 1px solid black" colspan="2"></td>
    </tr>

    <tr>
        <td colspan="8" style="border-left:1px solid black"><small class="vsm-f">' . $ticket['carrier']['address'] . '</small></td>
        <td colspan="2"></td>
        <td style="border-left:1px solid black" colspan="6"><small class="vsm-f">' . $ticket['email'] . '</small></td>
        <td style="border-right: 1px solid black" colspan="2"></td>
    </tr>

    <tr>
        <td colspan="3" style="border-left:1px solid black"><small></small></td>
        <td colspan="5"></td>
        <td colspan="2"><b style="font-size: 8px">Polski Bus</b></td>
        <td style="border-left:1px solid black" colspan="3"><small>Tel: ' . $ticket['phone'] . '</small></td>
        <td style="border-right: 1px solid black" colspan="5"></td>
    </tr>

    <tr>
        <td colspan="10" style="border-left:1px solid black; border-bottom:1px solid black; border-top:1px solid black">
        <small>Oferent:</small>
        </td>
        <td style="border:1px solid black;border-right: 0px" colspan="4">
            <b style="font-size: 8px;">Telefon do kierowcy:</b>
        </td>
        <td style="border-top:1px solid black;border-right: 1px solid black" align="right" colspan="4"><small>+48146371777,+48146371777</small></td>
    </tr>

    <tr>
        <div>
    <tr>
        <td class="gray no-borders-gray" style="border-top:1px solid black;border-right: 1px solid black;border-left:1px solid black" colspan="15" align="center">
            <span style="font-size:9px">Nazwa linii(kursu): </span>
            <b>'. $ticket["begin_station_locality"] .' - '. $ticket["end_station_locality"] .'</b>
        </td>
        <td style="border-right: 1px solid black" align="right" colspan="3" rowspan="1"><small>+48146371777,+48146371777</small></td>
    </tr>

    <tr style="padding:0px">
        <td class="gray no-borders-gray" style="border-left:1px solid black;" colspan="2"><small class="vsm-f">Data</small></td>
        <td class="gray no-borders-gray" colspan="2" align="center"><small class="vsm-f">Godzina</small></td>
        <td colspan="2" class="gray no-borders-gray"></td>
        <td class="gray no-borders-gray" colspan="4" style="padding:0"><small class="vsm-f">Miejscowość</small></td>
        <td class="gray no-borders-gray" colspan="2" align="right"><small class="vsm-f">Długość trasy </small></td>
        <td class="gray no-borders-gray" colspan="1" align="right"></td>
        <td style="border-right: 1px solid black" class="gray no-borders-gray" colspan="2" align="right"><b>' . $ticket['distance'] . ' km</b></td>
        <td colspan="3" style="border-right: 1px solid black"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;" class="gray no-borders-gray" colspan="2"><b>' . explode(' ', $ticket['departure'])[0] . '</b></td>
        <td class="gray no-borders-gray" colspan="2"><b>' . date_create_from_format('Y.m.d H:i:s', $ticket['departure'])->format('H:i') . '</b></td>
        <td colspan="2" class="gray no-borders-gray"></td>
        <td class="gray no-borders-gray" colspan="4"><b>' . $ticket['locality_from'] . '</b></td>
        <td style="border-right: 1px solid black" class="gray no-borders-gray" colspan="5"></td>
        <td style="border:1px solid black;" colspan="3" rowspan="14"><img style="padding-left: 4px; padding-bottom: 10px;" src="' . getQrCode($ticket) . '"></td>

    </tr>

    <tr>
        <td style="border-left:1px solid black;" class="gray no-borders-gray" colspan="2"></td>
        <td class="gray no-borders-gray" colspan="2"></td>
        <td colspan="2" class="gray no-borders-gray"></td>
        <td class="gray no-borders-gray" colspan="4"><small>' . $ticket['station_from'] . '</small></td>
        <td class="gray no-borders-gray" colspan="5"></td>
        <td style="border-right: 1px solid black" colspan="3"></td>

    </tr>

    <tr>
        <td style="border-left:1px solid black;" class="gray no-borders-gray" colspan="2"><b>' . explode(' ', $ticket['arrival'])[0] . '</b></td>
        <td class="gray no-borders-gray" colspan="2"><b>' . date_create_from_format('Y.m.d H:i:s', $ticket['arrival'])->format('H:i') . '</b></td>
        <td colspan="2" class="gray no-borders-gray"></td>
        <td class="gray no-borders-gray" colspan="7"><b>' . $ticket['locality_to'] . '	</b></td>
        <td class="gray no-borders-gray" colspan="2"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black; border-bottom: 1px solid black" class="gray no-borders-gray" colspan="2"></td>
        <td style="border-bottom:1px solid black;" class="gray no-borders-gray" colspan="2"></td>
        <td style="border-bottom:1px solid black;" colspan="2" class="gray no-borders-gray"></td>
        <td style="border-bottom:1px solid bottom;" class="gray no-borders-gray" colspan="7"><small>' . $ticket['station_to'] . '</small></td>
        <td style="border-bottom:1px solid bottom;" class="gray no-borders-gray" colspan="2"></td>
    </tr>

<!--    </div>-->

<!--    </tr>-->

    <tr>
        <td style="border-left:1px solid black;" colspan="3"><small class="vsm-f">Rodzaj biletu</small></td>
        <td colspan="4"></td>
        <td colspan="2"><small class="vsm-f">Ilość pasażerów</small></td>
        <td style="border:1px solid black" rowspan="2" width="5px" align="center"><b>' . $ticket['count']['count'] . '</b></td>
        <td style="border-left:1px solid black" colspan="5"><small class="vsm-f">Cena:</small></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;border-bottom: 1px solid black" ><b class="vsm-f">Międzynarodowy</b></td>
        <td style="border-bottom:1px solid black" ><b class="vsm-f">Normalny</b></td>
        <td style="border-bottom:1px solid black" colspan="8"></td>
        <td style="border-left:1px solid black" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;" colspan="4"><small class="vsm-f">Uwagi</small></td>
        <td colspan="1"></td>
        <td colspan="2"></td>
        <td colspan="2"></td>
        <td colspan="1"></td>
        <td style="border-left:1px solid black" colspan="1"><small class="vsm-f">Netto PL</small></td>
        <td colspan="3"></td>
        <td colspan="1" align="right"><b class="vsm-f">' . $net_price . '</b></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;" colspan="4"></td>
        <td colspan="1"></td>
        <td colspan="2"></td>
        <td colspan="2"></td>
        <td colspan="1"></td>
        <td style="border-left:1px solid black" colspan="1"><small class="vsm-f">Vat PL</small></td>
        <td colspan="3"></td>
        <td colspan="1" align="right"><b class="vsm-f" style="font-weight: bold">' . $vat_price . ' zł</span></b></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black; border-bottom: 1px solid black" colspan="4"></td>
        <td style="border-bottom: 1px solid black" colspan="1"></td>
        <td style="border-bottom: 1px solid black" colspan="2"></td>
        <td style="border-bottom: 1px solid black" colspan="2"></td>
        <td style="border-bottom: 1px solid black" colspan="1"></td>
        <td style="border-left:1px solid black" colspan="1"><small class="vsm-f">Brutto zagr.</small></td>
        <td colspan="3"></td>
        <td colspan="1" align="right"><b class="vsm-f">' . $full_price . ' zł</b></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black;" colspan="7"><small class="vsm-f">Administrator Platformy Komunikacyjno-Sprzedażowej PKS.pl</small>
        <td colspan="3"><small class="vsm-f">NIP: 00000000</small></td>
        <td style="border-left:1px solid black" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black; padding: 5px" colspan="10"></td>
        <td style="border-left:1px solid black;" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black"></td>
        <td colspan="9"></td>
        <td style="border-left:1px solid black" colspan="5"></td>
    </tr>

    <tr>
        <td style="border-left:1px solid black"></td>
        <td style="border-left:1px solid black;" colspan="9"></td>
        <td style="border-left:1px solid black" colspan="1"></td>
        <td colspan="3"></td>
        <td colspan="1"></td>
    </tr>

    <tr >
        <td style="border:1px solid black" rowspan="1" colspan="1" width="90px" align="center"><img src="./img/logo-mda-ticket.png" width="90px" height="20px"></td>
        <td style="border-left:1px solid black; border-bottom: 1px solid black" colspan="9"><b class="vsm-f">Małopolskie Dworce Autobusowe S.A.</b><br>
            <small class="vsm-f">31-514 Kraków, al. Beliny-Prażmowskiego 6A/6</small>
        </td>
        <td style="border-left:1px solid black;border-bottom: 1px solid black; padding-top: 5%" colspan="2"><b style="font-size: 11px">Razem</b></td>
        <td style="border-bottom: 1px solid black" colspan="2"></td>
        <td style="border-bottom: 1px solid black" padding-top: 5%" colspan="1" align="right"><b style="font-size:11px">' . $total_price . ' zł</b></td>
    </tr>
</table>