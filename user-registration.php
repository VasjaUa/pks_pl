<?php include_once('header.php') ?>
<div class="block-header">
    <div class="container">
        <div class="block-title">Rejestracja</div>
    </div>
</div>
<div class="block-registration-user">
    <div class="container">
        <div class="block-registration">
            <div class="block-title">Załóż nowe konto</div>
            <!--.caption Wprowadź dane:-->
            <form enctype="multipart/form-data" action="" method="post">
                <div class="row row-f">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 without_padding">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="wrap-block">
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="name" id="name" type="text" placeholder="Imię" required
                                           class="field-name form-control"></span>
                                    <span class="required">*</span>
                                </div>
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="surname" id="surname" type="text" placeholder="Nazwisko" required
                                           class="field-surname form-control"></span>
                                    <span class="required">*</span>
                                </div>
                                <div class="form-group sex-group">
                                    <div class="title">Płeć:</div>
                                    <div class="radio">
                                        <input type="radio" id="male" name="sex" value="male">
                                        <label for="male"><span></span>Mężczyzna</label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" id="female" name="sex" value="female">
                                        <label for="female"><span></span>Kobieta</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="phone" id="phone" type="phone" placeholder="Nr telefonu" required
                                                                     class="field-phone form-control"></span>
                                    <span class="required">*</span>
                                </div>
                                <div class="form-group">
                                    <!--label Preview File Icon-->
                                    <input name="file" id="file-3" type="file">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="wrap-block">
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="login" id="login1" type="text" placeholder="Login" required
                                                                     class="field-login form-control"></span>
                                    <span class="required">*</span>
                                </div>
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="email" id="email" type="email" placeholder="e-mail" required
                                                                     class="field-email form-control"></span>
                                    <span class="required">*</span>
                                </div>
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="password" id="password" type="password" placeholder="Hasło" required
                                           class="field-password form-control"></span>
                                    <span class="required">*</span>
                                </div>
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="repeat_password" id="repeat_password" type="password"
                                           placeholder="Powtórz hasło" required
                                           class="field-repeat-password form-control"></span>
                                    <span class="required">*</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="block-title">Dodatkowe dane:</div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 without_padding adress_data">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="wrap-block">
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="birthday" id="datetimepicker3" type="text" value="Data urodzenia"
                                                         class="form-control data-input birthday"></span>
                                </div>
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="country" id="region" type="text" placeholder="Kraj"
                                                         class="field-region form-control"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="wrap-block">
                                <div class="form-group">
                                    <span class="input_wrrap"><input name="city" id="city-user" type="text" placeholder="Miasto"
                                                                 class="field-city-user form-control"></span>
                                </div>
                                <div class="form-group">
                                    <!--<select name="" id="profession" class="form-control">
                                      <option value="Zawód">Zawód</option>
                                      <option value="1">1</option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="4">4</option>
                                    </select>-->
                                    <span class="input_wrrap"><input type="text" name="profy" class="form-control" id="profy" placeholder="Zawód"></span>
                                </div>
                            </div>
                        </div>
                    </div>
<!--                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 last_col">-->
<!--                        <div class="wrap-block">-->
<!--                            <div class="notation">* Pole wymagane</div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
                <div class="row row-f btn_wrrap_factura clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox-buy">
                            <input type="checkbox" class="checkbox_faktury btn_group_registration" autocomplete="off" id="btn_group_registration">
                            <label for="btn_group_registration" class="">
                                <span></span>
                                <strong>Dane do faktury</strong>
                            </label>
                        </div>
                    </div>
                </div>
                        <?php
                            include_once ('helpers/invoice/InvoiceHelper.php');
                            $in = new InvoiceHelper();
                            $in->getHtml();
                        ?>
                    <script>
                        $('#invoice_block').css('display','none');
                    </script>

                <div class="row row-f checkbox_row">
                    <div class="col-xs-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="checkbox">
                                    <span class="required">*</span>
                                    <input class="req" name="agree-to-receive" type="checkbox" id="agree-to-receive" title="Pole wymagane">
                                    <label for="agree-to-receive"><span></span><strong>Wyrażam zgodę na otrzymywanie
                                            faktur drogą elektroniczną</strong></label>
                                    <div style="display:none;" class="err-hidden"><div class="wrap-block"><div class="notation">* Pole wymagane</div> </div> </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <span class="required">*</span>
                                    <input class="req" name="regulamin" type="checkbox" id="regulamin" title="Pole wymagane">
                                    <label for="regulamin"><span></span><strong>Oświadczam, że zapoznałam/em się z Regulaminem Internetowej sprzedaży biletów przez MDA S.A.  i akceptuję jego warunki.</strong></label>
                                    <div style="display:none;" class="err-hidden"><div class="wrap-block"><div class="notation">* Pole wymagane</div> </div> </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <span style="visibility:hidden;" class="required">*</span>
                                    <input name="agree-to-sending" type="checkbox" id="agree-to-sending">
                                    <label for="agree-to-sending"><span></span><strong>Wyrażam zgodę na przesyłanie do
                                            mnie informacji handlowej przez MDA S.A. elektronicznie na podany adres
                                            e-mail</strong></label>
                                </div>
                            </div>
                            <div class="notation_text">
                                <p>W związku z dobrowolnym zarejestrowaniem się przez Panią/Pana w systemie internetowej sprzedaży biletów przekazane powyżej dane osobowe będą przetwarzane przez MDA, zgodnie z ustawą z dnia 29 sierpnia 1997r. o ochronie danych osobowych Dz. U. z 2002 r. Nr 101 poz. 926 z późniejszymi zmianami oraz Regulaminem Internetowej Sprzedaży Biletów w celu realizacji internetowej sprzedaży biletów, zapewnienia ciągłego dostępu do historii dokonanych transakcji.</p>
                                <p>Ma Pani/Pan prawo dostępu do swoich danych oraz ich poprawiania.</p>
                                <p>Osoby nie posiadające pełnej zdolności do czynności prawnych zobowiązane są do uzyskania uprzedniej zgody rodziców lub opiekunów prawnych.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-f">
                    <!-- <div class="row_wrrap"> -->
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row_col">
                            <div class="g-recaptcha" data-sitekey="6LdnHz0UAAAAAK5DSa24XEncpou9_Eh_n_6aKz-a"></div>
                        </div>
                        <input style="display:none" name="token" value="<?= md5(date('D:h:m:s')) ?>">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row_col">
                            <div class="wrap-btn">
                                <!--a.btn-registration(href="#") Załóż konto-->
                                <button type="submit" class="btn btn-registration">Załóż konto</button>
                            </div>
                        </div>
                    <!-- </div> -->
                </div>
            </form>
        </div>
    </div>
</div>



<!--<span class='msg'><?php /*echo $msg; */?></span>-->


<?php include_once('footer.php') ?>
<div id="footer">
    <div class="bg-contact"></div>
    <div class="footer">
        <div class="container">
            <div class="row-f">
                <div>
                    <div class="footer-carousel">
                        <!--.carousel-company
                        div
                            a(href="#")
                                img(src="../img/company-1.jpg", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-2.jpg", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-3.jpg", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-4.jpg", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-5.png", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-6.jpg", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-1.jpg", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-2.jpg", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-3.jpg", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-4.jpg", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-5.png", alt="")
                        div
                            a(href="#")
                                img(src="../img/company-6.jpg", alt="")
                        -->
                        <div class="carousel-company">
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-1.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-2.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-3.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-4.jpg" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-5.png" alt=""></a></div>
                                </div>
                            </div>
                            <div>
                                <div class="col">
                                    <div class="box-img"><a href="#"><img src="img/company-6.jpg" alt=""></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-text">
                <div>Nasz serwis wykorzystuje pliki cookies. Korzystanie z witryny oznacza zgodę na ich zapis lub odczyt zgodnie z ustawieniami przeglądarki.</div>
                <div class="copy">Copyright © MDA S.A Kraków</div>
            </div>
        </div>
    </div>
</div>
<!--if lt IE 9
script(src='libs/html5shiv/es5-shim.min.js')
script(src='libs/html5shiv/html5shiv.min.js')
script(src='libs/html5shiv/html5shiv-printshiv.min.js')
script(src='libs/respond/respond.min.js')
-->

<!--<script>-->
<!--    $('[data="postal-code"] input').mask('00-000', {clearIfNotMatch: true});-->
<!--    $('[data="NIP"] input').mask('0000000000', {clearIfNotMatch: true});-->
<!--</script>-->
<script src="libs/jquery/jquery-1.9.1.min.js"></script>
<script src="libs/bootstrap/js/bootstrap.min.js"></script>
<script src="libs/waypoints/waypoints.min.js"></script>
<script src="libs/animate/animate-css.js"></script>
<script src="libs/plugins-scroll/plugins-scroll.js"></script>
<script src="libs/WOW/wow.js"></script>
<script src="libs/slick/slick.min.js"></script>
<script src="libs/slicknav/jquery.slicknav.min.js"></script>
<script src="libs/equalheight/jquery.equalheight.js"></script>
<script src="libs/ui/jquery-ui.js"></script>
<script src="libs/ui/jquery.ui.timepicker-pl.js"></script>
<script src="libs/ui/datepicker-pl.js"></script>
<script src="libs/ui/datepicker-de.js"></script>
<script src="libs/ui/jquery.ui.timepicker.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="libs/bootstrap-fileinput/js/plugins/purify.min.js"></script>
<script src="libs/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="libs/bootstrap-fileinput/themes/fa/theme.js"></script>
<script src="libs/bootstrap-fileinput/js/locales/pl.js"></script>
<script src="libs/sweetalert/sweetalert.min.js"></script>
<script src="libs/jquery-cookie/cookie.js"></script>
<script src="libs/jq-validation/jquery.validate.min.js"></script>
<script src="libs/jq-validation/additional-methods.min.js"></script>
<script src="js/common.js"></script>
<script src="js/account.js"></script>
<script src="js/registration.js"></script>
<!--<script src="js/main.js"></script>-->
<!-- End Header-->
</body>
</html>