<?php
include_once 'libs/php_mailer/class.phpmailer.php';
include_once 'libs/php_mailer/class.smtp.php';


if (isset($_POST['resetPassword'])) {
    include_once('admin/config.php');
    include_once('admin/startup.php');
    include_once('admin/Model/Model.php');
    startup();

    $db = Model::Instance();
    $letter = $db->Array_where('sections', "WHERE id='6'")[0];
    $data = array();

    $url = "http://www.pks.pl/account.php?resetPassword=" . $_POST['token'] . "&reset=true&id=" . $_POST['id'];

    $data["email"] = $_POST['email'];
    $data["subject"] = $letter ["value"];
    $data["body"] = str_replace("{link}", $url, $letter["value1"]);
    $data["altBody"] = "<p>MDA 2017 (c)</p>";

    sendMail($data);

    die();
}


if (isset($_POST['name']) && !empty($_POST['name']) && isset($_POST['surname']) && !empty($_POST['surname']) && isset($_POST['token']) && !empty($_POST['token']) && isset($_POST['email']) && !empty($_POST['email'])) {
    include_once('admin/config.php');
    include_once('admin/startup.php');
    include_once('admin/Model/Model.php');
    startup();
    $db = Model::Instance();
    $letter = $db->Array_where('sections', "WHERE id='5'")[0];
    $data = array();

    $url = "http://www.pks.pl/registration.php?userCode=" . $_POST['token'];

    $data["email"] = $_POST['email'];
    $data["subject"] = $letter ["value"];
    $data["body"] = str_replace("{link}", $url, $letter["value1"]);
    $data["altBody"] = "<p>MDA 2017 (c)</p>";

    sendMail($data);
    echo 'success';
    header('Location:/');
}


function sendMail($data) {
    $MailErrors = array();
    $mailer = new PHPMailer(true);

    try {
        //server setings
        defineSettings($mailer);

        //recipients
        $mailer->Subject = $data["subject"];
        $mailer->Body = "<html lang='pl'>$data[body]</html>";
        $mailer->AltBody = $data["altBody"];
        $mailer->addAddress($data["email"]);

        $mailer->send();

    } catch (Exception $e) {
        echo $mailer->ErrorInfo;
    }

}

function defineSettings($mailer) {
    $mailer->isSMTP();
    $mailer->Host = 'pkskrakow.nazwa.pl';
    $mailer->SMTPDebug = 2;
    $mailer->SMTPAuth = true;
    $mailer->Port = 25;
    $mailer->Username = 'ticket@pks.pl';
    $mailer->Password = 'SZczot3czk4!';
    $mailer->CharSet = 'UTF-8';
    $mailer->isHTML(true);

    $mailer->setFrom('ticket@pks.pl');
    $mailer->FromName = 'PKS.PL';

    return $mailer;
}

/**
 * Function for generating pdf
 * @isInvoice == true : generating pdf
 */

function generatePDF($encodedFile, $isInvoice) {
    if ($isInvoice != true) {
        fwrite(fopen('Plik z biletem.pdf', 'w'), base64_decode($encodedFile));
        return realpath('Plik z biletem.pdf');
    } else {
        include_once ('libs/mpdf50/mpdf.php');

        // Defaults for creating pdf library
        $mpdf = new mPDF('utf-8', 'A4', '6', '', 10, 10, 7, 7, 10, 10);
        $mpdf->charset_in = 'utf-8';

        // Styles for pdf
        $mpdf->WriteHTML(file_get_contents('css/style_faktura.css'), 1);

        // generate invoice
        session_start();
        $mpdf->WriteHTML($_SESSION["faktura"], 2);
        $mpdf->Output('Plik z fakturą.pdf', 'F');
        return realpath('Plik z fakturą.pdf');
    }
}

/**
 * Function for generating invoices
 */

function generateInvoice($data, $tableData) {
    include_once('admin/config.php');
    include_once('admin/startup.php');
    include_once('admin/Model/Model.php');
    startup();
    $db = Model::Instance();
    $page = $db->Array_where('sections', "WHERE id='1'")[0];
    $numberTicket = '';



    foreach ($tickets as $key => $ticket) {
        $selectTicket = "SELECT * FROM tickets WHERE number_ticket ='" . $ticket['nu'] . "'";
        $query = mysql_query($selectTicket);

        $numberTicket .= ($key != 0) ? ", " : "";
        $numberTicket .=  mysql_fetch_assoc($query)["Number"];

    }

    $faktura = (isset($data)) ? json_decode(json_encode($data), true) : $_POST['faktura'];
    $invoice = "<div id='faktura' class='faktura'>
            <div class='container pdf' style='width: 1180px;'>
                <div class='row'>
                    <h2>Faktura nr ". $page[value] . " - " . substr($faktura['ticket_number'], 3) . "</h2>
                </div>
                <table class='row contacts_wrrap'>
                    <tr>
                        <td class='col'>
                            <div>Miejsce, data wystawienia: Kraków, " . $faktura['date_purchase'] . "</div>                            
                        </td>
                        <td class='col'>
                            <div>Data dostawy/wykonania usługi: " . $faktura['date_purchase'] . "</div>                            
                        </td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:5px'></td>
                        <td class='col' style='height:5px'></td>
                    </tr>
                    <tr>
                        <td class='col'>
                            <div class='contacts_block'>
                                <h4>Sprzedawca</h4>
                                <p>MAŁOPOLSKIE DWORCE AUTOBUSOWE</p>
                                <p>SPÓŁKA AKCYJNA</p>
                                <p>al. płk. Władysława Beliny-Prażmowskiego 6A lok.6</p>
                                <p>31-514 Kraków </p>
                            </div>
                        </td>
                        <td class='col'>
                            <div class='contacts_block'>
                                <h4>Nabywca</h4>
                                <p>Nazwa firmy lub os. fiz: " . $faktura['faktura']['name_company'] . "</p>
                                <p>Kod pocztowy: " . $faktura['faktura']['postal_code'] . "</p>
                                <p>Miasto: " . $faktura['faktura']['city'] . "</p>
                                <p>Ulica: " . $faktura['faktura']['street'] . "</p>
                                <p>NIP: " . $faktura['faktura']['nip'] . "</p>
                            </div>
                        </td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:5px'></td>
                        <td class='col' style='height:5px'></td>
                    </tr>
                    <tr>
                        <td class='col'><div>Forma zapłaty : Przelew</div></td>
                        <td class='col'></td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:5px'></td>
                        <td class='col' style='height:5px'></td>
                    </tr>
                    <tr>
                        <td class='col'><div>Numer konta: DB PBC S.A. - 07 1910 1048 4800 1407 1121 0001</div></td>
                        <td class='col'></td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:5px'></td>
                        <td class='col' style='height:5px'></td>
                    </tr>
                    <tr>
                        <td class='col'><div>Uwagi: Dotyczy biletu nr " . substr($faktura['ticket_number'], 3) . "</div></td>
                        <td class='col'></td>
                    </tr>
                    <tr class='empty_tr'>
                        <td class='col' style='height:10px'></td>
                        <td class='col' style='height:10px'></td>
                    </tr>
                </table>
                <table class='faktura table'>
                    <tr>
                        <th rowspan='2'>Lp</th>
                        <th rowspan='2'>Nazwa towaru / usługi</th>
                        <th rowspan='2'>szt.</th>
                        <th rowspan='2'>Ilość</th>
                        <th rowspan='2'>Cena<br>netto</th>
                        <th rowspan='2'>Wartość<br>netto</th>
                        <th colspan='2'>Podatek</th>
                        <th rowspan='2'>Wartość<br>brutto</th>
                    </tr>
                    <tr>
                        <th>%</th>
                        <th>kwota</th>
                    </tr>";

    $index = 1;
    $table = (isset($tableData)) ? json_decode(json_encode($tableData), true) : $_POST['table'];
    $total_netto = 0;
    $total_kwota = 0;
    $total_brutto = 0;

    foreach ($table as $item) {
        $total_netto += floatval($item['date_price']);
        $total_kwota += floatval($item['date_vat_pl']);
        $total_brutto += floatval($item['date_gross_price']);

        $invoice .= "
                    <tr>
                        <td>" . $index . "</td>
                        <td class='description'>Sprzedaż biletów jednorazowych;<br>
                        Relacja: " . $item['date_from'] . " - " . $item['date_to'] . "<br>
                        Bosacka;<br>
                        Data kursu:" . $item['date_departure'] . "; Godzina : " . $item['date_departure_hours'] . "; Odległość: " . $item['distance'] . "</td>
                        <td></td>
                        <td>" . $item['date_count'] . "</td>
                        <td>" . $item['date_price'] . "</td>
                        <td>" . $item['date_price'] . "</td>
                        <td>8,00 </td>
                        <td>" . $item['date_vat_pl'] . "</td>
                        <td>" . $item['date_gross_price'] . "</td>
                    </tr>
                   ";
        $index++;
    }

    $invoice .= "<tr class='bold'>
                        <td colspan='5'>Razem</td>
                        <td>" . $total_netto . "</td>
                        <td>X</td>
                        <td>" . $total_kwota . "</td>
                        <td>" . $total_brutto . "</td>
                    </tr>
                    <tr class='bold'>
                        <td colspan='5' class='without_border'></td>
                        <td>" . $total_netto . "</td>
                        <td>8</td>
                        <td>" . $total_kwota . "</td>
                        <td>" . $total_brutto . "</td>
                    </tr>
                </table>
                <table class='final_row row'>
                    <tr>
                        <td class='col'>
                            <h4>Do zapłaty : " . $total_brutto . " zł</h4>
                            <div>
                                <p>Słownie: " . numberToText($total_brutto) . "</p>
                            </div>
                        </td>
                        <td class='col'>
                            <div>Wydruk z dnia: " . $faktura['date_purchase'] . "</div>
                        </td>
                        <td class='col'></td>
                </table>
            </div>
        </div>";
    return $invoice;
}


/**
 * This function is used to set headers for mails
 */

function setHeaders() {
    $from_name = 'MDA';
    $from_email = 'ticket@pks.pl';

    $headers = "Content-type: text/html; charset=\"utf-8\"\r\n";
    $headers .= "From: $from_name<$from_email>\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
    $headers .= "Reply-To: <$from_email>\n";
    return $headers;
}

function numberToText($liczba) {

    $separator = ' ';
    $jednosci = array('', ' jeden', ' dwa', ' trzy', ' cztery', ' pięć', ' sześć', ' siedem', ' osiem', ' dziewięć');
    $nascie = array('', ' jedenaście', ' dwanaście', ' trzynaście', ' czternaście', ' piętnaście', ' szesnaście', ' siedemnaście', ' osiemnaście', ' dziewietnaście');
    $dziesiatki = array('', ' dziesieć', ' dwadzieścia', ' trzydzieści', ' czterdzieści', ' pięćdziesiąt', ' sześćdziesiąt', ' siedemdziesiąt', ' osiemdziesiąt', ' dziewięćdziesiąt');
    $setki  = array('', ' sto', ' dwieście', ' trzysta', ' czterysta', ' pięćset', ' sześćset', ' siedemset', ' osiemset', ' dziewięćset');
    $grupy = array(
        array('' ,'' ,''),
        array(' tysiąc' ,' tysiące' ,' tysięcy'),
        array(' milion' ,' miliony' ,' milionów'),
        array(' miliard',' miliardy',' miliardów'),
        array(' bilion' ,' biliony' ,' bilionów'),
        array(' biliard',' biliardy',' biliardów'),
        array(' trylion',' tryliony',' trylionów')
    );

    $wynik = ''; $znak = '';
    if ($liczba == 0)
        return 'zero';
    if ($liczba < 0) {
        $znak = 'minus';
        $liczba = -$liczba;
    }
    $g = 0;
    while ($liczba > 0) {


        $s = floor(($liczba % 1000)/100);
        $n = 0;
        $d = floor(($liczba % 100)/10);
        $j = floor($liczba % 10);


        if ($d == 1 && $j>0) {
            $n = $j;
            $d = $j = 0;
        }

        $k = 2;
        if ($j == 1 && $s+$d+$n == 0)
            $k = 0;
        if ($j == 2 || $j == 3 || $j == 4)
            $k = 1;

        if ($s+$d+$n+$j > 0)
            $wynik = $setki[$s].$dziesiatki[$d].$nascie[$n].$jednosci[$j].$grupy[$g][$k].$wynik;

        $g++;
        $liczba = floor($liczba/1000);
    }
    return trim($znak.$wynik);

}